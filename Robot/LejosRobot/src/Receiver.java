import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import lejos.nxt.SensorPort;
import lejos.nxt.Sound;
import lejos.nxt.TouchSensor;
import lejos.nxt.comm.Bluetooth;
import lejos.nxt.comm.NXTConnection;

/**
 * Handles communication between the robot and the comms server
 * 
 * @author Sarun Gulyanon
 * @author Richard Kenyon
 * 
 */
public class Receiver extends Thread {

	private static final byte ROBOT_READY = 0;

	private static final int COMMAND_SIZE = 3;
	
	// TODO: Do we need this?
	private static final int ERROR_CMD = 98;
	
	// The number of milliseconds we ignore commands for when
	// one of the robot's touch sensors are triggered
	private static final int IGNORE_TIME_MS = 1500;

	private TouchSensor leftTouch = new TouchSensor(SensorPort.S2);
	private TouchSensor rightTouch = new TouchSensor(SensorPort.S1);

	private InputStream in;
	private OutputStream out;
	private NXTConnection connection;
	
	private boolean isConnected = false;
	private boolean pleaseExit = false;
	private byte[] command = null;

	public Receiver() {
		establishConnection();
	}

	/**
	 * Determines whether the robot is connected to the comms server or not
	 * 
	 * @return A boolean representing the state of the connection
	 */
	public boolean hasConnection() {
		return isConnected;
	}

	/**
	 * Gets a command's opcode
	 * 
	 * @param command
	 *            A command that has been sent to the robot
	 * @return The opcode as a byte
	 */
	public byte getOpcode(byte[] command) {
		if (command != null) {
			return command[0];
		} else {
			// TODO: find a sensible value to return
			return ERROR_CMD;
		}
	}

	/**
	 * Returns an argument from a command
	 * 
	 * @param command
	 *            A command that has been sent to the robot
	 * @param argNo
	 *            Used to select an argument from a command i.e. 1 to get the
	 *            first argument, 2 for the second, and so on...
	 * @return The argument as a byte
	 */
	public byte getArgument(byte[] command, int argNo) {
		if (command != null && argNo > 0 && argNo < command.length) {
			return command[argNo];
		} else {
			// TODO: find a sensible value to return
			return ERROR_CMD;
		}
	}

	/**
	 * Check the Bluetooth connection. Reconnect if necessary.
	 */
	public void run() {
		while (!pleaseExit) {
			if (!isConnected) {
				System.out
					.println("Attempting to reconnect...");
				closeConnection();
				establishConnection();
			}
			
			command = receiveCommand();
		}

		System.out.println("Asked to exit; closing connection...");
		closeConnection();
	}

	/**
	 * Ask the Receiver to exit
	 */
	public void exit() {
		pleaseExit = true;
	}

	/**
	 * Create a bluetooth connection to PC
	 */
	private void establishConnection() {
		System.out.println("Waiting for Bluetooth connection...");
		connection = Bluetooth.waitForConnection();
		in = connection.openInputStream();
		out = connection.openOutputStream();
		
		System.out.println("Connection established!");
		sendReadySignal();
		isConnected = true;

		Sound.setVolume(Sound.VOL_MAX);
		Sound.beep();
	}

	/**
	 * Send 0 to PC indicating that robot is ready
	 */
	private void sendReadySignal() {
		sendByte(ROBOT_READY);
	}

	/**
	 * Send a byte to PC
	 * 
	 * @param b
	 *            the byte we want to send to the PC
	 */
	private void sendByte(byte b) {
		try {
			out.write(b);
			out.flush();
		} catch (IOException e) {
			System.err.println("Couldn't send byte: " + e.toString());
		}
	}

	/**
	 * Listens on the input stream for data and commands
	 * 
	 * @return The command as a byte array or null if there is some problem with
	 *         the connection
	 */
	public byte[] receiveCommand() {

		byte[] bytes = new byte[COMMAND_SIZE];

		try {
			int numOfBytesRead = in.read(bytes);
			
			if (numOfBytesRead == -1) {
				isConnected = false;	// we reached the end of the stream
			}

		} catch (IOException e) {
			System.err.println("Exception occured "
					+ "while reading from stream: " + e.toString());
			isConnected = false;
		}
		
		return bytes;
	}
	
	public byte[] getCommand() {
		// Ignore commands for a while if we bump into anything
		if (leftTouch.isPressed() || rightTouch.isPressed()) {
			try {
				Thread.sleep(IGNORE_TIME_MS);
			} catch (InterruptedException e) {
				System.err.println(e.toString());
			}
		}

		byte[] b = command;
		
		// We set the command buffer to null because,
		// the controller is only interested in new commands.
		// We don't want to give the controller the same command twice.
		command = null;
		
		return b;
	}

	/**
	 * Closes bluetooth connection
	 */
	private void closeConnection() {
		try {
			in.close();
			out.close();
		} catch (IOException e) {
			System.err.println("Couldn't close connection " + e.toString());
		}
		
		connection.close();
		isConnected = false;
	}
}