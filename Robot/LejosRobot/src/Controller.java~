import lejos.nxt.Button;
import lejos.nxt.Motor;
import lejos.nxt.NXT;
import lejos.nxt.Sound;

/**
 * Executes commands received from Strategy
 * 
 * @author Iliyan Ivanov
 * @author Keith Robertson
 * 
 */
public class Controller {

	/**
	 * opcode fields
	 */
	private final static int MOVE = 0X01;
	private final static int KICK = 0X02;
	private final static int TURN = 0X03;
	private final static int STOP = 0X04;
	private final static int EXIT = 99;

	private static Receiver receiverThread;

	public static void main(String[] args) {

		Motor.A.setAcceleration(1000);
		Motor.B.setAcceleration(1000);

		receiverThread = new Receiver();
		receiverThread.start();

		for (;;) {
			if (receiverThread.hasConnection()) {
				byte[] command = receiverThread.getCommand();
				
				if (command != null) {
					runCommand(command);
				}
			} else {
				//System.out.println("Connection Lost");
				// defend();

			}
		}
	}

	/**
	 * Checks the opcode and executes the right movement method
	 * 
	 */
	private static void runCommand(byte[] command) {
		int opcode = receiverThread.getOpcode(command);

		switch (opcode) {
		case MOVE:
			System.out.println("CASE : MOVE");
			int left = receiverThread.getArgument(command, 1);
			int right = receiverThread.getArgument(command, 2);
			setMotorSpeed(left, right);
			break;
		case KICK:
			System.out.println("CASE : KICK");
			kick();
			break;
		case TURN:
			System.out.println("CASE : TURN");
			byte angle1 = receiverThread.getArgument(command, 1);
			byte angle2 = receiverThread.getArgument(command, 2);
			turnByAngle(joinInt(angle1, angle2));
			// rotateByAngle(angle);
			break;
		case STOP:
			System.out.println("CASE : STOP");
			stop();
			break;
		case EXIT:
			System.out.println("CASE : EXIT");
			quit();
			break;
		}
	}

	/**
	 * Wait for the robot to stop moving, beep, close the receiver thread and
	 * reboot the brick
	 */
	private static void quit() {
		// TODO Auto-generated method stub

		while (Motor.A.isMoving() && Motor.B.isMoving()) {
			; // wait for previous movement to finish
		}
		Sound.setVolume(100);
		Sound.beep();
		receiverThread.exit();
		NXT.exit(0);
	}

	/**
	 * Stop any kind of movement - does not wait for the robot to finish its
	 * previous movement
	 */
	private static void stop() {
		Motor.A.setSpeed(0);
		Motor.B.setSpeed(0);
		Motor.A.stop();
		Motor.B.stop();
	}

	/**
	 * Kick once. Can kick while moving
	 */
	private static void kick() {
		Motor.C.setSpeed(900);
		Motor.C.rotate(-55);
		Motor.C.rotate(55);
	}

	/**
	 * Movement method. Waits for the robot to stop any previous movement. Sets
	 * the speeds for the left and the right motor 
	 * - speeds vary from -90 : 90 -
	 * - negative numbers cause the motors to rotate backwards
	 */
	private static void setMotorSpeed(int left, int right) {
		while (Motor.A.isMoving() && Motor.B.isMoving()) {
			; // wait for previous movement to finish
		}
		if (left < 0 && right < 0) {
			System.out.println("Both NEGATIVE");
			Motor.A.setSpeed(10 * (-left));
			Motor.B.setSpeed(10 * (-right));
			Motor.A.backward();
			Motor.B.backward();
		} else if (left < 0 && right >= 0) {
			Motor.A.setSpeed(10 * (-left));
			Motor.B.setSpeed(10 * right);
			Motor.A.backward();
			Motor.B.forward();
		} else if (left >= 0 && right < 0) {
			Motor.A.setSpeed(10 * left);
			Motor.B.setSpeed(10 * (-right));
			Motor.A.forward();
			Motor.B.backward();
		} else if (left >= 0 && right >= 0) {
			System.out.println("Both POSITIVE");
			Motor.A.setSpeed(10 * left);
			Motor.B.setSpeed(10 * right);
			Motor.A.forward();
			Motor.B.forward();
		}
	}

	/**
	 * Turn the robot by the given angle. - again waits for any previous
	 * movement to finish
	 */

	private static void turnByAngle(int angle) {
		while (Motor.A.isMoving() && Motor.B.isMoving()) {
			; // wait for previous movement to finish
		}
		System.out.println("Turning by " + angle);
		Motor.A.setSpeed(400);
		Motor.B.setSpeed(400);
		Motor.A.rotate(angle, true);
		Motor.B.rotate(-angle, true);
	}

	/**
	 * Joins the two received bytes into an int
	 */
	public static int joinInt(byte x, byte y) {
		int a = x << 8;
		int b = y & 255;
		System.out.println(a + " + " + b);
		return a + b;
	}

	/**
	 * A backup mechanism - if the connection goes down, this method tries to
	 * move the robot back to our own goal
	 * 
	 */
	private static void defend() {
		// Might be needed later
		int ballPositionX;
		int ballPositionY;

		final int startOfPitchX = 5;
		final int endOfPitchY = 327;
		final int middleOfPitchX = 320;
		final int middleOfPitchY = 207;

		// Initially, we assume robot is in the centre of pitch
		// private static int robotPositionX = middleOfPitchX;
		// private static int robotPositionY = middleOfPitchY;

		// A random point on the pitch (for testing)
		final int robotPositionX = 266;
		final int robotPositionY = 196;

		// Facing east-end goal (assumed opponents)
		int orientation = 297;

		final int pitchLength = 630;
		final int pitchSizeWidth = 250;

		int centreOfGoalX = (pitchLength / 2) + startOfPitchX;
		int centreOfGoalY = endOfPitchY;

		double deltaX = robotPositionX - centreOfGoalX;
		double deltaY = robotPositionY - centreOfGoalY;

		System.out.println("deltaX: " + deltaX);
		System.out.println("deltaY: " + deltaY);

		// double thetaRadians = Math.atan(deltaX / deltaY);
		double thetaRadians = Math.atan(deltaX / deltaY);

		double thetaDegrees = thetaRadians * (180 / Math.PI);

		System.out.println("thetaRadians: " + thetaRadians);
		System.out.println("thetaDegrees: " + thetaDegrees);

		int turningAngle = (int) (180 - thetaDegrees - orientation);
		System.out.println("turningAngle: " + turningAngle);

		/*
		 * if (robotPositionX > centreOfGoalX) { angleWanted = angleWanted -
		 * orientation; } else if (robotPositionX < centreOfGoalX) { angleWanted
		 * = angleWanted - orientation; } else if (robotPositionX ==
		 * centreOfGoalX) { angleWanted = 180 - orientation; }
		 */

		// if (robotPositionX == centreOfGoalX) {
		/*
		 * int turningAngle = 180 - theta - orientation; // } else { //
		 * turningAngle -= orientation; // }
		 * 
		 * long distanceTime = (pitchSizeWidth - robotPositionY) * 4700 /
		 * pitchSizeWidth;
		 * 
		 * turnByAngle(turningAngle);
		 * 
		 * Motor.A.setSpeed(720); Motor.B.setSpeed(720); Motor.A.forward();
		 * Motor.B.forward(); Thread.sleep(distanceTime); Motor.A.stop();
		 * Motor.B.stop(); if (turningAngle > 180) { turnByAngle(360 -
		 * turningAngle); } else { turnByAngle(-turningAngle); }
		 */

		Button.waitForPress();
	}
}
