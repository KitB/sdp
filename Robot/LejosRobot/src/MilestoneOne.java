import lejos.nxt.Button;
import lejos.nxt.Motor;
import lejos.nxt.NXT;


public class MilestoneOne {
	
	
	
	public static void main(String[] args){
		// Setting the acceleration for the motors
		Motor.A.setAcceleration(1000);
		Motor.B.setAcceleration(1000);
		//Penalty
		System.out.println("Press a button...");
		Button.waitForPress();
		kick();
		//Long Kick
		System.out.println("Press a button...");
		Button.waitForPress();
		kick();
		//Travel the whole pitch length
		System.out.println("Press a button...");
		Button.waitForPress();
		travel();
		NXT.exit(0);
		
	}
	
	private static void kick(){
		System.out.println("Kicking");
		Motor.C.setSpeed(900);
		Motor.C.rotate(-55);
		Motor.C.rotate(55);		
	}
	private static void travel() {
		Motor.A.setSpeed(720);
		Motor.B.setSpeed(720);
		Motor.A.forward();
		Motor.B.forward();
		try {
			Thread.sleep(4700);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Motor.A.setSpeed(0);
		Motor.B.setSpeed(0);	
	}

}
