import lejos.nxt.Motor;
import lejos.nxt.NXT;
import lejos.nxt.Sound;

/**
 * Robot Controller which executes commands received from Strategy
 * 
 * @author Iliyan Ivanov
 * 
 */
public class Controller {

	/**
	 * Opcodes
	 */
	private final static int MOVE = 0X01;
	private final static int KICK = 0X02;
	private final static int TURN = 0X03;
	private final static int STOP = 0X04;
	private final static int RUSH = 0X05;
	private final static int PENALTY = 0X07;
	private final static int DEFENCE = 0X08;

	private final static int PENALTY_LEFT = 1;
	private final static int PENALTY_MIDDLE = 2;
	private final static int PENALTY_RIGHT = 3;

	private final static int EXIT = 99;

	private static Receiver receiverThread;
	private static Instinct instinct;

	// The strategy team sends an angle in degrees
	// TURN_FACTOR is used to make the robot turn
	// through the same angle sent to us by strategy
	private final static double TURN_FACTOR = 1.40;

	public static void main(String[] args) {

		final int ACCELERATION = 1000;
		final int KICKACCELERATION = 6000;
		Motor.A.setAcceleration(ACCELERATION);
		Motor.B.setAcceleration(ACCELERATION);
		Motor.C.setAcceleration(KICKACCELERATION);

		instinct = new Instinct();
		instinct.start();

		receiverThread = new Receiver();
		receiverThread.start();

		for (;;) {
			if (receiverThread.hasConnection()) {
				byte[] command = receiverThread.getCommand();

				if (command != null) {
					runCommand(command);
				}
			} else {
				stop();
			}
		}
	}

	/**
	 * Checks the opcode and executes the right movement method
	 */
	private static void runCommand(byte[] command) {
		byte opcode = receiverThread.getOpcode(command);

		switch (opcode) {
		case MOVE:
			int left = receiverThread.getArgument(command, 1);
			int right = receiverThread.getArgument(command, 2);

			setMotorSpeed(left, right);
			break;

		case KICK:
			kick();
			break;

		case TURN:
			// The angles we get from strategy are split into two parts
			byte firstPart = receiverThread.getArgument(command, 1);
			byte secondPart = receiverThread.getArgument(command, 2);

			turnByAngle(joinInt(firstPart, secondPart));
			break;

		case RUSH:
			byte d1 = receiverThread.getArgument(command, 1);
			byte d2 = receiverThread.getArgument(command, 2);
			startRush(joinInt(d1,d2));
			break;

		case STOP:
			stop();
			break;

		case PENALTY:
			byte direction = receiverThread.getArgument(command, 1);
			penalty(direction);
			break;

		case DEFENCE:
			defence();
			break;

		case EXIT:
			// TODO: EXIT should be QUIT (exit is a reserved keyword in Python)
			quit();
			break;
		}
	}

	private static void defence() {
		final int SPEED = 90;
		final int TRAVEL_TIME_MS = 500;

		setMotorSpeed(SPEED, SPEED);

		try {
			Thread.sleep(TRAVEL_TIME_MS);
		} catch (InterruptedException e) {}

		stop();

		setMotorSpeed(-SPEED, -SPEED);

		try {
			Thread.sleep(TRAVEL_TIME_MS);
		} catch (InterruptedException e) {}

		stop();
	}

	private static void penalty(byte direction) {
		System.out.println("PENALTY: " + direction);

		final int ANGLE_DEGREES = 25;
		final int DISTANCE_CM = 13;
		final int SPEED = 3;

		travel(SPEED, DISTANCE_CM);
		//startRush(DISTANCE_CM);
		
		stop();
		
		switch (direction) {
		case PENALTY_LEFT:
			turnByAngle(ANGLE_DEGREES);
			kick();
			break;

		case PENALTY_MIDDLE:
			kick();
			break;

		case PENALTY_RIGHT:
			turnByAngle(-ANGLE_DEGREES);
			kick();
			break;
		}
	}

	/**
	 * Wait for the robot to stop moving, beep, close the receiver thread and
	 * reboot the brick
	 */
	private static void quit() {
		final int VOLUME = 100;

		Sound.setVolume(VOLUME);
		Sound.beep();

		receiverThread.exit();
		NXT.exit(0);
	}

	/**
	 * Stop any kind of movement - does not wait for the robot to finish its
	 * previous movement
	 */
	private static void stop() {
		Motor.A.setSpeed(0);
		Motor.B.setSpeed(0);
		Motor.A.stop();
		Motor.B.stop();
	}

	/**
	 * Kick once. Can kick while moving
	 */
	private static void kick() {
		final int KICKER_SPEED = 900;
		final int KICKER_ANGLE = 20;

		Motor.C.setSpeed(KICKER_SPEED);
		Motor.C.rotate(-KICKER_ANGLE);
		Motor.C.rotate(+KICKER_ANGLE);
	}

	/**
	 * Movement method. Waits for the robot to stop any previous movement Sets
	 * the speeds for the left and the right motor - speeds vary from -90 : 90
	 * negative numbers cause the motors to rotate backwards
	 * 
	 * @param left
	 *            - speed for the left motor (-90;90)
	 * @param right
	 *            - speed for the right motor (-90;90)
	 */
	private static void setMotorSpeed(int left, int right) {
		// TODO: what is 10? can we replace it with a constant?

		if (left >= 0 && right >= 0) {
			Motor.A.setSpeed(10 * left);
			Motor.B.setSpeed(10 * right);
			Motor.A.forward();
			Motor.B.forward();
		} else if (left < 0 && right >= 0) {
			Motor.A.setSpeed(10 * (-left));
			Motor.B.setSpeed(10 * right);
			Motor.A.backward();
			Motor.B.forward();
		} else if (left >= 0 && right < 0) {
			Motor.A.setSpeed(10 * left);
			Motor.B.setSpeed(10 * (-right));
			Motor.A.forward();
			Motor.B.backward();
		} else if (left < 0 && right < 0) {
			Motor.A.setSpeed(10 * (-left));
			Motor.B.setSpeed(10 * (-right));
			Motor.A.backward();
			Motor.B.backward();
		}
	}

	/**
	 * Turn the robot by the given angle. - again waits for any previous
	 * movement to finish
	 * 
	 * @param angle
	 *            - the angle to turn by in degrees
	 */
	private static void turnByAngle(int angle) {
		stop();

		final int TURNING_SPEED = 400;
		Motor.A.setSpeed(TURNING_SPEED);
		Motor.B.setSpeed(TURNING_SPEED);

		int angleNew = (int) (angle * TURN_FACTOR);
		Motor.A.rotate(-angleNew, true);
		Motor.B.rotate(angleNew, true);

		while (Motor.A.isMoving() && Motor.B.isMoving()) {
			; // wait for previous movement to finish
		}
	}

	/**
	 * Joins the two received bytes into an int
	 */
	public static int joinInt(byte x, byte y) {
		int a = x << 8;
		int b = y & 255;

		return a + b;
	}

	/**
	 * Robot move forward/backward for a certain distance
	 * 
	 * @param speed
	 *            - speed of robot between -7 : +7
	 * @param distance
	 *            - distance we want to travel in ~cm~
	 */
	public static void travel(int speed, int distance) {
		// TODO: loose the magic numbers

		Motor.A.setSpeed(100 * speed);
		Motor.B.setSpeed(100 * speed);
		int go = 14 * distance;

		if (speed < 0) {
			Motor.A.rotate(-go, true);
			Motor.B.rotate(-go, false);
		} else {
			Motor.A.rotate(go, true);
			Motor.B.rotate(go, false);
		}
	}
	// 17.03.2012 - v1.0
	// dist in cm.
	// opcode = 5
	public static void startRush(int dist){
		Motor.A.setSpeed(900);
		Motor.B.setSpeed(900);
		//dp.travel(dist);
		int d = (int)(14.05*dist);
		Motor.A.rotate(d, true);
		Motor.B.rotate((d+45),true);
		kick();
	}
}
