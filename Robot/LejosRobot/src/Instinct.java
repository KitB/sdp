import lejos.nxt.SensorPort;
import lejos.nxt.TouchSensor;

public class Instinct extends Thread {

	// The maximum amount of time it takes the robot to
	// back off when its sensors are triggered.
	private static final int REACTION_TIME = 50;

	private TouchSensor leftTouch = new TouchSensor(SensorPort.S2);
	private TouchSensor rightTouch = new TouchSensor(SensorPort.S1);

	/**
	 * Back away if any of the touch sensors are pressed
	 */
	
	private final int DISTANCE = 15;	// originally 15, doubled for milestone 4
	private final int SPEED = -20;
	
	public void run() {
		for (;;) {
			while (leftTouch.isPressed() || rightTouch.isPressed()) {
				Controller.travel(SPEED, DISTANCE); // Move back a little
			}

			// Right now strategy don't care about this
			// receiverThread.sendTouch();

			try {
				Thread.sleep(REACTION_TIME);
			} catch (InterruptedException e) {
				System.err.println("Instinct: Sleep interrupted: "
						+ e.toString());
			}
		}
	}
}
