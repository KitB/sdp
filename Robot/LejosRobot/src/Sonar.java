import lejos.nxt.SensorPort;
import lejos.nxt.Sound;
import lejos.nxt.UltrasonicSensor;


public class Sonar {
	
	// Ultrasonic test
	// 10 cm away from an object seems a reasonable place to stop
	
	public static void main(String[] args){
		
		while(true){
			
			UltrasonicSensor sonar = new UltrasonicSensor(SensorPort.S1);
			
			int d = sonar.getDistance();
			System.out.println("Distance: "+d);
			if(d < 15){
				try {
					Thread.sleep(100*d);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				Sound.setVolume(80);
				Sound.beep();
			}
			
			
		}
	}

}
