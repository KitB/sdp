
public class ControllerTest {

	private static Receiver receiverThread;

	public static void main(String[] args) {
		byte[] command;
		receiverThread = new Receiver();
		receiverThread.start();
		
		for (;;) {
			if (receiverThread.hasConnection()) {
				command = receiverThread.getCommand();
				
				if (command != null) {
					System.out.println("Controller got command: " + command[0]);
					System.out.println("Control has args {" + command[1] +
						", " + command[2] + ", " + command[3] + ", " + 
						command[4] + "}");			
				}
			}
			else {
				System.out.println("No connection");
			}
		}
	}
}
