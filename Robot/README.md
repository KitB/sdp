# Running
To run the code on the robot, it is usually enough to run the default program. Failing that you can run "Controller.nxj".

# Uploading
To upload code to the robot you will need to run:

    . .bashrc
    nxjbrowse

Then connect to "group 6" and upload the NXJ file using the interface

# Building
To build the Controller run:

    . .bashrc
    cd LejosRobot/src
    nxjc -d ../bin Controller.java
    cd ../bin/
    nxjlink -o Controller.nxj Controller

And you will end up with a nice little Controller.nxj file sitting in your bin/ directory which you can then upload to the robot and run.

