import pygame

from Strategy.Communication.Communication import RobotControl
r = RobotControl()


clock = pygame.time.Clock()
while True:
    clock.tick(25)
    r.stop()
