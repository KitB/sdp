from pygame.time import Clock
import time

from ..LocalExceptions import TimeOut
from ..Params import Params
from ..Logging.SingleLog import Log
from ..Utils.Predictor import Predictor

TAG = "STRATEGY"

class Strategy:
    def __init__(self, world, robot, decisions=list(), framerate=30, timeout=None):
        Log.d(TAG, "Strategy created")
        self._world = world
        self._robot = robot
        self._decisions = decisions
        self._isDone = True
        self.framerate = framerate
        self.timeout = timeout
        self.extras = dict()
        if timeout:
            self.start_time = time.time()

        self.__predictor = Predictor(world)

    def attach(self, decision):
        self._decisions.append(decision)

    def getPredictor(self):
        return self.__predictor

    def finish(self):
        self._isDone = True
        self._robot.stop()

    def getWorld(self):
        return self._world

    def getRobot(self):
        return self._robot

    def setExtra(self, name, value):
        self.extras[name] = value

    def getExtra(self, name):
        return self.extras[name]

    def __call__(self):
        self.start_time = time.time()
        self._isDone = False
        clock = Clock()
        try:
            while not self._isDone:
                clock.tick(Params.strategyFPS)
                
                self.getWorld().update()

                
                # Checks actions according their priority and executes the first
                # (highest priority) action which fires.
                for decision in self._decisions:
                    # Loop through decisions until we find one that fires
                    isTrue = decision(self)
 #                   Log.d(TAG, "%s returned %s" % (decision._percept.__class__, "False" if not isTrue else "!!!!!!!!!!!!!TRUE!!!!!!!!!!!!!"))
                    
                    if isTrue:
                        break
        except KeyboardInterrupt:
            Log.d(TAG, "User exited")
            self.finish()
