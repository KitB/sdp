from math import hypot, pi, degrees

from ..LocalFuncs import angleBetween, relativeAngle, botDistance, approxAt, getKickingPos, getDefendingPos, computeIntersection
from ..Shared.Funcs import vecDist, angleDiff, vecAngle
from ..Model.World import *
from ..Params import Params
from ..Logging.SingleLog import Log
from ..Shared.Funcs import vecNormalise
from ..Drawing.Drawable import Line
import numpy

TAG = "PERCEPT"

class Percept(object):
    def __init__(self):
        self._percepts = list()
        self._negated = False

    def _check(self, world):
        """ Return whether the percept is true in world. NEVER CALL THIS DIRECTLY """
        return True

    def __call__(self, strategy):
        """ Check a compound percept """
        for p in self._percepts:
            if not p(strategy):
                return False
        return (self._check(strategy) != self._negated)

    def negate(self):
        self._negated = not self._negated
        return self

    def add(self, percept):
        """ Add a new percept to this. """
        # I was able to form recursive percepts that would exhaust the stack prior to adding this line
        assert self not in percept._percepts, "Recursive percept detected, aborting"
        self._percepts.append(percept)
        # Return self so we can chain calls to add and initialise with them
        # Like so: p = HasBall().add(CanScore()).add(InGoal())
        return self


class AlwaysTrue(Percept):
    def _check(self,strategy):
        return True

class HasBall(Percept):


    def __init__(self, distance_threshold=Params.hasBallDistTol, angle_threshold=Params.hasBallAngleTol):
        super(HasBall, self).__init__()
        self.dt = distance_threshold
        self.at = angle_threshold
        self.ball_updated = True
        self.last_value_percept = False


    def _check(self, strategy):
        if self.get_ball_updated(strategy):
            world = strategy.getWorld()
            d = botDistance(world.getUs(), world.getBall())
            a = angleBetween(world.getUs(), world.getBall())
            self.last_value_percept = ((d < self.dt) and (abs(a) < self.at))
        return self.last_value_percept


    def get_ball_updated(self, strategy):
        world = strategy.getWorld()
        us = world.getUs()
        ball = world.getBall()
        return us.positions[-1][3] <= ball.positions[-1][2]

class HasBallMoved(Percept):
    def _check(self,strategy):
        try:
            return strategy.getExtra('Has Ball Moved')  
        except KeyError:
            return False

class ApproxAt(Percept):
    # WARNING WARNING - use this percept for stationary goals!!!
    def __init__(self, goalPoint, distance_threshold=Params.atGoalDistTol):
        super(ApproxAt, self).__init__()
        self.dt = distance_threshold
        self.gp = goalPoint
    def _check(self, strategy):
        world = strategy.getWorld()
        return approxAt(world.getUs(), self.gp, self.dt)

class ApproxAtKickingPos(Percept):
    def __init__(self, distance_threshold=Params.atGoalDistTol):
        super(ApproxAtKickingPos, self).__init__()
        self.dt = distance_threshold
    def _check(self, strategy):
        world = strategy.getWorld()
        return approxAt(world.getUs(), getKickingPos(world), self.dt)        
        
class ApproxAtDefendingPos(Percept):
    def __init__(self, distance_threshold=Params.atGoalDistTol):
        super(ApproxAtDefendingPos, self).__init__()
        self.dt = distance_threshold
    def _check(self, strategy):
        world = strategy.getWorld()
        return approxAt(world.getUs(), getDefendingPos(world), self.dt)  

class FacingWall(Percept):
    def _check(self,strategy):
        w = strategy.getWorld()
        pos = w.getUs().getPosition()
        kicker = w.getUs().getKickerPosition()
        # is kicker further away from the centre than centroid
        
        return hypot(*kicker) > hypot(*pos)

class MovingToBack(Percept):
    def _check(self,strategy):
        w = strategy.getWorld()
        (vx,vy) = w.getBall().getVelocity()
        
        return vy < 0
        
class NextToWall(Percept):
    def _check(self,strategy):
        w = strategy.getWorld()
        pos = w.getUs().getPosition()
        dims = (Params.pitchLengthAbst/2, Params.pitchWidthAbst/2)
        w= dims[0] - abs(pos[0]) < Params.nextToWallDist
        h= dims[1] - abs(pos[1]) < Params.nextToWallDist
        
        return w or h

class BallMoving(Percept):
    def __init__(self, speedThresh=Params.movingBallSpeedThresh):
        super(BallMoving, self).__init__()
        self.speedThresh = speedThresh
    def _check(self, strategy):        
        w = strategy.getWorld()
        speed = w.getBall().getSpeed()
        
        if speed > self.speedThresh:
            strategy.setExtra('Has Ball Moved', True)
        return speed > self.speedThresh

class WithinGoal(Percept):
    def __init__(self, distThresh=Params.withinGoalTol):
        super(WithinGoal, self).__init__()
        self.distThresh = distThresh
    def _check(self, strategy):
        w = strategy.getWorld()
        r = w.getUs()
        (rx,ry) = r.getPosition()
        (gx,gy) = computeIntersection(w, gx = rx)
        
        return r.at((gx,gy), distTol=Params.inBallMotionLineTol)

class InBallMotionLine(Percept):
    def __init__(self, distThresh=Params.inBallMotionLineTol):
        super(InBallMotionLine, self).__init__()
        self.distThresh = distThresh
    def _check(self, strategy):
        w = strategy.getWorld()
        us = numpy.array(w.getUs().getPosition())
        ball = numpy.array(w.getBall().getPosition())
        b_vel = numpy.array(w.getBall().getVelocity())
    
        if numpy.linalg.norm(b_vel) == 0:
            b_vel = numpy.array([0, 0])
        else:
            b_vel = b_vel/numpy.linalg.norm(b_vel)
        
        p= numpy.array([-b_vel[1] * self.distThresh ,b_vel[0] * self.distThresh]) + ball
        print p
        
        l = 1000
        p11 = p
        p21 = numpy.array([-b_vel[1] * self.distThresh ,b_vel[0] * self.distThresh]) - ball
        
        p12 = (p11[0] + b_vel[0] * l, p11[1] + b_vel[1] * l );
        p22 = (p21[0] + b_vel[0] * l, p21[1] + b_vel[1] * l );
        
        w.clearLayer('goalLine')
        w.addToDrawing(Line(p11,p12, colour=(0,0,255), layer='goalLine', abst=True))
        w.addToDrawing(Line(p21,p22, colour=(0,0,255), layer='goalLine', abst=True))

        
        if b_vel[0] == 0 or b_vel[1] ==0:
            return False

        dist = abs(numpy.dot( (us - p), b_vel ))
        print dist
        
        
        return dist < self.distThresh

class TheyHaveBall(Percept):
    def __init__(self, distance_threshold=Params.hasBallDistTol, angle_threshold=Params.hasBallAngleTol):
        super(TheyHaveBall, self).__init__()
        self.dt = distance_threshold
        self.at = angle_threshold
    def _check(self, strategy):
        world = strategy.getWorld()
        d = botDistance(world.getThem(), world.getBall())
        a = angleBetween(world.getThem(), world.getBall())
        returnval = ((d < self.dt) and (abs(a) < self.at))
        return returnval

class BallInMiddle(Percept):
    def _check(self, strategy):
        world = strategy.getWorld()
        return approxAt(world.getBall(), world.getCentrePos(), Params.ballInMiddleDistTol)

class FacingGoal(Percept):      # rename to FacingTheirGoal
    # TODO: add a more complex check once the exact goal position is known
    def _check(self, strategy):
        world = strategy.getWorld()
        goal = world.getTheirGoalpost()
        a = relativeAngle(world.getUs(), goal)
        return a < Params.facingGoalAngleTol
        
class FacingTheirSide(Percept):     # TODO: check if TheirGoalpost is the correct one!
    # Are we facing their side of the pitch or our side
    def __init__(self, facingAngle= pi/2):
        super(FacingTheirSide, self).__init__()
        self.facingAngle = facingAngle  
    def _check(self,strategy):
        w = strategy.getWorld()
        orient = w.getUs().getOrientation()
        pos = w.getUs().getPosition()
        gp = w.getTheirGoalpost()
        wallPoint = (gp[0], pos[1])
        
        angle = abs(angleDiff(orient,vecAngle(wallPoint,pos))) 
        return angle < self.facingAngle

# Not working
class CanScore(Percept):
    def __init__(self):
        super(CanScore, self).__init__()
        self.add(HasBall()).add(FacingGoal())

    def _check(self, strategy):
        world = strategy.getWorld()
        them = world.getThem()
        ballx, bally = world.getBall().getPosition()
        sideX = world.getTheirGoalpost()[0]

        goalPointTop = (sideX, world.getDimensions()[1]/2)  # TODO: Measure GoalDimensions
        goalPointBottom = (sideX, -world.getDimensions()[1]/2)

        corners = them.getCorners()
        highest, lowest = max(corners, key=lambda x: x[1]), min(corners, key=lambda x: x[1])

        intersectionTop = (float(bally - goalPointTop[1]) / (ballx - goalPointTop[0])) * (highest[0] - ballx) + bally
        intersectionBottom = (float(bally - goalPointBottom[1]) / (ballx - goalPointBottom[0])) * (lowest[0] - ballx) + bally

        topGap = intersectionTop - highest[1]
        bottomGap = lowest[1] - intersectionBottom
        
        ballGap = Params.canScoreBallGap
        
        return (topGap > ballGap or bottomGap > ballGap)

# Note to Self (Jack) I've tweaked isInGoalbox method
class InGoal(Percept):
    def __init__(self, robot=None, side=None):
        super(InGoal, self).__init__()
        self._robot = robot
        self._side = side
    def _check(self, strategy):
        world = strategy.getWorld()
        return world.isInGoalbox(self._robot, self._side)

class UsInGoal(Percept):
    def _check(self, strategy):
        world = strategy.getWorld()
        return world.getUs().at(world.getOurGoalpost(), 200)

class ThemInGoal(Percept):
    def _check(self, strategy):
        world = strategy.getWorld()
        return world.getThem().at(world.getTheirGoalpost(), 200)

class HalfDistance(Percept):
    def __init__(self, initial):
        super(HalfDistance, self).__init__()
        self._initial = initial
        self._prev = initial
    def _check(self, strategy):
        world = strategy.getWorld()
        world.updateBlueRobot(*vc.getBlueBot())
        bot = world.getUs()
        ball = world.getBall()
        distance = vecDist(bot.getPosition(), ball.getPosition())
        angle = angleBetween(bot, ball)
        if distance > self._prev*1.1:
            Log.d(TAG, "Wrong movement")
            return True
        self._prev = distance
        return distance<=(self._initial/2.75)

class HasMovedEnoughToCalculateAngle(Percept): # Dayumn that's a long name
    def __init__(self, initPosition):
        super(HasMovedEnoughToCalculateAngle, self).__init__()
        self.initPosition = initPosition
    def _check(self, strategy):
        return vecDist(bot.getPosition(), self.initPosition)>1.5

class JustOnce(Percept):
    def __init__(self):
        super(JustOnce, self).__init__()
        self.done = False

    def __call__(self, strategy):
        isTrue = super(JustOnce, self).__call__(strategy)
        if isTrue:
            self.done = True
        return isTrue

    def _check(self, strategy):

        if self.done:
            return False
        else:
            return True

class OnGoalLine(Percept):
    def _check(self, strategy):
        w = strategy.getWorld()
        goalPost = w.getTheirGoalpost()
        bot = w.getUs()
        ball = w.getBall()
        a = vecAngle(bot.getPosition(), ball.getPosition())
        if w.getOurSide() == 'low':
            a = -simpleAngle(a+pi)
        return abs(degrees(a)) < Params.facingGoalAngleTol


# Not Working  --> # What is it supposed to do?
class BallGoalBound(Percept):
    def _check(self, strategy):
        world = strategy.getWorld()
        velocity = world.getBall().getVelocity()
        position = world.getBall().getPosition()
        if velocity[0] != 0.0:
            gradient = velocity[1] * 1.0 / velocity[0]
        else:
            return False
        (w, h) = world.getDimensions()
        goal = (w, h/2)
        if world.getTheirSide() == "high":
            goal = (0, h/2)            
        targetY = gradient*(goal[0] - position[0]) + position[1]
        return targetY < h and targetY > 0
        
