class Decision(object):
    def __init__(self, percept, action):
        self._percept = percept
        self._action = action

    def __call__(self, parent):
        if self._percept(parent):
            self._action(parent)
            return True
        return False
