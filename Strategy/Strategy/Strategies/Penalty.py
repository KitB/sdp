from ..Decision import Strategy, Decision, Percept
from ..Decision.Percept import HasBall, AlwaysTrue, BallGoalBound, TheyHaveBall, UsInGoal, ThemInGoal, BallInMiddle, CanScore, FacingGoal, ApproxAt
from ..Actions.SimpleActions import *
from ..Actions.NavigatePID import moveTo
from ..Actions.Astar import navigateTo
from ..LocalFuncs import cm2abst, getKickingPos, getDefendingPos

def penaltyDefensive(world, robotControl):
    s = Strategy(world, robotControl)
    
    
    s.attach(Decision(HasBall().add(FacingTheirSide()), kick))
    
    s.attach(Decision(HasBallMoved().add(MovingToBack()), lambda s: moveTo(s,gpos=s.getWorld().getBall().getPosition(), backwards=True)))
    
    s.attach(Decision(HasBallMoved().add(MovingToBack().negate()), lambda s: moveTo(s,gpos=s.getWorld().getBall().getPosition(), backwards=False)))   
    
    s.attach(Decision(BallMoving().add(MovingToBack()), lambda s: moveTo(s,gpos=s.getWorld().getBall().getPosition(), backwards=True)))
    
    s.attach(Decision(BallMoving().add(MovingToBack().negate()), lambda s: moveTo(s,gpos=s.getWorld().getBall().getPosition(), backwards=False)))

    return s


def penaltyOffensiveLeft(world, robotControl):

    s = Strategy(world, robotControl)
    
    s.attach(Decision(JustOnce(), lambda s: robotControl.penalty(1)))
    
    # TODO: Add code which calls the appropriate penalty mode

    return s
    
def penaltyOffensiveRight(world, robotControl):

    s = Strategy(world, robotControl)
    
    s.attach(Decision(JustOnce(), lambda s: robotControl.penalty(3)))
    
    # TODO: Add code which calls the appropriate penalty mode

    return s    
