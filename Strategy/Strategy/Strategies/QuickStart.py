from Strategy.Decision import Strategy, Decision, Percept
from Strategy.Decision.Percept import HasBall, AlwaysTrue, BallGoalBound, TheyHaveBall, UsInGoal
from Strategy.Actions.SimpleActions import *
from Strategy.Actions.NavigatePID import navigatePID

def makeMatchStrategy(world, robotControl):
    """ Return a match strategy - Not functional yet """
    s = Strategy(world, robotControl)
    
