from ..Decision import Strategy, Decision, Percept
from ..Decision.Percept import HasBall, AlwaysTrue, BallGoalBound, TheyHaveBall, UsInGoal, ThemInGoal, BallInMiddle, CanScore, FacingGoal, ApproxAt
from ..Actions.SimpleActions import *
from ..Actions.NavigatePID import moveTo
from ..Actions.Astar import navigateTo
from ..LocalFuncs import cm2abst, getKickingPos, getDefendingPos

def test(world, robotControl):
    """ Change to test the program """
    s = Strategy(world, robotControl)
        
    s.attach(Decision((NextToWall().add(FacingWall())), lambda s: moveTo(s, backwards=True)))

  
    s.attach(Decision(AlwaysTrue(), moveTo))

    return s

def experimental(world, robotControl):
    """ Change to test the program """
    s = Strategy(world, robotControl)
        
    s.attach(Decision(AlwaysTrue(), lambda s: moveTo(s,backwards=False)))

    return s

def moveToPrediction(w, robotControl):
    s = Strategy(w, robotControl)

    s.attach(Decision(AlwaysTrue(), printPrediction))

    return s
    
def printVision(world,robotControl):
    s = Strategy(world, robotControl)
    s.attach(Decision(AlwaysTrue(), printVisionData))
    
    return s


def drawPrediction(world, robotControl):
    s = Strategy(world, robotControl)
    s.attach(Decision(AlwaysTrue(), debugPrediction) )
    return s

def catchBall(world, robotControl):
    s = Strategy(world, robotControl)
    s.attach(Decision(AlwaysTrue(), catch_ball ) )
    return s
