from ..Decision import Strategy, Decision, Percept
from ..Decision.Percept import HasBall, AlwaysTrue, BallGoalBound, TheyHaveBall, UsInGoal, ThemInGoal, BallInMiddle, CanScore, FacingGoal, ApproxAt
from ..Actions.SimpleActions import *
from ..Actions.NavigatePID import moveTo
from ..Actions.Astar import navigateTo
from ..LocalFuncs import cm2abst, getKickingPos, getDefendingPos

def friendly2(world, robotControl):
    """ Return a match strategy - Needs testing """
    s = Strategy(world, robotControl)
        
   
    
    s.attach(Decision(HasBall().add(FacingTheirSide()), kick))
        #when not facing goal - go to kicking position
    s.attach(Decision(HasBall().add(FacingTheirSide().negate()), lambda s: moveTo(s,gpos=getKickingPos(world))))
    
    s.attach(Decision(OnGoalLine(), moveTo))
    
    s.attach(Decision(ApproxAt(goalPoint=getKickingPos(world),distance_threshold=cm2abst(15)), moveTo ))
    
    s.attach(Decision(HasBall().negate(), navigateTo))
    
    #we are in the kicking position - move towards ball



    return s

