from ..Decision import Strategy, Decision, Percept
from ..Decision.Percept import HasBall, AlwaysTrue, BallGoalBound, TheyHaveBall, UsInGoal, ThemInGoal, BallInMiddle, CanScore, FacingGoal, ApproxAt, ApproxAtDefendingPos, ApproxAtKickingPos, FacingTheirSide, FacingWall, NextToWall
from ..Actions.SimpleActions import *
from ..Actions.NavigatePID import moveTo
from ..Actions.Astar import navigateTo
from ..LocalFuncs import cm2abst, getKickingPos, getDefendingPos

def makeMatchStrategy(world, robotControl):
    """ Return a match strategy - Needs testing """
    s = Strategy(world, robotControl)

    # Rush forwards     
    s.attach(Decision(JustOnce(), doStartRush ))
    
    # Kick
    s.attach(Decision(HasBall().add(FacingTheirSide(facingAngle=pi/3)), kick))      
      
    # Turn for kick    
    s.attach(Decision(HasBall().add(FacingTheirSide(facingAngle=pi/3).negate()), lambda s: moveTo(s,gpos=world.getTheirGoalpost(), maxspeed=110))) # Turn less

    # Defend
    s.attach(Decision(TheyHaveBall(), lambda s: moveTo(s,gpos=getDefendingPos(world))))

    # bumping into wall
    s.attach(Decision(FacingWall().add(NextToWall().add(HasBall().negate())), lambda s: moveTo(s,gpos=(0,0), maxspeed=70) ))

    # Defensive moving towards ball
    s.attach(Decision(ApproxAtDefendingPos(distance_threshold=cm2abst(30)).add(TheyHaveBall()), lambda s: moveTo(s,maxspeed=40) ))

    # Goto Ball
    s.attach(Decision(AlwaysTrue(), moveTo))

    return s
