from ..Decision import Strategy, Decision, Percept
from ..Decision.Percept import HasBall, AlwaysTrue, BallGoalBound, TheyHaveBall, UsInGoal, ThemInGoal, BallInMiddle, CanScore, FacingGoal, ApproxAt
from ..Actions.SimpleActions import *
from ..Actions.NavigatePID import moveTo
from ..Actions.Astar import navigateTo
from ..LocalFuncs import cm2abst, getKickingPos, getDefendingPos

def friendly3(world, robotControl):
    """ Return a match strategy"""
    s = Strategy(world, robotControl)
        
    s.attach(Decision(AlwaysTrue(), moveTo))



    return s

