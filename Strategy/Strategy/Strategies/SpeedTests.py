from Strategy.Decision import Strategy, Decision, Percept
from Strategy.Decision.Percept import JustOnce, HasBall, AlwaysTrue, BallGoalBound, TheyHaveBall, UsInGoal, ThemInGoal, BallInMiddle, CanScore, FacingGoal, ApproxAt
from Strategy.Actions.SimpleActions import *
from Strategy.Actions.NavigatePID import moveTo
from Strategy.Actions.Astar import navigateTo
from Strategy.LocalFuncs import cm2abst, getKickingPos, getDefendingPos

def straightSpeedTest(world, robotControl):
    """ Get velocity when moving forward at 40 """
    s = Strategy(world, robotControl)
        
    s.attach(Decision(JustOnce(), straightSpeedTest))

    return s
    
def movetoSpeedTest(world, robotControl):
    """ Get velocity when moving to ball """
    s =  Strategy(world, robotControl)
    
    s.attach(Decision(AlwaysTrue(), moveTo))
    
    return s
