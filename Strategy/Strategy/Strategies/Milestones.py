from ..Decision import Strategy, Decision, Percept
from ..Decision.Percept import HasBall, AlwaysTrue, BallGoalBound, TheyHaveBall, UsInGoal
from ..Actions.SimpleActions import *
from ..Actions.NavigatePID import moveTo
from ..Actions.Astar import navigateTo
from ..LocalFuncs import computeIntersection, predictInterceptBasedOnOpponentOrientation

def makeFollowStrategy(world, robotControl):
    """ Return a strategy that will follow the ball """
    s = Strategy(world, robotControl)
    s.attach(Decision(HasBall(), lambda s: s.getRobot().stop()))
    s.attach(Decision(HasBall().negate(), navigate))
    return s

def makeFollowKickStrategy(world, robotControl):
    """ Return a strategy that will go to the ball and kick """
    s = Strategy(world, robotControl)
    s.attach(Decision(HasBall(), lambda s: s.getRobot().kick()))
    s.attach(Decision(HasBall().negate(), navigate))
    return s

def makeAstarKickStrategy(world, robotControl):
    """ Return a strategy that will navigate to the ball, avoiding obstacles, and kick """
    s = Strategy(world, robotControl)
    s.attach(Decision(HasBall(), lambda s: s.getRobot().kick()))
    s.attach(Decision(HasBall().negate(), navigateTo))
    return s

def makeMoveToPointStrategy(world, robotControl):
    """ Return a strategy that will move to a point? """
    s = Strategy(world, robotControl)
    s.attach(Decision(HasBall(), lambda s: s.finish() ))
    s.attach(Decision(HasBall().negate(), move_to_point_act))
    return s

def makeNavigatePIDStrategy(world, robotControl):
    """ Return a strategy that navigates to the ball using PID control """
    s = Strategy(world, robotControl)
    s.attach(Decision(HasBall(), lambda s: s.getRobot().kick()))
    s.attach(Decision(HasBall().negate(), moveTo))
    return s

    

def makeMilestoneFourStrategy(world, robotControl):
    """ This is what we actually used """
    s = Strategy(world, robotControl)
    
    # set initial goal to move to the prediction
    (rx,ry) = world.getUs().getPosition()    
    s.setExtra('Goal Position', (-800, 0))    
    s.attach(Decision(JustOnce().add(BallMoving()), lambda s: changeGoal(s, computeIntersection(world))))
    s.attach(Decision(HasBall().add(FacingGoal()), kick)) 
    s.attach(Decision(WithinGoal(distThresh=200), lambda s: moveToInterception(s,maxspeed = 40))) 
    s.attach(Decision(HasBallMoved().add(ApproxAt(world.getBall().getPosition(), distance_threshold=600)), lambda s: moveTo(s,maxspeed = 40)))
 
    s.attach(Decision(BallMoving(), moveToInterception))    # speed 90 
    s.attach(Decision(HasBallMoved(), lambda s: navigateTo(s,maxspeed = 40))) 
    s.attach(Decision(AlwaysTrue(), moveToGoalPos))  
    return s
    
 #####    
def makeMilestoneFourStrategyOptional(world, robotControl):
    """ Intended strategy, but there were issues with order of commands not being strictly followed """
    s = Strategy(world, robotControl)
    (rx, ry) = world.getUs().getPosition()
    
    # set initial goal to move to the prediction
    prediction = predictInterceptBasedOnOpponentOrientation(world)
    s.setExtra('Goal Position', prediction)
    s.attach(Decision(JustOnce().add(BallMoving()), lambda s: changeGoal(s, computeIntersection(world))))
    s.attach(Decision(HasBall().add(FacingGoal()), kick)) 
    s.attach(Decision(InBallMotionLine(distThresh=150), lambda s: moveTo(s,maxspeed = 40)))
    s.attach(Decision(HasBallMoved().add(ApproxAt(world.getBall().getPosition(), distance_threshold=500)), lambda s: moveTo(s,maxspeed = 40)))
    s.attach(Decision(BallMoving(), moveToInterception))    # speed 90
    s.attach(Decision(HasBallMoved(), lambda s: navigateTo(s,maxspeed = 40)))
    s.attach(Decision(AlwaysTrue(), moveToGoalPos))
    return s    
