# Miscellaneous functions
import operator
from math import pi, sin, cos
from .Params import Params
from Shared.Funcs import simpleAngle, vecDist, vecPlus, vecAngle, vecSub, vecNormalise, rotateAbout, convert, rotate, computeKickingPosition, getRectangle




def botDistance(obj1, obj2):    # TODO: rename to objDistance
    return vecDist(obj1.getPosition(), obj2.getPosition())

def angleBetween(bot, ball):
    return relativeAngle(bot, ball.getPosition())

def relativeAngle(bot, point):
    orient = simpleAngle(bot.getAngle())
    assert abs(orient) <= pi

    alpha = vecAngle(point,bot.getPosition())
    angle = simpleAngle(alpha-orient)
    return angle

def cm2abst(point, integer=True):
    return convert(point, Params.pitchLengthCM,Params.pitchLengthAbst, integer=integer)
def abst2cm(point, integer=True):
    return convert(point, Params.pitchLengthAbst,Params.pitchLengthCM, integer=integer)
    
def abst2pixel(point, dimensions=False, integer=True):
    if dimensions:      # As dimensions of the abstract coordinates are not in abstract coordinates
        return convert(point, Params.pitchLengthAbst,Params.pitchLengthPixel, integer=integer)
    return convert(point, Params.pitchLengthAbst,Params.pitchLengthPixel, shift=1, integer=integer)
    
def pixel2abst(point, offset=(0,0), dimensions=False, integer=True, factor=1):
    if dimensions:
        return convert(point, Params.pitchLengthPixel * factor,Params.pitchLengthAbst, integer=integer)
    return convert(point, Params.pitchLengthPixel * factor,Params.pitchLengthAbst, shift=-1, offset = offset, integer=integer)  
    
def abst2astar(point):
    return convert(point, Params.pitchLengthAbst, Params.pitchLengthAstar, shift= 1, integer=True)    
    
def astar2abst(point):
    return convert(point, Params.pitchLengthAbst, Params.pitchLengthAstar, shift= -1, integer=True)
    
    
def getKickingPos(world, distance = cm2abst(Params.kickingDistBall)):

    distance = distance + cm2abst(Params.robotLength)/2
    
    bpos = world.getBall().getPosition()    
    gpos = world.getTheirGoalpost()
    return map(int, computeKickingPosition(bpos,gpos,distance))
    
def approxAt(obj, gpos, distTol=Params.atGoalDistTol):
    dist = vecDist(obj.getPosition(), gpos)
    return dist < distTol
    
    
def getDefendingPos(world, dist = Params.defendingDist):
    pos = getKickingPos(world, -dist) #using minus to invert
    return pos
    
def getBallRobotDist(w):
    # returns the distance from the edge of the robot to the edge of the ball (dist = 0 means that the robot is touching the ball)
    pos = w.getUs().getPosition()
    bpos = w.getBall().getPosition()
    dist = vecDist(pos,bpos)
    
    return dist - cm2abst(Params.robotLength) - cm2abst(Params.ballRadius)
    
    
def getSquareInFrontOfBall(world, width=cm2abst(Params.areaInFrontOfBallWidth), length= cm2abst(Params.areaInFrontOfBallLength)):
 
    pos = getKickingPos(world, -cm2abst(length/2)) #using minus to invert
    orient = vecAngle(world.getTheirGoalpost,world.getBall().getPosition())    
    return getRectangle(pos, orient, width * 0.9, length * 0.9)
    
    
def predictInterceptBasedOnOpponentOrientation(world):

    (rx,ry) = world.getUs().getPosition()
    (bx,by) = world.getBall().getPosition()
    alpha = world.getThem().getOrientation()
    
    gx = float(rx) * 0.8 # Param
    
    (cx, sy) = (cos(alpha), sin(alpha))
    
    if sy ==0:
        return (gx,by)
    
    k = (gx - bx) / cx
    gy = by+ k * sy
    
   # world.clearLayer('T goal')
   # world.addToDrawing(Cross(goal, colour=(255, 0, 255), abst=True, layer='T goal'))
    
    return (gx,gy)
     
def computeIntersection(world, gx = None):
    ball = world.getBall()
    (bx, by) = ball.getPosition()
    (vx, vy) = ball.getVelocity()    
    
    if not gx:
        (rx,ry) = world.getUs().getPosition()
        gx = float(rx) * 0.8 # Hack... avoid goalline, go closer to the centre
    else:
        gx = gx * 0.99
    
    (tx, ty) = world.getTheirGoalpost()
    
    if abs(tx - bx) > abs(tx - gx):
        gx = bx 

    
    if vx==0:
        return (gx, by)
    
    k = float(gx-bx)/vx
    gy = by + vy*k
    
    return (gx,gy)
    
def getCorners(robot, tolerance = 1):
    """ Get four robot corners, in anti-clockwise order starting with the front-left. Use tolerance to increase the virtual size of the robot."""
    
    orient = robot.getAngle()
    centroid = robot.getPosition()
    width = cm2abst(Params.robotWidth) * tolerance
    length = cm2abst(Params.robotLength) * tolerance
    relativeCentre = cm2abst(Params.robotRelCentroid)
    pos = vecPlus(centroid, rotate(relativeCentre,orient))
         
    return getRectangle(pos, orient, width, length)
