import socket
import sys
import array
from math import degrees

from ..Logging.SingleLog import Log

TAG = "ROBOTCONTROL"

class RobotControl:
    def __init__(self, host=None, port=None):
        print("Initialising robot control!")
        self.__HOST = host if not host == None else socket.gethostname()
        self.__PORT = port if not port == None else 6789
        self.__MOVE = 1
        self.__KICK = 2
        self.__TURN = 3
        self.__STOP = 4
        self.__RUSH = 5
        self.__SPEEDRO = 6
        self.__PENALTY = 7
        self.__DEFENCE = 8
        self.__EXIT = 99
        
        self.PENALTY_LEFT = 1
        self.PENALTY_MIDDLE = 2
        self.PENALTY_RIGHT = 3

        self.__socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        try:
            self.__socket.connect((self.__HOST, self.__PORT))
            print("Debug: Strategy connected to comms!")
        except socket.error as (no, str):
            Log.d(TAG, "Couldn't connect to socket: %s" % str)

    def forwards(self, motor_speed = 40):
        Log.d(TAG, "Going forwards")
        self.move(motor_speed, motor_speed)

    def backwards(self, motor_speed = 20):
        Log.d(TAG, "Going backwards")
        motor_speed = -motor_speed
        self.move(motor_speed, motor_speed)

    def stop(self):
        Log.d(TAG, "Stopping")
        command = array.array('b', [self.__STOP, 0, 0])
        self.__socket.send(command)

    def move(self, left, right):
        print("Moving!", left, right)
        #sys.stdout.write("\rMoving %4d, %4d" % (left, right))
        #sys.stdout.flush()
        command = array.array('b', [self.__MOVE, left, right])
        self.__socket.send(command)

    def speedRo(self, speed, ro):
        sys.stdout.write("\rMoving %4d, %4d" % (speed, ro))
        sys.stdout.flush()
        command = array.array('b', [self.__SPEEDRO, speed, ro])
        self.__socket.send(command)

    def turn(self, angle, radians=True):
        Log.d(TAG, "Turning by %f"%angle)
        angle = int(degrees(angle)) if radians else int(angle)
        command = array.array('B', [self.__TURN, (angle>>8)&255, (angle)&255])
        self.__socket.send(command)

    def kick(self):
        Log.d(TAG, "Kicking")
        command = array.array('b', [self.__KICK, 0, 0])
        self.__socket.send(command)

    def reset(self):
        Log.d(TAG, "Resetting")
        command = array.array('b', [self.__RESET, 0, 0])
        self.__socket.send(command)

    def startRush(self, dist):
        Log.d(TAG, "Rushing..")
        command = array.array('B', [self.__RUSH, (dist>>8)&255, (dist)&255])
        self.__socket.send(command)
        
    def penalty(self, direction):
        Log.d(TAG, "Penalty!")
        command = array.array('b', [self.__PENALTY, direction, 0])
        self.__socket.send(command)
        
    def defence(self):
        Log.d(TAG, "Defence!")
        command = array.array('b', [self.__DEFENCE, 0, 0])
        self.__socket.send(command)

    def exit(self):
        Log.d(TAG, "Exiting")
        command = array.array('b', [self.__EXIT, 0, 0])
        self.__socket.send(command)

class FakeRobot:
    tag = "FAKEROBOT"
    def __init__(self, host=None, port=None):
        Log.d(self.tag, "Initialising robot")

    def forwards(self):
        Log.d(self.tag, "Going forwards")

    def backwards(self):
        Log.d(self.tag, "Going backwards")

    def stop(self):
        Log.d(self.tag, "Stopping")

    def turn_around(self):
        Log.d(self.tag, "Turning around")

    def kick(self):
        Log.d(self.tag, "Kicking")

    def move(self, left, right):
        Log.d(self.tag, "Moving: (%d, %d)"%(left, right))

    def turn(self, angle):
        Log.d(self.tag, "Turning by angle %f"%angle)

    def exit(self):
        Log.d(self.tag, "Exiting")
