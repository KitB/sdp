from math import pi, cos, sin
from multiprocessing import Process, Value, Array

import pygame
from pygame.locals import *

from VisionClient import VisionSource
from ..Drawing.Drawable import drawRobot, drawBall, drawPitch, blue, yellow
from ..Shared.Funcs import vecAngle, rotateAbout, simpleAngle
from ..Logging.SingleLog import Log
from ..Shared.ProcessManager import processes

TAG = "MODELVISION"

w, h = 630, 330
bx, by, ba = 60, h/2, 0
yx, yy, ya = w-60, h/2, pi
ballx, bally = w/2, h/2


def inCircle((px, py), (cx, cy)):
    r = 20
    hr = r/2
    x1, y1 = cx-hr, cy-hr
    x2, y2 = cx+hr, cy+hr
    inX = x1 < px and px < x2
    inY = y1 < py and py < y2
    return inX and inY

def inRobot(pos, (rx, ry, ra)):
    w, h = 100, 60
    hw, hh = w/2, h/2
    rpx, rpy = rotateAbout(pos, (rx, ry), -ra)
    x1, y1 = rx-hw, ry-hh
    x2, y2 = rx+hw, ry+hh
    inX = x1 < rpx and rpx < x2
    inY = y1 < rpy and rpy < y2
    return inX and inY

def getOrientPoint((x, y, a)):
    dx, dy = 50*cos(a), 50*sin(a)
    return x+dx, y+dy

class ModelVision(VisionSource):
    def __init__(self):
        self._done = Value('b', False)
        self._blue_pos = Array('i', [bx, by])
        self._blue_angle = Value('d', ba)
        self._yellow_pos = Array('i', [yx, yy])
        self._yellow_angle = Value('d', ya)
        self._dims = (w, h)
        self._ball = Array('i', [ballx, bally])
        self._control_process = Process(target=self.mainloop, args=(self._blue_pos, self._blue_angle, self._yellow_pos, self._yellow_angle, self._ball, self._done))
        self._control_process.start()
        processes.add("model vision", self._control_process)

    def getBlueBot(self):
        return (self._blue_pos[0], self._blue_pos[1], simpleAngle(self._blue_angle.value))

    def getYellowBot(self):
        return (self._yellow_pos[0], self._yellow_pos[1], simpleAngle(self._yellow_angle.value))

    def getBall(self):
        return (self._ball[0], self._ball[1])

    def isDone(self):
        return self._done.value

    def finish(self):
        self._done.value = True

    def draw(self):
        screen = self._screen

        # Draw the background
        screen.fill((70, 111, 32))

        # Draw the pitch
        drawPitch(screen, *self._dims)

        # Draw the ball
        drawBall(screen, *self._ball)

        # Draw the robots
        drawRobot(screen, blue, *self._blue)
        drawRobot(screen, yellow, *self._yellow)

        pygame.display.update()

    def getMouseOnObject(self):
        pos = pygame.mouse.get_pos()
        if inCircle(pos, self._ball):
            return 'r'
        elif inCircle(pos, getOrientPoint(self._blue)):
            return 'ba'
        elif inCircle(pos, getOrientPoint(self._yellow)):
            return 'ya'
        elif inRobot(pos, self._blue):
            return 'b'
        elif inRobot(pos, self._yellow):
            return 'y'
        else:
            return None

    def mainloop(self, blue_pos, blue_angle, yellow_pos, yellow_angle, ball, done):
        self._blue = (blue_pos[0], blue_pos[1], blue_angle.value)
        self._yellow = (yellow_pos[0], yellow_pos[1], yellow_angle.value)
        self._ball = (ball[0], ball[1])
        pygame.init()
        self._screen = pygame.display.set_mode(self._dims)
        self._clock = pygame.time.Clock()
        obj = None
        while not done.value:
            self._clock.tick(60)
            try:
                for event in pygame.event.get():
                    if event.type == QUIT:
                        done.value = True
                    elif event.type == MOUSEBUTTONDOWN:
                        obj = self.getMouseOnObject()
                    elif event.type == MOUSEMOTION:
                        mx, my = pygame.mouse.get_pos()
                        if obj == 'b':
                            ba = self._blue[2]
                            self._blue = mx, my, ba
                            blue_pos[0] = mx
                            blue_pos[1] = my
                        elif obj == 'y':
                            ya = self._yellow[2]
                            self._yellow = mx, my, ya
                            yellow_pos[0] = mx
                            yellow_pos[1] = my
                        elif obj == 'r':
                            self._ball = mx, my
                            ball[0] = mx
                            ball[1] = my
                        elif obj == 'ba':
                            bx, by = self._blue[0:2]
                            newangle = vecAngle((bx, by), (mx, my))+pi
                            self._blue = (bx, by, newangle)
                            blue_angle.value = newangle
                        elif obj == 'ya':
                            yx, yy = self._yellow[0:2]
                            newangle = vecAngle((yx, yy), (mx, my))+pi
                            self._yellow = (yx, yy, newangle)
                            yellow_angle.value = newangle
                    elif event.type == KEYDOWN:
                        if event.key == K_e:
                            done.value = True

                    elif event.type == MOUSEBUTTONUP:
                        obj = None
                self.draw()
            except KeyboardInterrupt:
                Log.d(TAG, "User exited")
                done.value = True

if __name__ == "__main__":
    import sys
    m = ModelVision()
    while not m.isDone():
        try:
            bluestr = "b(%3d, %3d, %3.2f)"%m.getBlueBot()
            yellowstr = "y(%3d, %3d, %3.2f)"%m.getYellowBot()
            ballstr = "r(%3d, %3d)"%m.getBall()
            sys.stdout.write("\r%s\t%s\t%s"%(bluestr, yellowstr, ballstr))
            sys.stdout.flush()
        except KeyboardInterrupt:
            Log.d(TAG, "User exit")
            m.finish()
