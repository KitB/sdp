# VisionClient provides a high-level interface to a socket IPC system

# When instantiated this class will spin off a thread that will listen on the socket, updating positions for each object as it goes.

import socket
import struct
from math import pi
import time

from ..Params import Params
from ..LocalFuncs import pixel2abst
from ..Logging.SingleLog import Log

from ..LocalExceptions import AbstractClassInstantiatedError, abstract

TAG = "VISIONCLIENT"

MESSAGE_LENGTH=30

RQ_BLUE = 1
RQ_YELLOW = 2
RQ_RED = 3
RQ_DIMENSIONS = 4
RQ_FPS = 5

class VisionSource(object):
    """ An abstract class representing a source for vision data """
    def __init__(self):
        abstract(self)
    def getBlueBot(self):
        """ Return the position and angle of the blue robot """
        return self._blue
    def getYellowBot(self):
        """ Return the position and angle of the yellow robot """
        return self._yellow
    def getBall(self):
        """ Return the position of the ball """
        return self._ball
    def getDimensions(self):
        """ Return the dimensions of the pitch """
        return self._dims

dummyVals = { 'blue' : (230, 250, pi)
            , 'yellow' : (120, 360, pi/4)
            , 'ball' : (50, 70)
            , 'dims' : (631, 331)
            }

class DummyVision(VisionSource):
    """ A fake vision source that serves constant data """
    def __init__(self, v):
        self.blue = v['blue']
        self.yellow = v['yellow']
        self.ball = v['ball']
        self.dims = v['dims']
    def getBlueBot(self):
        return self.blue
    def getYellowBot(self):
        return self.yellow
    def getBall(self):
        return self.ball
    def getAbstractDims(self):
        return self.dims

class VisionClient(VisionSource):
    """ The real vision source that serves vision data acquired from a vision server """
    def __init__(self, host=None, tryPort=31410, usingSim=False):
        # We'll be trying up to twenty higher than the given port
        p = tryPort
        h = host if host != None else socket.gethostname()
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        connected = False
        while not connected:
            try:
                s.connect((h, p))
                Log.d(TAG, "VISION: Connected on port %d" % p)
                connected = True
            except socket.error:
                if not p-tryPort > 20:
                    p += 1
                else:
                    raise

        self._factor = 100    # Assuming vision is using factor 100 - for rounding purposes
        self._port = p
        self._socket = s
        (cx,cy,dx,dy) = self.getVisionDimensions()
        self._offset = (self._factor*(cx-dx),self._factor*(cy-dy))    # top left corner coordinates
    def getOffset(self):
        return self._offset
    def getFactor(self):
        return self._factor

    def getSocket(self):
        """ Returns the socket we're listening on """
        return self._socket

    # Request functions {
    # TODO: Empty input stream before sending request
    def getBlueBot(self):
        self.sendLine(struct.pack('b', RQ_BLUE))
        (type, x, y, tp, ta, a) = self.receive()
        assert type == 'b', "Wrong type returned: expected 'b'; given %s"%type
        (x,y) = pixel2abst((x,y), offset=self.getOffset(), factor=self.getFactor())
        return (x, y, a+pi, tp, ta)

    def getYellowBot(self):
        self.sendLine(struct.pack('b', RQ_YELLOW))
        (type, x, y, tp, ta, a) = self.receive()
        assert type == 'y', "Wrong type returned: expected 'y'; given %s"%type
        (x,y) = pixel2abst((x,y), offset=self.getOffset(), factor=self.getFactor())
        return (x, y, a+pi, tp, ta)

    def getBall(self):
        self.sendLine(struct.pack('b', RQ_RED))
        (type, x, y, _, ta, _) = self.receive()
        assert type == 'r', "Wrong type returned: expected 'r'; given %s"%type
        (x,y) = pixel2abst((x,y), offset=self.getOffset(), factor=self.getFactor())
        return (x, y, ta)

    def getFps(self):
        self.sendLine(struct.pack('b', RQ_FPS))
        (type, x) = self.receive_fps()
        assert type == 'f', "Wrong type returned: expected 'f'; given %s"%type
        return x
        
    def getAbstractDims(self):
        return (Params.pitchLengthAbst, Params.pitchWidthAbst)
        
    def getDimensions(self):    # returns (w,h)
        return (self._pitchDims)

    def getVisionDimensions(self):
        self.sendLine(struct.pack('b', RQ_DIMENSIONS))
        (type, cx, cy, dx, dy) = self.receiveDims()
        Params.pitchLengthPixel = dx*2
        Params.pitchWidthPixel = dy*2        
        self._pitchDims = (Params.pitchWidthPixel, Params.pitchLengthPixel)
        Params.writeFile()
        assert type == 'd', "Wrong type returned: expected 'd'; given %s"%type
        return (cx, cy, dx, dy)     # centre point, and the distance to the end of the pitch

    def receive(self):
        data = self._socket.recv(MESSAGE_LENGTH)
        #assert len(data) == MESSAGE_LENGTH, "Wrong length, expected %d given %d"%(MESSAGE_LENGTH, len(data))
        return struct.unpack('cHHddf', data[:-2])

    def receiveDims(self):
        data = self._socket.recv(12)
        assert len(data) == 12, "Wrong length, expected 12 given %d"%len(data)
        return struct.unpack('cHHHH', data[:-2])
    # }

    def receive_fps(self):
        data = self._socket.recv(10)
        assert len(data) == 10, "Wrong length, expected %d given %d"%(10, len(data))
        return struct.unpack('cf', data[:-2])
        
    def sendLine(self, content):
        toSend = "%s\r\n" % content
        self._socket.send(toSend)

if __name__ == '__main__':
    import pygame.time
    vc = VisionClient('', 31410, False)
    while True:
            pygame.time.Clock().tick(10)
            Log.d(TAG, "b(%d,%d)"%vc.getBlueBot())
