class AbstractClassInstantiatedError(Exception):
    def __init__(self, className):
        if isinstance(className, basestring):
            self.className = className
        else:
            self.className = className.__class__.__name__
    def __str__(self):
        return "Attempted to instantiate abstract class %s"%self.className
def abstract(instance):
    raise AbstractClassInstantiatedError(instance.__class__.__name__)

class TimeOut(Exception):
    def __init__(self):
        pass
    def __str__(self):
        return "Process timed out"
