from ..Simulator.Model.WorldObjects import Ball, Robot
from ..Simulator.Util.Lookahead import *
from ..Simulator.Simulation import Simulation
from ..LocalFuncs import abst2pixel
from ..LocalFuncs import pixel2abst
import math

class Predictor:

    def __init__(self, world):
        self.__world = world
        self.__LOOK_AHEAD = 100 # ideally infinity

        br = self.__world.getBlueRobot()
        yr = self.__world.getYellowRobot()
        ball = self.__world.getBall()

        objects = {}
        objects['ball'] = Ball( abst2pixel(ball.getPosition()), abst2pixel(ball.getVelocity(),dimensions=True))
        objects['blue'] = Robot(abst2pixel(br.getPosition()), abst2pixel(br.getVelocity(),dimensions=True), br.getOrientation(), "blue")
        objects['yellow'] = Robot(abst2pixel(yr.getPosition()), abst2pixel(yr.getVelocity(),dimensions=True), yr.getOrientation(), "yellow")

        self.__sim = Simulation(objects=objects)
        self.__sim.set_us_color(world.getUs().getColour())



    def __get_key_frame(self):
        br = self.__world.getBlueRobot()
        yr = self.__world.getYellowRobot()
        ball = self.__world.getBall()


        return { 'blue'   : { 'position' : abst2pixel(br.getPosition())
                            , 'velocity' : abst2pixel(br.getVelocity(),dimensions=True)
                            , 'angle'    : br.getOrientation()
                            }
               , 'yellow' : { 'position' : abst2pixel(yr.getPosition())
                            , 'velocity' : abst2pixel(yr.getVelocity(),dimensions=True)
                            , 'angle'    : yr.getOrientation()
                            }
               , 'ball'   : { 'position' : abst2pixel(ball.getPosition())
                            , 'velocity' : abst2pixel(ball.getVelocity(),dimensions=True)
                            }
               }


    def get_intersection_ball_us(self, timeout = 3):
        self.__sim.keyFrame(self.__get_key_frame(), True)
        return pixel2abst(get_intersection_ball_us(self.__sim, timeout))

    def get_ball_position(self, timeout = 1):
        self.__sim.keyFrame(self.__get_key_frame())
        return pixel2abst(get_ball_pos_after(self.__sim, timeout))