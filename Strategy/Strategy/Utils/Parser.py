from optparse import OptionParser
from Default import string2Strategy

parser = OptionParser()

parser.add_option( "--vision-port"
                 , dest="vision_port"
                 , action="store"
                 , type="int"
                 , help="Which port to connect on for vision"
                 , default=31410
                 )
parser.add_option( "--vision-host"
                 , dest="vision_host"
                 , action="store"
                 , type="string"
                 , help="Which host to connect to for vision"
                 , default=''
                 )
parser.add_option( "--comms-port"
                 , dest="comms_port"
                 , action="store"
                 , type="int"
                 , help="Which port to connect on for robot communication"
                 , default=6789
                 )
parser.add_option( "--comms-host"
                 , dest="comms_host"
                 , action="store"
                 , type="string"
                 , help="Which host to connect to for robot communication"
                 , default=''
                 )

parser.add_option( "-s", "--strategy"
                 , dest="strategy"
                 , choices=string2Strategy.keys()
                 , help="Which action to perform"
                 , default='match'
                 )
parser.add_option( "-r", "--robot"
                 , dest="robot"
                 , choices=["b", "y"]
                 , help="Which colour our robot is"
                 )
parser.add_option( "-S", "--side"
                 , dest="side"
                 , choices=["high", "low"]
                 , help="Which side of the pitch we are on, measured in x value"
                 )
parser.add_option( "-D", "--debug"
                 , dest="debug"
                 , action="store_true"
                 , default=True
                 , help="Output extra debugging info including a graphical redrawing of vision data"
                 )
parser.add_option( "-F", "--fake"
                 , dest="fake"
                 , action="store_true"
                 , default=False
                 , help="Use fake vision and communication"
                 )
parser.add_option( "-P", "--penalty"
                 , dest="penalty"
                 , action="store_true"
                 , default=False
                 , help="Play offensive penalty strategy"
                 )
parser.add_option( "-W", "--wait"
                 , dest="wait"
                 , action="store_true"
                 , help="Wait for user input before finally running the strategy"
                 , default = True
                 )
parser.add_option( "--no-robot"
                 , dest="no_robot"
                 , action="store_true"
                 , default=False
                 , help="Use a fake robot, for when we just want to test vision or planning"
                 )

def getOptions():
    (options, args) = parser.parse_args()
    return options
