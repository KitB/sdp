from WorldObjects import *
from VisionClient import *
from Strategy import angleBetween

vc = VisionClient()
bot = Robot(*vc.getBlueBot())
ball = Ball(*vc.getBall())

print angleBetween(bot, ball)
