from ..Model.World import World
from ..Communication.VisionClient import VisionClient
from ..Communication.ModelVision import ModelVision
from ..Communication.Communication import RobotControl, FakeRobot
from ..Strategies.Milestones import makeFollowStrategy, makeFollowKickStrategy, makeAstarKickStrategy, makeMoveToPointStrategy, makeNavigatePIDStrategy, makeMilestoneFourStrategy
from ..Strategies.Friendly3 import friendly3
from ..Strategies.Friendly2 import friendly2
from ..Strategies.Test import test, experimental, printVision, moveToPrediction, drawPrediction, catchBall
from ..Strategies.Match import makeMatchStrategy
from ..Strategies.Penalty import penaltyDefensive, penaltyOffensiveLeft, penaltyOffensiveRight


string2Strategy = { 'match'         : makeMatchStrategy
                  , 'follow'        : makeFollowStrategy
                  , 'follow-kick'   : makeFollowKickStrategy
                  , 'astar-kick'    : makeAstarKickStrategy
                  , 'move-to-point' : makeMoveToPointStrategy
                  , 'navigate-pid'  : makeNavigatePIDStrategy
                  , 'intercept'     : makeMilestoneFourStrategy
                  , 'friendly3'     : friendly3
                  , 'friendly2'     : friendly2
                  , 'test'          : test
                  , 'experimental'  : experimental
                  , 'print'         : printVision
                  , 'predictor'     : moveToPrediction
                  , 'draw-prediction': drawPrediction
                  , 'catch-ball'    : catchBall
                  , 'defendPenalty' : penaltyDefensive
                  , 'attackPenaltyLeft' : penaltyOffensiveLeft
                  , 'attackPenaltyRight' : penaltyOffensiveRight
                  }

def fillStrategy(strategyFunction, options):
    if options.fake:
        vc = ModelVision()
        r = FakeRobot()
    else:
        vc = VisionClient(options.vision_host, options.vision_port)
        if options.no_robot:
            r = FakeRobot()
        else:
            r = RobotControl(options.comms_host, options.comms_port)

    w = World(vc, options.debug)
    w.setPlayer(options.robot)
    w.setSide(options.side)

    return strategyFunction(w, r)

def optionsToStrategy(options):
    return fillStrategy(string2Strategy[options.strategy], options)
