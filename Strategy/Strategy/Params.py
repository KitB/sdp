# This file stores only parameters. If you want to use a parameter, import it in the file you want to use it in. For example: 'import Params', followed by circleDist = Params.circleDist in your code later on. 
import atexit
from math import pi
import sys
from inspect import getsourcelines, getfile, getsourcefile
import re
import pyinotify
from Shared.Params import ParameterStore

class LParams(ParameterStore):
    pass

Params = LParams('Params')

# Percept
Params.hasBallDistTol = 150
Params.hasBallAngleTol = 0.34999999999999998
Params.ballInMiddleDistTol = 30
Params.facingGoalAngleTol = 0.5
Params.canScoreBallGap = 50
Params.atGoalDistTol = 300
Params.defendingDist = 100
Params.nextToWallDist = 150

# Actions
Params.navigateAngleTol = 1.0471975512

# Robot (in cm)
Params.robotWidth = 18
Params.robotLength = 20
Params.robotRelCentroid = (0, 0)
Params.robotSizeToleranceAvoiding = 2.0

# Ball (in cm)
Params.ballRadius = 1.7

# Pitch (in cm)
Params.pitchWidthCM = 122
Params.pitchLengthCM = 244

# Pitch (in Abstract Units)
Params.pitchWidthAbst = 1000
Params.pitchLengthAbst = 2000

# Pitch (in pixels)         # FIXME: This is not a parameter, unless simulator is used!
Params.pitchWidthPixel = 320
Params.pitchLengthPixel = 620

# Pitch (in Astar Planning Units)
Params.pitchWidthAstar = 1000
Params.pitchLengthAstar = 2000

# Number of previous positions, velocities stored
Params.nPrevPos = 5

# The distance to the ball for kicking (in cm)
Params.kickingDistBall = 15

Params.areaInFrontOfBallLength = 10
Params.areaInFrontOfBallWidth  = 6

# Velocity update vector (taking into account previous positions
Params.velocityUpdateCoef = [0.050000000000000003, 0.10000000000000001, 0.25, 0.59999999999999998]
Params.orientationUpdateCoef = [0.02, 0.029999999999999999, 0.050000000000000003, 0.14999999999999999, 0.75]


# PID parameters (1-30, 31-60,61-90)
Params.KpCoefficient = [2.5, 2, 2]
Params.KiCoefficient = [0.1, 0, 0.3]
Params.KdCoefficient = [0.5, 0.1, 0.3]
Params.moveSpeed = 70
Params.K = 10
# NOTE: speeds 25, 40, 70
    # for speed 90:
    #           3, 0.3, 2.3  (2 - 2.5 for D)
    #           2.9, 0.3, 1
    #           2.8, 0.3, 1.7

Params.movingBallSpeedThresh = 70
Params.inBallMotionLineTol = 200
Params.withinGoalTol = 150
Params.closeness  = 600
Params.ballPredictionDistRatio = 800

# Other
Params.strategyFPS = 25

# How long to rush for (in seconds)
Params.rushTime = 1.3999999999999999

# Constants
Params.turningConst = 1.4444444439999999

Params.pixelsToCm = 6.0999999999999996
