from collections import deque
from math import cos, sin, pi, hypot
import operator

import pygame
import numpy

from ..Shared.Funcs import rotateAbout, vecDist, vecAngle, vecNorm, weightNorm, vecPlus, simpleAngle
from ..LocalFuncs import abst2pixel, getCorners, approxAt, cm2abst
from ..Drawing.Drawable import Drawable, drawRobot
from ..Params import *



class WorldObject:

    def __init__(self, visionSource, obj):
        assert obj in ['b', 'y', 'r'], "Expected obj as ['b', 'y', 'r'], got: '%s'"%obj
        self._vc = visionSource
        self.positions = deque(maxlen=Params.nPrevPos)
        self.velocity = (0,0)
        self._obj = obj

        if self._obj == 'b':
            self.positions.append(self._vc.getBlueBot())
        elif self._obj == 'y':
            self.positions.append(self._vc.getYellowBot())
        elif self._obj == 'r':
            self.positions.append(self._vc.getBall())

    def update(self):
        if self._obj == 'b':
            state = self._vc.getBlueBot()
            if not state[3] == self.positions[-1][3]:
                self.positions.append(state)
            self.computeOrientation() 
        elif self._obj == 'y':
            state = self._vc.getYellowBot()
            if not state[3] == self.positions[-1][3]:
                self.positions.append(state)
            self.computeOrientation() 
        elif self._obj == 'r':
            state = self._vc.getBall()
            if not state[2] == self.positions[-1][2]:
                self.positions.append(state)
        self.computeVelocity()  
  

    def getPosition(self):
        return self.positions[-1][:-1]
        
    def getSpeed(self):
        """Returns a scalar """
        vel = self.getVelocity()
        return hypot(*vel)
        
    def getVelocity(self):
        return self.velocity  

    def computeVelocity(self):
        """ Finds velocity vector of the robot, given previous positions. 
        Uses parameter to weigh how important the previous positions are.
        The norm of the vector is the speed in abstract/seconds """

        coef = numpy.array(weightNorm( Params.velocityUpdateCoef))
        pos = map(lambda x: x[0:2],self.positions)

        if len(pos)<Params.nPrevPos:
            return (0,0)
        l = Params.nPrevPos - len(coef) -1
        vels = map(lambda ((x1,y1),(x2,y2)): (x1-x2,y1-y2), zip(pos[1+l:],pos[l:]))
        estimate = map(sum,zip(*map(lambda (x,(a,b)): (x*a,x*b),zip(coef, vels))))
        velocity = numpy.array(map(int,(estimate)))

        if self._obj == 'r':
            pos_index = 2
        else:
            pos_index = 3

        # extract the timestamps
        pos_timestamps = numpy.array(map(lambda x: x[pos_index], self.positions))

        # calculate the delta of the timestamps, by taking into accout the weights
        t_delta = sum((pos_timestamps[1+l:] - pos_timestamps[l:-1]) * coef)

        # speed in secods
        self.velocity = (velocity/t_delta).tolist()
        
  #TODO:  def getAcceleration(self):

    def getPreviousPosition(self, n=1):
        assert n <= Params.nPrevPos and n > 0, "Expected n in range 1-Params.nPrevPos"
        return self.positions[-n]


    def getColour(self):
        if self._obj == "b":
            return "blue"
        elif self._obj == "y":
            return "yellow"
        else:
            return "ball"

class Robot(WorldObject, Drawable):

    def __init__(self, visionSource, colour):
        assert colour in ['blue', 'yellow'], "Robots can only be blue or yellow, given \"%s\""%colour
        if colour == 'blue':
            WorldObject.__init__(self, visionSource, 'b')
            self._colour = ( 12,170,205) # Blue colour
        elif colour == 'yellow':
            WorldObject.__init__(self, visionSource, 'y')
            self._colour = (234,234, 45) # Yellow colour
        self.update()

    def getAngle(self):
        return self._orientation

    def getKickerPosition(self):
        (x,y) = self.getPosition()
        offset = cm2abst(Params.robotLength) / 2
        orient = self.getOrientation()
        return (x + int(cos(orient) * offset), y+ int(sin(orient) * offset))


    def getOrientation(self):
        return self.getAngle()

    def average2orients(self, (v1,cf1), (v2,cf2)):
        v1 = simpleAngle(v1)
        v2 = simpleAngle(v2)

        (c1,c2) = weightNorm((cf1,cf2))
        if abs(v1-v2) > pi:
            if v1 > v2:
                v2 += pi*2
            else:
                v1 += pi*2
        val = simpleAngle(v1*c1 + v2*c2) 

        return (val, cf1+cf2)
 
    def computeOrientation(self):
        coef = weightNorm(Params.orientationUpdateCoef)
        orientations = map(lambda x: x[2], self.positions)
        (angle, coeficient) = reduce(self.average2orients, zip(orientations,coef), (0,0))
        self._orientation = angle

    def getPreviousAngle(self, n=1):
        assert n <= Params.nPrevPos and n > 0, "Expected n in range 1-Params.nPrevPos"
        return self.positions[-n][2]

    def getCorners(self,tolerance = 1):
        return getCorners(self,tolerance)

    def getPosition(self):
        return self.positions[-1][0:2]

    def getPreviousPosition(self, n=1):
        assert n <= Params.nPrevPos and n > 0, "Expected n in range 1-Params.nPrevPos"
        return self.positions[-n][0:2]


    def draw(self, screen):
        self.update()
        (x, y) = abst2pixel(self.getPosition())
        a = self.getAngle()
        drawRobot(screen, self._colour, x, y, a)

    def at(self, dest, distTol = Params.atGoalDistTol):
        return approxAt(self, dest, distTol)



class Ball(WorldObject, Drawable):

    def __init__(self, visionSource):
        WorldObject.__init__(self, visionSource, 'r') 
        self._colour = (239, 39, 19)

    def draw(self, screen):
        self.update()

        try:
            pygame.draw.circle(screen, self._colour, abst2pixel(self.getPosition()), 5, 0)
        except TypeError:
            # These happen, we ignore them
            pass

