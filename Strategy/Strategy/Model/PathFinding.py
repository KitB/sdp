import numpy
import heapq

from ..Logging.SingleLog import Log

TAG = "PATHFINDING"

def offset(maxValue, value, offsetValue):
    if value + offsetValue < 0:
        return 0
    elif value + offsetValue > maxValue:
        return maxValue
    else:
        return value + offsetValue
      
    
class Node:

    """ Helper class for Graph. """

    def __init__(self, x, y):
        self._x = x
        self._y = y
        self._heuristicCost = 0
        self._movementCost = 0
        self._totalCost = 0
        self._parent = None
        self._blacklist = False

    def x(self):
        return self._x

    def y(self):
        return self._y

    def getHeuristicCost(self):
        return self._heuristicCost

    def getMovementCost(self):
        return self._movementCost

    def getTotalCost(self):
        return self._totalCost

    def getParent(self):
        return self._parent

    def setHeuristicCost(self, value):
        self._heuristicCost = value

    def setMovementCost(self, value):
        self._movementCost = value

    def setTotalCost(self, value):
        self._totalCost = value

    def setParent(self, node):
        self._parent = node

    def isBlackListed(self):
        return self._blacklist

    def blacklist(self, value):
        self._blacklist = value

    def toCoords(self, gridWidth, gridHeight):

        """ Returns the center point of the node (grid square). """

        x = (self._x * gridWidth) + gridWidth / 2
        y = (self._y * gridHeight) + gridHeight / 2
        return x, y


class Graph:

    def __init__(self, pitchWidth, pitchHeight, width, height):

        """ width and height define the size of our Graph in nodes. """

        self._pitchWidth = int(pitchWidth) # Safe guarding as non-integer division will cause unexpected results.
        self._pitchHeight = int(pitchHeight)
        self._gridSquareWidth = self._pitchWidth / width
        self._gridSquareHeight = self._pitchHeight / height
        self._width = width
        self._height = height
        self.us = None
        self._them = None
        self._ball = None
        self._grid = numpy.array([[Node(x,y) for y in range(height)] for x in range(width)], object)
        self._badNodes = set()


    # TODO: Check for negative points
    def getNode(self, x, y, offsetValue = 0):
        
        """ Points on the bound were causing out of array index errors due
        to grid squares starting at 0. The greater than is not strictly
        necessary but gives more room for error. """

        if x >= self._pitchWidth:
            gridx = self._width - 1
        elif x < 0:
            gridx = 0
        else:
            gridx = x / self._gridSquareWidth

        if y >= self._pitchHeight:
            gridy = self._height - 1
        elif y < 0:
            gridy = 0
        else:
            gridy = y / self._gridSquareHeight

        return self._grid[offset(self._width - 1, gridx, offsetValue)][offset(self._height - 1, gridy, offsetValue)]


    def getNeighbours(self, node):

        # Returns up to a maximum of eight neighbouring nodes.

        x, y = node.x(), node.y()
        neighbours = []
        for i in range(x - 1, x + 2):
            for j in range(y - 1, y + 2):
                if (i >= 0 and j >= 0 and i < self._width and
                    j < self._height and (i != x or j != y)):
                    neighbours.append(self._grid[i][j])
        return neighbours

    def setBall(self, x, y):
        self._ball = self.getNode(x, y)

    def setUs(self, x, y):
        self._us = self.getNode(x, y)

    def setThem(self, x, y):
        self._them = self.getNode(x, y)
        

    def getRect(self, points, offset = 0):
        BLx, BLy = min(points, key=lambda x: x[0])[0], min(points, key=lambda x: x[1])[1]
        TRx, TRy = max(points, key=lambda x: x[0])[0], max(points, key=lambda x: x[1])[1]
        BL = self.getNode(BLx, BLy, -(offset))
        TR = self.getNode(TRx, TRy, offset)
        nodes = []
        for i in range(BL.x(), TR.x() + 1):
            for j in range(BL.y(), TR.y() + 1):
                nodes.append(self._grid[i][j])
        return nodes
        
    def blacklistRect(self, points):
        nodes = self.getRect(points, 1)
        for node in nodes:
            node.blacklist(True)

    def addToBadNodes(self, points):
        nodes = self.getRect(points, 1)
        for node in nodes:
            self._badNodes.add(node)

    def calculateMovementCost(self, start, end):
        wallThreshold = 2

        if end in self._badNodes:
            return 400

        for node in self.getNeighbours(end):
            if node.isBlackListed():
                return 300

        if(end.x() < wallThreshold or end.x() > (self._width - 1 - wallThreshold) or 
           end.y() < wallThreshold or end.y() > (self._height - 1 - wallThreshold)):
            return 150 # Nodes which are next to the wall
        if(start.x() == end.x() or start.y() == end.y()):
            return 10
        else:
            return 14

    def getPath(self, c):

        """ Returns the path in order and applies path simplification. """

        path = [c]
        while c.getParent() is not None:
            c = c.getParent()
            path.append(c)
        path.reverse()
        return self.simplifyPath(path)

    def simplifyPath(self, path):

        """ Attempts to simplify the path by removing nodes which are
        very close to eachother. """

        newList = []
        for i in range(0, len(path) - 1):
            # Magic number for distance - 25 - where we remove close nodes.
            while((i < len(path) - 1) and
                  (((path[i].x() - path[i+1].x())**2 + (path[i].y() - path[i+1].y())**2) < 25)):
                path.remove(path[i+1])
        return path

    def printPath(self, path):
        for node in path:
            Log.d(TAG, "Node (%d,%d)" % (node.x(), node.y()))

    def resetNodes(self):

        """ Memory saving method, allows use of the same
        date structure for multiple path searches. """

        for row in self._grid:
            for node in row:
                node.setParent(None)
                node.setTotalCost(0)
                node.setHeuristicCost(0)
                node.setMovementCost(0)

    def findPath(self, current, end):

        """ 
        
        If a valid path exist, this will return the end
        node with it parent set to the previouse node (and so on).
        To get the complete path apply getPath(). Returns an empty
        list otherwise. 
        
        Heuristic: Chebyshev Distance

        """

        openSet = set()
        openHeap = []
        heapq.heapify(openHeap)
        closedSet = set()
        openSet.add(current)
        openHeap.append((current.getTotalCost(),current))

        while openSet:
            current = heapq.heappop(openHeap)[1]

            if current == end:
                return current

            openSet.remove(current)
            closedSet.add(current)

            for gridPoint in self.getNeighbours(current):

                if not(gridPoint.isBlackListed()):
                    cost = current.getMovementCost() + self.calculateMovementCost(current, gridPoint)

                    if gridPoint not in closedSet:


                        gridPoint.setHeuristicCost(max(abs(gridPoint.x() - end.x()),
                                                       abs(gridPoint.y() - end.y()))*10)

                        if gridPoint not in openSet:
                            openSet.add(gridPoint)
                            gridPoint.setMovementCost(cost)
                            gridPoint.setTotalCost(gridPoint.getMovementCost() + gridPoint.getHeuristicCost())
                            heapq.heappush(openHeap, (gridPoint.getTotalCost(), gridPoint))
                            gridPoint.setParent(current)
        return []
