from multiprocessing import Process, Value, Queue
from Queue import Empty, Full

import pygame

from ..Model.WorldObjects import *
from ..Drawing.Drawable import Drawable
from ..Communication.VisionClient import VisionClient

from ..Logging.SingleLog import Log
from ..Shared.ProcessManager import processes

TAG = "WORLD_MODEL"

white = (255,255,255)


class World(Drawable):
    drawQueue = None
    _vc = None
    _blueRobot = None
    _yellowRobot = None
    extraDrawings = {}
    
    def update(self):
        self._blueRobot.update()
        self._yellowRobot.update()
        self._ball.update()
        
    def __init__(self, visionSource, DEBUG=False):
        self._vc = visionSource
        self._blueRobot = Robot(visionSource, 'blue')
        self._yellowRobot = Robot(visionSource, 'yellow')
        self._ball = Ball(visionSource)
        (self._w, self._h) = visionSource.getAbstractDims()
        self._DEBUG = DEBUG
        if DEBUG:
            self.drawQueue = Queue()
            self._done = Value('b', False)
            self._debug_process = Process(target=self.doDebug, args=(self._done,))
            self._debug_process.start()
            processes.add("world debug", self._debug_process)


    def setPlayer(self, player):
        assert player in ['b', 'y'], "Player must be \"b\" or \"y\"; given \"%s\""%player
        self._player = player
        self._us = self._blueRobot if player == 'b' else self._yellowRobot
        self._them = self._blueRobot if player == 'y' else self._yellowRobot

    def getPlayer(self):
        return self._player

    def setSide(s, side):
        # TODO: change to +1 or -1, so computation is simpler and without if / else.
        assert side in ["low", "high"], "Side must be \"low\" or \"high\" (based on x-value); given \"%s\""%side
        s._side = side

        #TODO: Integrate this with side of the pitch
        sign = -1 if side == 'low' else 1
        s._ourGoalpostPos   = ( sign*Params.pitchLengthAbst /2, 0)
        s._theirGoalpostPos = (-sign*Params.pitchLengthAbst /2, 0)

    def getSide(self):
        return self._side

    # These two (getSide and getOurSide) are semantically separate.
    # If we decide that side represents the side we are shooting at in future then they will be different

    def getOurSide(self):
        return self._side

    def getTheirSide(self):
        if self._side == "high":
            return "low"
        else:
            return "high"

    def getUs(self):
        if self._us:
            return self._us
        else:
            return None

    def getThem(self):
        if self._them:
            return self._them
        else:
            return None

    def getTheirGoalpost(self):
        return self._theirGoalpostPos

    def getOurGoalpost(self):
        return self._ourGoalpostPos

    def getCentrePos(self):
        return (0,0)

    def getBlueRobot(self):
        return self._blueRobot

    def getYellowRobot(self):
        return self._yellowRobot

    def getBall(self):
        return self._ball

    def getDimensions(self):
        return (self._w, self._h)


    #TODO: def getDT(self):

    def getFps(self):
        return self._vc.getFps()
        
        
    def draw(self):
        screen = self._screen
        # Draw the background
        screen.fill((70, 111, 32))

        # Draw the pitch
        (w,h) = abst2pixel((self._w,self._h), dimensions=True)
        hw = w/2
        hh = h/2
        z = -2
        pygame.draw.polygon(screen, white, ((z,z),(z,h),(w,h),(w,z)), 10)
        pygame.draw.line(screen, white, (hw, 0), (hw, h), 5)
        pygame.draw.circle(screen, white, (hw, hh), 20, 0)

        # Draw the objects
        try:
            self._blueRobot.draw(screen)
        except IndexError:
            Log.d(TAG, "No blue robot")

        try:
            self._ball.draw(screen)
        except IndexError:
            Log.d(TAG, "No ball")

        try:
            self._yellowRobot.draw(screen)
        except IndexError as e:
            Log.d(TAG, "No yellow robot")

        # Draw any extra drawables
        while True:
            try:
                drawable = self.drawQueue.get_nowait()
                try:
                    (b, l) = drawable
                    self.extraDrawings[l] = list()
                except Exception, e:
                    if drawable == False:
                        self.extraDrawings = {}
                    else:
                        try:
                            self.extraDrawings[drawable.layer].append(drawable)
                        except KeyError:
                            self.extraDrawings[drawable.layer] = [drawable]
            except Empty:
                break

        for layer in self.extraDrawings:
            for drawable in self.extraDrawings[layer]:
                drawable.draw(self._screen)

        pygame.display.update()

    def doDebug(self, done):
        Log.d(TAG, "Initialising pygame for debug")
        self._vc = VisionClient(tryPort=self._vc._port)
        self._blueRobot = Robot(self._vc, 'blue')
        self._yellowRobot = Robot(self._vc, 'yellow')
        self._ball = Ball(self._vc)
        pygame.init()
        self._screen = pygame.display.set_mode(abst2pixel((self._w,self._h), dimensions=True))
        pygame.display.set_caption("Strategy Debug: Redraw vision")
        clock = pygame.time.Clock()
        Log.d(TAG, "Running at %f fps" % clock.get_fps())
        while not done.value:
            clock.tick()
            self.draw()

    def addToDrawing(self, drawable):
        try:
            self.drawQueue.put(drawable)
            return True
        except TypeError:
            return False
        except AttributeError:
            return False

    def clearExtras(self):
        if self._DEBUG:
            self.drawQueue.put(False)

    def clearLayer(self, layer):
        if self._DEBUG:
            self.drawQueue.put((False, layer))
