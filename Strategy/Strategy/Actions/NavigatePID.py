from ..Shared.Funcs import simpleAngle, vecAngle, vecPlus, vecDist
from ..LocalFuncs import relativeAngle
from ..Drawing.Drawable import Cross, Line
from ..Params import Params
from ..Shared.Funcs import angleDiff
import time

from math import degrees, cos, sin, pi

integral = 0
lastError = 0
derivative = 0
framerate = 25      # TODO: update from strategy!

def computeGoalPos(strategy):
    w = strategy.getWorld()    
    ourPos  = w.getUs().getPosition()
    ballPos = w.getBall().getPosition()
    
    dist = vecDist(ourPos,ballPos)
    
    if dist < 300:
        return ballPos
        
    p = strategy.getPredictor().get_ball_position(timeout=dist/Params.ballPredictionDistRatio)
    
    return p


def moveTo(strategy, gpos=None, maxspeed=None, backwards=False):
    # Uses PID - check http://en.wikipedia.org/wiki/PID_controller
    # experimental at the moment - params not tweaked on the robot
    ### Arguments:
    #               gpos - specifies the goal destination (where do you want the robot to go to)
    #              speed - specifies the speed the robot should move at
       
    global integral,lastError,derivative

    if not maxspeed:
        maxspeed=Params.moveSpeed

    r = strategy.getRobot()
    w = strategy.getWorld()
    bot = w.getUs()
    currentSpeed = maxspeed

    if not gpos:
        #gpos = w.getBall().getPosition() 
        gpos = computeGoalPos(strategy)

    angle = relativeAngle(bot, gpos)
    w.clearLayer('navigatepid')
    (x, y) = w.getUs().getPosition()
    orient = bot.getOrientation()
    line = Line((x, y), vecPlus((x, y), (300*cos(angle+orient), 300*sin(angle+orient))), abst=True, layer='navigatepid')
    w.addToDrawing(line)
    
 #   print gpos    
 #   w.clearLayer('prediction')
 #   w.addToDrawing(Cross(gpos, colour=(0, 0, 255), abst=True, layer='prediction'))

    pos = bot.getPosition()
    
    alpha = vecAngle(gpos,pos)           
    
    if backwards:
        alpha = angleDiff(alpha,pi)     
    
    error = simpleAngle(alpha-orient)   
    
    acceptableError = 0.01      # slightly reduce oscillation
    
    if abs(error) < acceptableError:
        error = 0
    
    integral = 2.0/3 * integral
    integral = integral + error
    derivative = (error - lastError) * framerate
    
    Kp = Params.K * getCoef(currentSpeed, Params.KpCoefficient)
    Ki = Params.K * getCoef(currentSpeed, Params.KiCoefficient)
    Kd = Params.K * getCoef(currentSpeed, Params.KdCoefficient)

    correction = Kp * error + Ki * integral + Kd * derivative
    
    lastError = error

    angle = correction
        
    a = abs(angle)
    smaller = max(int(maxspeed-a), -maxspeed)   
    if angle >= 0:       
        ws = (maxspeed, smaller)
    elif angle < 0:
        ws = (smaller, maxspeed)
    
    if backwards:
        print ws
        (left,right) = ws
        ws = (-right,-left)
        print ws
    
    r.move(*ws)
    
def getCoef(speed, coef):
    maxSpeed = 90
    if speed > maxSpeed:
        return coef[-1]

    idx = int(float(speed)/(maxSpeed+1)*len(coef))
    return coef[idx]

