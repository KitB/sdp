# Strategy for SDP group 6
## Running

To run strategy you will first need to have the vision server running and the comms server running and connected to a robot running our Control code. Instructions for these are in the readmes for those systems.

Once all other systems are running you can run an instance of the strategy system. Let's take for example the strategy code for the second milestone:
To run the "dribble" task you would run:

    . .bashrc
    ./M2 -a dribble

For further information on the options this takes: run `./M2 -h` after the first line of the above script.

## Design
The classes are expected to feed into each other like so:
Strategy takes a World (from World.py) and a RobotControl (from Communication.py) during construction. After constructing, you are expected to provide strategy with a list of decision rules by running `strategy.append(decision)`. It is vital to note that these decision rules will be checked in the order in which they are added and *only the first rule that returns true will fire* so be careful of the order in which you add rules.

A Decision rule consists of a Percept (I'll explain these in a moment) and a callable (for most purposes this is a function), when a decision rule is called (applied like a function) it checks to see if its percept is true and if it is then it runs its action (which is expected to be some robot commands *but* could be anything (even modifying the state of the strategy)). Percepts can be chained ("anded") by calling `percept.add(percept)` and are called as functions with a strategy as an argument (this means they can access the World in the strategy as well as potentially sharing state with each other in strategy), to define a Percept simply subclass "Percept" and define `_check(self, strategy)`, getting the World via `strategy.getWorld()`.

When a strategy is called, it will continually run through its decisions until one is fired.

Following is some simple code that will make the robot kick the ball when it has the ball:

    #!/usr/bin/env python

    from Strategy.Decision.Strategy import Strategy
    from Strategy.Decision.Decision import Decision
    from Strategy.Decision.Percept import Percept
    from Strategy.Model.World import World
    from Strategy.Communication.VisionClient import VisionClient
    from Strategy.Communication.Communication import RobotControl

    from Strategy.Shared.Funcs import vecDist

    # Initialise the World model
    vc = VisionClient()
    w = World(vc)
    w.setPlayer('b') # We're assuming we're blue
    w.setSide('low') # And on the low x-value side of the pitch

    # Get control of the robot
    r = RobotControl()

    # Create the Percepts
    class HasBall(Percept):
        def _check(self, strategy):
            """ Returns True iff the ball is within 60px of the robot centre """
            w = strategy.getWorld()
            us = w.getUs()
            ball = w.getBall()
            d = vecDist(us.getPosition(), ball.getPosition())
            return d < 60
    class CannotKick(Percept):
        def _check(self, strategy):
            """ This is a dummy to demonstrate percept chaining """
            return False

    # Create the actions
    def kick(strategy):
        r = strategy.getRobot()
        r.kick()
        strategy.finish() # We're done so finish

    # Create the Decision
    kickWithBall = Decision(HasBall().add(CannotKick().negate()), kick) # Note no brackets after "kick"

    # Create the strategy
    s = Strategy(w, r)
    s.attach(kickWithBall)

    # Run the strategy until it is done
    s()
