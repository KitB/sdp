import struct

from twisted.internet.protocol import Factory
from twisted.protocols.basic import LineReceiver

from vision import Main

# Constants
RQ_BLUE = 1
RQ_YELLOW = 2
RQ_RED = 3
RQ_DIMENSIONS = 4
RQ_FPS = 5

def _buildPacket(type, one, two, three, four, five):
    """ Creates a packet like a string """
    try:
        c = struct.pack('cHHddf', type, one, two, three, four, five)
    except struct.error:
        # there's a bug to be fixed here
        c = struct.pack('cHHddf', type, 0, 0, 0, 0, 0)
    assert len(c)==28
    return c

def _buildDimensionPacket(cx, cy, dx, dy):
    """ Creates a packet in which to send the dimensions of the pitch """
    try:
        c = struct.pack('cHHHH', 'd', cx, cy, dx, dy)
    except struct.error, e:
        # Something went wrong
        c = struct.pack('cHHHH', 'd', 0, 0, 0, 0)
    return c

def _build_fps_packet(fps):
    """ Creates a packet in which to send the dimensions of the pitch """
    try:
        c = struct.pack('cf', 'f', fps)
    except struct.error:
        # Something went wrong
        c = struct.pack('cf', 'f', 0.0)
    return c

class VisionServer(LineReceiver):
    def __init__(self, main):
        self.m = main
        self.state = "GETCLIENT"

    def connectionMade(self):
        print "Got client"
        self.state = "GETREQUEST"

    def connectionLost(self, _):
        print "Lost client"
        self.state = "GETCLIENT"

    def lineReceived(self, line):
        if self.state == "GETREQUEST":
            self.handle_request(line)

    def handle_request(self, data):
        (r,) = struct.unpack("b", data)
        if r == RQ_BLUE:
            self.sendBlue()
        elif r == RQ_YELLOW:
            self.sendYellow()
        elif r == RQ_RED:
            self.sendBall()
        elif r == RQ_DIMENSIONS:
            self.sendDimensions()
        elif r == RQ_FPS:
            self.sendFPS()

    def sendBlue(self):
        (x, y, tp) = self.m.get_position_blue_bot()
        (a, ta) = self.m.get_orientation_blue_bot()
        p = _buildPacket('b', x, y, tp, ta, a)
        self.sendLine(p)
        

    def sendYellow(self):
        (x, y, tp) = self.m.get_position_yellow_bot()
        (a, ta) = self.m.get_orientation_yellow_bot()
        p = _buildPacket('y', x, y, tp, ta, a)
        self.sendLine(p)

    def sendBall(self):
        (x, y, t) = self.m.get_position_ball()
        p = _buildPacket('r', x, y, 0.0, t, 0.0)
        self.sendLine(p)

    def sendDimensions(self):
        (cx, cy, dx, dy) = self.m.get_dimensions()
        p = _buildDimensionPacket(cx, cy, dx, dy)
        self.sendLine(p)
    
    def sendFPS(self):
        p = _build_fps_packet(self.m.get_fps())
        self.sendLine(p)

class VisionServerFactory(Factory):
    def __init__(self, main=None, pitch=0, use_mplayer=False, use_jpegStream=False, video_device_number=0, computer=1):
        if main:
            self.m = main
        else:
            self.m = Main(pitch, use_mplayer, use_jpegStream, video_device_number, computer)
    def buildProtocol(self, addr):
        return VisionServer(self.m)

if __name__ == "__main__":
    from twisted.internet import reactor
    reactor.listenTCP(31410, VisionServerFactory())
    reactor.run()



