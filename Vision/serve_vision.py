# Serve vision just runs a vision server
from vision_server import VisionServerFactory
from optparse import OptionParser
from twisted.internet import reactor

# Get arguments from command line
parser = OptionParser()
parser.add_option("-P", "--pitch",\
        dest="pitch", action="store", type="int", help="Choose pitch to run on, one of {0, 1}", default=0)
parser.add_option("-C", "--computer",\
        dest="computer", action="store", type="int", help="Choose computer to run on, one of {1 - 8}", default=1)
parser.add_option("-p", "--port",\
        dest="port", action="store", type="int", help="Which port to listen on", default=31410)
parser.add_option("-H", "--host",\
        dest="host", action="store", type="string", help="Which host to listen on", default='')
parser.add_option("-m", "--use-mplayer",\
        dest="use_mplayer", action="store_true", help="Use mplayer for vision input rather than SimpleCV",\
        default=False)
parser.add_option("-v", "--video-device-number",\
        dest="videoDev", action="store", type="int", help="The number of the /dev/video* device to use", default=0)
parser.add_option("-j", "--use-jpegStreaming", \
        dest="use_jpegStreaming", action="store_true", help="Use a set of jpeg images as vision input. At the moment, images must be stored \
                                                            in the directory vision/JpegStream, and named 00000, 00001 etc", default=False)
(options, args) = parser.parse_args()
if not options.pitch in [0,1]:
    parser.error("Pitch must be one of: {0,1}")

if not options.videoDev in [-2,-1,0,1,2]:
    parser.error("Pitch must be one of: {-2..2}")

print "Looking at pitch: %d"%options.pitch
print "Using video device: %d"%options.videoDev
print "Listening on port %d"%options.port
try:
    server = VisionServerFactory(pitch = options.pitch, use_mplayer = options.use_mplayer, use_jpegStream = options.use_jpegStreaming, video_device_number=options.videoDev, computer=options.computer)
    reactor.listenTCP(options.port, server, interface=options.host)
    reactor.run()
except KeyboardInterrupt:
    sys.stdout.write("\rUser exited\n")
    sys.stdout.flush()
server.close()
