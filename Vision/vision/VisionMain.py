import os
from FrameCapture import *
from ImagePreprocess import *
from ObjectParameters import *
from VisionInterface import *
from HelperFunctions import translateCoordinates



# Main vision function. Loop through frame, calculate information
def visionMain(mediator):
    
    # For brevity we'll use m for the mediator
    m = mediator
    
    # Also for brevity. Later we'll set up some names
    bp, bpt, ba, bat = (m._pos_blue_bot, m._pos_blue_bot_t, m._orientation_blue_bot, m._orientation_blue_bot_t)
    yp, ypt, ya, yat = (m._pos_yellow_bot, m._pos_yellow_bot_t, m._orientation_yellow_bot, m._orientation_yellow_bot_t)
    ballp, ballt = (m._pos_ball, m._pos_ball_t)
    
    path = os.path.join(m._path, "Parameters", "pitch_" + str(m._pitch), "computer_" + str (m._computer))
    pathIntrinsics = os.path.join(path, "Intrinsics.xml")
    pathDistortion = os.path.join(path, "Distortion.xml")
    
    # These are the objects (obj)
    yellow_robot = Object("yellow_robot", path)
    blue_robot = Object("blue_robot", path)
    ball = Object("ball", path)
    yellow_circle = Object("yellow_circle", path)
    blue_circle = Object("blue_circle", path)
    parameters = Object("pre_process", path)
    pitch = Object("pitch", path)
    
    # There are the classes used
    preprocessor = pre_processor(640, 480, parameters, pathIntrinsics, pathDistortion)
    frame_capture = FrameCapture(preprocessor, m._use_mplayer, m._use_jpegStreaming, m._videoDev)
    vision_interface = VisionInterface(preprocessor, frame_capture)
    
    # Information holders
    coords_yellow_robot = None
    coords_blue_robot = None
    coords_ball = None
    
    # Increase accuracy with int rounding
    factor = 100   # Strategy will assume the factor 100. WARNING!
    
    # Calculate pitch coordinates
    (cx, cy, dx, dy) = pitch.get_pitch_dims()
    m._dimensions[0] = cx # * factor
    m._dimensions[1] = cy # * factor
    m._dimensions[2] = dx # * factor
    m._dimensions[3] = dy # * factor
    
    # Variables for translating coordinates to adjust for camera height
    camera_x = preprocessor.get_camera_x()
    camera_y = preprocessor.get_camera_y()
    camera_h = preprocessor.get_camera_h()
    robot_h = preprocessor.get_robot_h()
    height_ratio = robot_h/camera_h


    while True:
        
        # Timestamps
        timestamp = time.time()
        
        # Blue robot
        binary_image_blue_robot, coords_blue_robot, coords_blue_robot_cropped = vision_interface.get_coordinates(coords_blue_robot, blue_robot, None, False, None, None)
        binary_image_blue_circle, coords_blue_circle, coords_blue_circle_cropped = vision_interface.get_coordinates(coords_blue_robot, blue_circle, binary_image_blue_robot,  True, None, blue_robot)
        orientation_blue_robot = vision_interface.get_orientation(coords_blue_circle, coords_blue_robot)
        
        # Yellow robot
        binary_image_yellow_robot, coords_yellow_robot, coords_yellow_robot_cropped = vision_interface.get_coordinates(coords_yellow_robot, yellow_robot, None, False, coords_blue_robot, None)
        binary_image_yellow_circle, coords_yellow_circle, coords_yellow_circle_cropped = vision_interface.get_coordinates(coords_yellow_robot, yellow_circle, binary_image_yellow_robot,  True, None, yellow_robot)
        orientation_yellow_robot = vision_interface.get_orientation(coords_yellow_circle, coords_yellow_robot)
        
        
        # Ball
        binary_image_ball, coords_ball, coords_ball_cropped = vision_interface.get_coordinates(coords_ball, ball, None, False, None, None)
        
        # Move to next frame
        vision_interface.nextFrame()
        
        # Calculate fps
        fps = vision_interface.get_frame_rate()
        
        # Auto-reduce search rate for an object, if it hasn't been recently found
        if not coords_blue_robot:
            vision_interface.search_rate_adjustment("Blue lost")
        if not coords_yellow_robot:
            vision_interface.search_rate_adjustment("Yellow lost")
        
        
        
        # Translate coordinates to adjust for camera height
        if coords_blue_robot:
            coords_blue_robot_fixed = (translateCoordinates(coords_blue_robot[0], camera_x, height_ratio), \
                                       translateCoordinates(coords_blue_robot[1], camera_y, height_ratio))
        else:
            coords_blue_robot_fixed = coords_blue_robot
            
        if coords_yellow_robot:
            coords_yellow_robot_fixed = (translateCoordinates(coords_yellow_robot[0], camera_x, height_ratio), \
                                         translateCoordinates(coords_yellow_robot[1], camera_y, height_ratio))
        else:
            coords_yellow_robot_fixed = coords_yellow_robot
        
        
        
        # Adding timestamps
        if coords_yellow_robot:
            (yp[0], yp[1], ypt.value) = int(coords_yellow_robot_fixed[0] * factor), int(coords_yellow_robot_fixed[1] * factor), timestamp
        if coords_blue_robot:
            (bp[0], bp[1], bpt.value) = int(coords_blue_robot_fixed[0] * factor), int(coords_blue_robot_fixed[1] * factor), timestamp
        if coords_ball:
            (ballp[0], ballp[1], ballt.value) = int(coords_ball[0] * factor), int(coords_ball[1] * factor), timestamp
        if orientation_yellow_robot:
            (ya.value, yat.value) = orientation_yellow_robot, timestamp
        if orientation_blue_robot:
            (ba.value, bat.value) = orientation_blue_robot, timestamp



