import os
from FrameCapture import *
from ImagePreprocess import *
from ObjectParameters import *
from VisionInterface import *
from HelperFunctions import translateCoordinates



class VisionDebugger:
    
    # Initialize the VisionDebugger
    def __init__(self, path, pitch=0, use_mplayer=False, use_jpegStreaming=False, videoDevice=0, computer=1):
        self.path = os.path.join(path, "Parameters", "pitch_" + str(pitch), "computer_" + str(computer))
        self.pathIntrinsics = os.path.join(self.path, "Intrinsics.xml")
        self.pathDistortion = os.path.join(self.path, "Distortion.xml")
        
        # These are the objects (obj)
        self.yellow_robot = Object("yellow_robot", self.path)
        self.blue_robot = Object("blue_robot", self.path)
        self.ball = Object("ball", self.path)
        self.yellow_circle = Object("yellow_circle", self.path)
        self.blue_circle = Object("blue_circle", self.path)
        self.parameters = Object("pre_process", self.path)
        self.pitch = Object("pitch", self.path)
        
        # There are the classes used
        self.preprocessor = pre_processor(640, 480, self.parameters, self.pathIntrinsics, self.pathDistortion)
        self.frame_capture = FrameCapture(self.preprocessor, use_mplayer, use_jpegStreaming, videoDevice)
        self.vision_interface = VisionInterface(self.preprocessor, self.frame_capture)
        
        # Information holders
        self.coords_yellow_robot = None
        self.coords_blue_robot = None
        self.coords_ball = None
        self.dimensions = [None]*4
        
        # Calculate pitch coordinates
        (cx, cy, dx, dy) = self.pitch.get_pitch_dims()
        self.dimensions[0] = cx
        self.dimensions[1] = cy
        self.dimensions[2] = dx
        self.dimensions[3] = dy
        
        # Variables for translating coordinates to adjust for camera height
        self.camera_x = self.preprocessor.get_camera_x()
        self.camera_y = self.preprocessor.get_camera_y()
        self.camera_h = self.preprocessor.get_camera_h()
        self.robot_h = self.preprocessor.get_robot_h()
        self.height_ratio = self.robot_h/self.camera_h
        
        # If true, auto-reload the color model parameters
        self.auto_ball = True
        self.auto_yellow = True
        self.auto_blue = True
        self.auto_yellow_circle = True
        self.auto_blue_circle = True



    # Used by VisionDisplay to calculate everything for a single frame
    def doCalculations(self):
        
        # If true, auto-reload the color model parameters in each frame
        if self.auto_ball:
            self.auto_color_ball(True)
        if self.auto_yellow:
            self.auto_color_yellow(True)
        if self.auto_blue:
            self.auto_color_blue(True)
        if self.auto_yellow_circle:
            self.auto_color_yellow_circle(True)
        if self.auto_blue_circle:
            self.auto_color_blue_circle(True)
        
        # Full, undistorted, rotated image
        image = self.vision_interface.get_full_image_rgb()
        
        # Full, undistorted, rotated, HSV image
        hsv_image = self.vision_interface.get_full_image_hsv()
        
        # Croped, undistorted, rotated image
        croped_image = self.vision_interface.get_croped_image_rgb()
        
        # Blue robot
        binary_image_blue_robot, self.coords_blue_robot, coords_blue_robot_cropped = self.vision_interface.get_coordinates(self.coords_blue_robot, self.blue_robot, None, False, None, None)
        binary_image_blue_circle, coords_blue_circle, coords_blue_circle_cropped = self.vision_interface.get_coordinates(self.coords_blue_robot, self.blue_circle, binary_image_blue_robot,True, None, self.blue_robot)
        orientation_blue_robot = self.vision_interface.get_orientation(coords_blue_circle, self.coords_blue_robot)
        
        # Yellow robot
        binary_image_yellow_robot, self.coords_yellow_robot, coords_yellow_robot_cropped = self.vision_interface.get_coordinates(self.coords_yellow_robot, self.yellow_robot, None, False, self.coords_blue_robot, None)
        binary_image_yellow_circle, coords_yellow_circle, coords_yellow_circle_cropped = self.vision_interface.get_coordinates(self.coords_yellow_robot, self.yellow_circle, binary_image_yellow_robot, True, None, self.yellow_robot)
        orientation_yellow_robot = self.vision_interface.get_orientation(coords_yellow_circle, self.coords_yellow_robot)
        
        # Ball
        binary_image_ball, self.coords_ball, coords_ball_cropped = self.vision_interface.get_coordinates(self.coords_ball, self.ball, None, False, None, None)
        
        # Move to next frame
        self.vision_interface.nextFrame()
        
        # Calculate fps
        fps = self.vision_interface.get_frame_rate()
        
        # Auto-reduce search rate for an object, if it hasn't been recently found
        if not self.coords_blue_robot:
            self.vision_interface.search_rate_adjustment("Blue lost")
        if not self.coords_yellow_robot:
            self.vision_interface.search_rate_adjustment("Yellow lost")
        
        
        
        # Translate coordinates to adjust for camera height (Not used anyway at the moment. If used, need initialization)
        if self.coords_blue_robot:
            coords_blue_robot_fixed = (translateCoordinates(self.coords_blue_robot[0], self.camera_x, self.height_ratio), \
                                       translateCoordinates(self.coords_blue_robot[1], self.camera_y, self.height_ratio))
        if self.coords_yellow_robot:
            coords_yellow_robot_fixed = (translateCoordinates(self.coords_yellow_robot[0], self.camera_x, self.height_ratio), \
                                         translateCoordinates(self.coords_yellow_robot[1], self.camera_y, self.height_ratio))
        if coords_blue_circle:
            coords_blue_circle_fixed = (translateCoordinates(coords_blue_circle[0],  self.camera_x, self.height_ratio), \
                                        translateCoordinates(coords_blue_circle[1], self.camera_y, self.height_ratio))
        if coords_yellow_circle:
            coords_yellow_circle_fixed = (translateCoordinates(coords_yellow_circle[0],  self.camera_x, self.height_ratio), \
                                          translateCoordinates(coords_yellow_circle[1], self.camera_y, self.height_ratio))
        
        
        
        # Images, Points, Floats
        return image, hsv_image, croped_image, binary_image_yellow_robot, binary_image_yellow_circle, \
               binary_image_blue_robot, binary_image_blue_circle, binary_image_ball, \
               self.coords_yellow_robot, coords_yellow_circle, self.coords_blue_robot, coords_blue_circle,\
               self.coords_ball, coords_yellow_robot_cropped, coords_blue_robot_cropped, coords_ball_cropped,\
               orientation_yellow_robot, orientation_blue_robot, fps



    # Return full, unprocessed image
    def get_unprocessed_image(self):
        return self.vision_interface.get_unprocessed_image()



    # Load default settings
    def load_default_parameters(self):
        self.yellow_robot.load_parameters()
        self.blue_robot.load_parameters()
        self.ball.load_parameters()
        self.yellow_circle.load_parameters()
        self.blue_circle.load_parameters()
        self.parameters.load_parameters()
        self.preprocessor.load_default_parameters()



    # Reload new settings, if xml has changed
    def reload_parameters(self):
        self.yellow_robot = Object("yellow_robot", self.path)
        self.blue_robot = Object("blue_robot", self.path)
        self.ball = Object("ball", self.path)
        self.yellow_circle = Object("yellow_circle", self.path)
        self.blue_circle = Object("blue_circle", self.path)
        self.parameters = Object("pre_process", self.path)
        self.preprocessor = pre_processor(640, 480, self.parameters)
        self.vision_interface = VisionInterface(self.preprocessor, self.frame_capture)



    def auto_color_ball(self, auto_color):
        self.auto_ball = auto_color
        self.ball = Object("ball", self.path)

    def auto_color_yellow(self, auto_color):
        self.auto_yellow = auto_color
        self.yellow_robot = Object("yellow_robot", self.path)

    def auto_color_blue(self, auto_color):
        self.auto_blue = auto_color
        self.blue_robot = Object("blue_robot", self.path)

    def auto_color_yellow_circle(self, auto_color):
        self.auto_yellow_circle = auto_color
        self.yellow_circle = Object("yellow_circle", self.path)

    def auto_color_blue_circle(self, auto_color):
        self.auto_blue_circle = auto_color
        self.blue_circle = Object("blue_circle", self.path)

    def auto_color_none(self):
        self.auto_ball = False
        self.auto_yellow = False
        self.auto_blue = False
        self.auto_yellow_circle = False
        self.auto_blue_circle = False



