#!/usr/bin/env python

import Image
import gtk
import cProfile
import gobject
from VisionDebugger import *
import SimpleCV
from numpy import *
from lxml import etree
from xml.dom import minidom
from xml.etree import ElementTree


# Current tasks/issues:

# scales dont work very well. They break at seemingly random times, and after one breaks it will not correct itself.
# you can update the scales with no video running and it might work or might break. You can update the scales with video running and it might work or 
# might break, although it seems more likely to break if you're playing something. 

# Stuff to come:
# More coordinate information, more video options (easy to do).
# Training should be linked in to vision, not sure what it does, need to talk to athanasios in detail I think.
# Save/refresh, be able to save the current hue etc settings, and load a saved one in.
# If user clicks on video feed, return coordinates of the pointer.

class Signal:
    def __init__(self):
        window = gtk.Window()
        window.set_default_size(1200, 350)
        window.set_title("Vision GUI")

        table = gtk.Table(10, 10, False)
        
        self.current_image = gtk.Image()


        label = gtk.Label("Please Select a Pitch")
        dialog = gtk.Dialog("Pitches",
                            None,
                            gtk.DIALOG_MODAL | gtk.DIALOG_DESTROY_WITH_PARENT,
                            )
        dialog.vbox.pack_start(label)
        dialog.add_buttons("Main Pitch", 42, "Side Pitch", 24)
        label.show()
        response = dialog.run()
        dialog.destroy()

        if (response == 42):
            self.vision_debugger = VisionDebugger('vision', pitch=0, use_mplayer=False, videoDevice=0)
            print "On the main pitch, not using mplayer"
            cwd = os.getcwd()
            os.chdir(cwd + "/vision/Parameters/pitch_0")
        elif (response == 24):
            self.vision_debugger = VisionDebugger('vision', pitch=1, use_mplayer=True, videoDevice=0)
            print "On the side pitch, using mplayer"
            cwd = os.getcwd()
            os.chdir(cwd + "/vision/Parameters/pitch_1")
        else:
            print "Error: pitch not selected"

        for event, element in etree.iterparse("Distortion.xml", tag='data'):
                    values = element.text
                    values = values.strip()
                    list2 = values.split(' ')

        global button_video, button_cropped, button_binary_yellow, button_snapshot
        button_video = gtk.ToggleButton(label="Start Video")
        button_cropped = gtk.ToggleButton(label="Cropped Image")
        button_binary_yellow = gtk.ToggleButton(label="Binary of Yellow Robot")
        button_snapshot = gtk.Button("Snapshot")
        button_snapshot.set_sensitive(False)
        button_training = gtk.Button("Training")
        button_refresh = gtk.Button("refresh", gtk.STOCK_REFRESH)
        button_save = gtk.Button("Save", gtk.STOCK_SAVE)   
        button_save.set_sensitive(False)

# This long winded menu code is how you nest menus within menus, to create a nice hierarchical effect.      
        rotate_menu = gtk.Menu()
        rotate_menu_item_90 = gtk.MenuItem("90 degrees")
        rotate_menu_item_180 = gtk.MenuItem("180 degrees")
        rotate_menu_item_270 = gtk.MenuItem("270 degrees")
        rotate_menu.append(rotate_menu_item_90)
        rotate_menu.append(rotate_menu_item_180)
        rotate_menu.append(rotate_menu_item_270)
        rotate_menu_item_90.show()
        rotate_menu_item_180.show()
        rotate_menu_item_270.show()
        rotate_menu_title = gtk.MenuItem("Rotate: ")
        rotate_menu_title.show()
        rotate_menu_title.set_submenu(rotate_menu)
        rotate_menu_bar = gtk.MenuBar()
        rotate_menu_bar.append(rotate_menu_title)
        rotate_menu_bar.show()

# Text labels
        label_coords_ball = gtk.Label("Ball coordinates: ")
        label_coords_yellow_robot = gtk.Label("Yellow Robot coordinates: ")
        label_coords_blue_robot = gtk.Label("Blue Robot coordinates: ")
        label_coords_blue_circle = gtk.Label("Blue Circle coordinates: ")
        label_coords_yellow_circle = gtk.Label("Yellow Circle coordinates: ")
        label_hue = gtk.Label("Hue")
        label_saturation = gtk.Label("Saturation")
        label_value = gtk.Label("Value")

# Text entry box
        global entry_coords_ball, entry_coords_yellow_robot, entry_coords_blue_robot, entry_coords_blue_circle, entry_coords_yellow_circle
        entry_coords_ball = gtk.Entry()
        entry_coords_ball.set_text("(0,0)")
        entry_coords_ball.select_region(0, len(entry_coords_ball.get_text()))
        entry_coords_ball.show

        entry_coords_yellow_robot = gtk.Entry()
        entry_coords_yellow_robot.set_text("(0,0)")
        entry_coords_yellow_robot.select_region(0, len(entry_coords_yellow_robot.get_text()))
        entry_coords_yellow_robot.show

        entry_coords_blue_robot = gtk.Entry()
        entry_coords_blue_robot.set_text("(0,0)")
        entry_coords_blue_robot.select_region(0, len(entry_coords_blue_robot.get_text()))
        entry_coords_blue_robot.show

        entry_coords_blue_circle = gtk.Entry()
        entry_coords_blue_circle.set_text("(0,0)")
        entry_coords_blue_circle.select_region(0, len(entry_coords_blue_circle.get_text()))
        entry_coords_blue_circle.show

        entry_coords_yellow_circle = gtk.Entry()
        entry_coords_yellow_circle.set_text("(0,0)")
        entry_coords_yellow_circle.select_region(0, len(entry_coords_yellow_circle.get_text()))
        entry_coords_yellow_circle.show

        colour_selection = gtk.ColorSelection()
        colour_selection.set_has_opacity_control(True)
        colour_selection.set_has_palette(True)



        self.adjustment_distortion = gtk.Adjustment(value = list2[0], lower = -0.5, upper = 0.5, step_incr = 0.0001, page_incr = 0.001, page_size = 0)
        self.adjustment_distortion2 = gtk.Adjustment(value = list2[1], lower = -0.5, upper = 0.5, step_incr = 0.001, page_incr = 0.001, page_size = 0)
        self.adjustment_distortion3 = gtk.Adjustment(value = list2[2], lower = -0.5, upper = 0.5, step_incr = 0.001, page_incr = 0.001, page_size = 0)
        self.adjustment_distortion4 = gtk.Adjustment(value = list2[3], lower = -0.5, upper = 0.5, step_incr = 0.001, page_incr = 0.001, page_size = 0)

        self.vision_distortion_slider = gtk.HScale(adjustment = self.adjustment_distortion)
        self.vision_distortion_slider.set_digits(16)
        self.vision_distortion_slider.set_update_policy(gtk.UPDATE_DELAYED)
        self.vision_distortion_slider.connect("value_changed", self.scale_changed)
        self.vision_distortion_slider2 = gtk.HScale(adjustment = self.adjustment_distortion2)
        self.vision_distortion_slider2.set_digits(16)
        self.vision_distortion_slider2.set_update_policy(gtk.UPDATE_DELAYED)
        self.vision_distortion_slider2.connect("value_changed", self.scale_changed)
        self.vision_distortion_slider3 = gtk.HScale(adjustment = self.adjustment_distortion3)
        self.vision_distortion_slider3.set_digits(16)
        self.vision_distortion_slider3.set_update_policy(gtk.UPDATE_DELAYED)
        self.vision_distortion_slider3.connect("value_changed", self.scale_changed)
        self.vision_distortion_slider4 = gtk.HScale(adjustment = self.adjustment_distortion4)
        self.vision_distortion_slider4.set_digits(16)
        self.vision_distortion_slider4.set_update_policy(gtk.UPDATE_DELAYED)
        self.vision_distortion_slider4.connect("value_changed", self.scale_changed)
        self.vision_label_distortion = gtk.Label("Distortion")


# So we start off horizontal, looks better
        global rotate_angle
        rotate_angle = 270

# This connect section is what makes the widgets do stuff. The first argument is a signal that they are coded to respond to, and the following is the
# function they subsequently execute, and any data to be passed.
        window.connect("destroy", lambda q: gtk.main_quit())
        button_snapshot.connect("clicked", self.snapshot)
        button_video.connect("toggled", self.video)
        button_cropped.connect("toggled", self.video)
        button_binary_yellow.connect("toggled", self.video)
        rotate_menu_item_90.connect("activate", self.rotate_image, self.current_image, 90)
        rotate_menu_item_180.connect("activate", self.rotate_image, self.current_image, 180)
        rotate_menu_item_270.connect("activate", self.rotate_image, self.current_image, 270)
        colour_selection.connect("color-changed", self.colour_selected)
        entry_coords_yellow_robot.connect("activate", self.enter_pressed, entry_coords_yellow_robot, "yellow_coords")
        entry_coords_ball.connect("activate", self.enter_pressed, entry_coords_ball, "ball_coords")
        entry_coords_blue_robot.connect("activate", self.enter_pressed, entry_coords_blue_robot, "blue_coords")
        entry_coords_blue_circle.connect("activate", self.enter_pressed, entry_coords_blue_circle, "blue_circle_coords")
        entry_coords_yellow_circle.connect("activate", self.enter_pressed, entry_coords_yellow_circle, "yellow_circle_coords")

# This adds the widgets to the table defined at the top. table.attach(widget, cell to start X coord, cell to end X coord, cell to start Y coord, cell to
# end Y coord)
        window.add(table)
        table.attach(self.current_image, 0, 3, 0, 3)
        table.attach(label_coords_ball, 7, 9, 0, 1)
        table.attach(label_coords_yellow_robot, 7, 9, 1, 2)
        table.attach(label_coords_blue_robot, 7, 9, 2, 3)
        table.attach(label_coords_blue_circle, 7, 9, 3, 4)
        table.attach(label_coords_yellow_circle,7, 9, 4, 5)
        table.attach(button_video, 0, 1, 6, 7)
        table.attach(button_cropped, 1, 2, 6, 7)
        table.attach(button_binary_yellow, 2, 3, 6, 7)
        table.attach(button_snapshot, 0, 1, 9, 10)
        table.attach(button_training, 1, 2, 9, 10)
        table.attach(rotate_menu_bar, 2, 3, 9, 10)
        table.attach(entry_coords_ball,9, 10, 0, 1)
        table.attach(entry_coords_yellow_robot, 9, 10, 1, 2)
        table.attach(entry_coords_blue_robot, 9, 10, 2, 3)
        table.attach(entry_coords_blue_circle, 9, 10, 3, 4)
        table.attach(entry_coords_yellow_circle, 9, 10, 4, 5)
        #table.attach(colour_selection, 8, 10, 6, 7)
        table.attach(self.vision_distortion_slider, 6, 8, 6, 8)
        table.attach(self.vision_distortion_slider3, 6, 8, 8, 10)
        table.attach(self.vision_distortion_slider2, 8, 10, 6, 8)
        table.attach(self.vision_distortion_slider4, 8, 10, 8, 10)
        window.show_all()  

# Called when one of the video buttons is pressed, it checks if there is already an active feed. Every 55 ms it tells start_video to execute.
# 55 is the minimum value you can use, any lower and the images will not load.
    def video(self, widget):
        global end
        if (widget.get_active() == False):
            end = 1
        else:
            end = 0
        maintimer = gobject.timeout_add(100, self.start_video, widget)

# Loads the appropriate frame to display, depending on if we're in binary mode, standard mode, etc.
    def start_video(self, widget):
        self.makeCalculations()
        t1 = ""
        t2 = ""
        t3 = ""
        t4 = ""
        t5 = ""
        t6 = ""
        t7 = ""
        t8 = ""
        t9 = ""
        t10 = ""

        if (coords_ball != None):
            t1 = coords_ball[0]
            t2 = coords_ball[1]
            t1 = "(%s, %s)" % (str(round(t1,2)), str(round(t2,2)))
        else:
            t1 = "cannot detect ball"
        
        if (coords_yellow_robot != None):
            t3 = coords_yellow_robot[0]
            t4 = coords_yellow_robot[1]
            t3 = "(%s, %s)" % (str(round(t3,2)), str(round(t4,2)))
        else:
            t3 = "cannot detect yellow robot"
        
        if (coords_blue_robot != None):
            t5 = coords_blue_robot[0]
            t6 = coords_blue_robot[1]
            t5 = "(%s, %s)" % (str(round(t5,2)), str(round(t6,2)))
        else:
            t5 = "cannot detect blue robot"
        
        if (coords_blue_circle != None):
            t7 = coords_blue_circle[0]
            t8 = coords_blue_circle[1]
            t7 = "(%s, %s)" % (str(round(t7,2)), str(round(t8,2)))
        else:
            t7 = "cannot detect blue circle"
        
        if (coords_yellow_circle != None):
            t9 = coords_yellow_circle[0]
            t10 = coords_yellow_circle[1]
            t9 = "(%s, %s)" % (str(round(t1,2)), str(round(t10,2)))
        else:
            t9 = "cannot detect yellow circle"
        
        entry_coords_ball.set_text(t1)
        entry_coords_yellow_robot.set_text(t3)
        entry_coords_blue_robot.set_text(t5)
        entry_coords_blue_circle.set_text(t7)
        entry_coords_yellow_circle.set_text(t9)
        tmp_label = widget.get_label()
        if (widget == button_video):
            image2 = image
            tmp_label = "Start Video"
            button_cropped.set_sensitive(False)
            button_binary_yellow.set_sensitive(False)    
        elif (widget == button_cropped):
            image2 = cropped_image
            tmp_label = "Cropped Video"
            button_video.set_sensitive(False)
            button_binary_yellow.set_sensitive(False)
        elif (widget == button_binary_yellow):
            image2 = binary_image_yellow_robot
            tmp_label = "Binary of Yellow Robot"
            button_video.set_sensitive(False)
            button_cropped.set_sensitive(False)
        if (end == 0):
            widget.set_label("Stop Stream")
            self.get_image(image2)           
            return True
        elif (end == 1):
            widget.set_label(tmp_label)
            button_video.set_sensitive(True)
            button_cropped.set_sensitive(True)
            button_binary_yellow.set_sensitive(True)
            return False

# when the 90 option is clicked, 270 is sent. 90 seemed to be turning counter-clockwise, I wanted clockwise, and -90 was not an option. Need to add it
# to the current rotate angle for the image to stay rotated while the video is playing.
    def rotate_image(self, widget, image, degree):
        global image_current, rotate_angle
        rotate_angle = 360 - degree + rotate_angle
        image_current = image_current.rotate_simple(rotate_angle)
        self.current_image.set_from_pixbuf(image_current)
        button_snapshot.set_sensitive(True)

    def scale_changed(self, widget):
        value = widget.get_value() 

        for event, element in etree.iterparse("Distortion.xml", tag='data'):
            values = element.text
            values = values.strip()
            list2 = values.split(' ')

        if widget == self.vision_distortion_slider:
            list2[0] = value
        elif widget == self.vision_distortion_slider2:
            list2[1] = value
        elif widget == self.vision_distortion_slider3:
            list2[2] = value
        elif widget == self.vision_distortion_slider4:
            list2[3] = value

        outstring = "%s %s %s %s" % (list2[0], list2[1], list2[2], list2[3])

        dom = minidom.parse("Distortion.xml")
        for author in dom.getElementsByTagName('data'):
            author.childNodes = [dom.createTextNode(outstring)]

        f = open("Distortion.xml", 'w')
        dom.writexml(f)
        f.close()
    
    def colour_enter_pressed(self, widget, entry, scale):
        print scale.get_value()
        digit = float(entry.get_text())
        scale.set_value(digit)
        entry.select_region(0, len(entry.get_text()))

    def enter_pressed(self, widget, entry_coords, data):
        entry_text = entry_coords.get_text()
        if data == "ball_coords":
            entry_box = "ball entry box"
        elif data == "yellow_coords":
            entry_box = "yellow robot entry box"
        elif data == "blue_coords":
            entry_box = "blue robot entry box"
        elif data == "blue_circle_coords":
             entry_box = "blue circle entry box"
        else:
            print "error: entry box not recognised"
        print "Entry contents of " + entry_box + " : " + entry_text
    
    def snapshot(self, button_snapshot):
        image_current.save("snapshot", "jpeg", {"quality":"100"})
        button_snapshot.set_sensitive(False)

    def get_image(self, image):
# Both of these fix the mirror issue, don't know which is better.
        global image_current
        image_numpy = image.getNumpy()
        image_numpy = fliplr(image_numpy) #*
        image_current = gtk.gdk.pixbuf_new_from_array(image_numpy, gtk.gdk.COLORSPACE_RGB, 8)
        image_current = image_current.rotate_simple(rotate_angle)
#*        image_current = image_current.flip(True)
        self.current_image.set_from_pixbuf(image_current)
        button_snapshot.set_sensitive(True)   

    def colour_selected(self, widget):
        print "Colour selected:", widget.get_current_color()        
  
    # Calculate all needed information
    def makeCalculations(self):
        
        global image, hsv_image, cropped_image, binary_image_yellow_robot, binary_image_yellow_circle, \
        binary_image_blue_robot, binary_image_blue_circle, binary_image_ball, \
        coords_yellow_robot, coords_yellow_circle, coords_blue_robot, coords_blue_circle,\
        coords_ball, coords_yellow_robot_cropped, coords_blue_robot_cropped, coords_ball_cropped, \
        orientation_yellow_robot, orientation_blue_robot, fps\
                
        image, hsv_image, cropped_image, binary_image_yellow_robot, binary_image_yellow_circle, \
        binary_image_blue_robot, binary_image_blue_circle, binary_image_ball, \
        coords_yellow_robot, coords_yellow_circle, coords_blue_robot, coords_blue_circle,\
        coords_ball, coords_yellow_robot_cropped, coords_blue_robot_cropped, coords_ball_cropped, \
        orientation_yellow_robot, orientation_blue_robot, fps\
        = self.vision_debugger.doCalculations()

Signal()
gtk.main()
