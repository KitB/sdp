import os
from SimpleCV import Image



class JpegStream:
    
    # Initialize a JpegStream
    def __init__(self, videoDev=1, path="vision/JpegStream", sample_numbering="0", sample_name="", loop=True):
        
        # Information about input
        self.path = path
        self.file_name = sample_name
        self.total_length = len(sample_numbering)
        
        # Other variables
        self.loop = loop
        self.count = 0
        self.path_to_next_image = None



    # Get the next image in line
    def getImage(self):
        
        # Check if image exists
        if os.path.exists(self.getNextPath()):
            self.count += 1
            return Image(self.next_image_path)
        elif self.loop and self.count != 0:
            self.count = 0
            return self.getImage()
        else:
            print "Jpeg streaming: Out of images"
            return Image(self.next_image_path)



    # Find the pathname to the next image
    def getNextPath(self):
        
        # Fill up with zeros, to create the image number
        file_number = str(self.count)
        zeros_number = self.total_length - len(file_number) +1
        file_number = file_number.zfill(zeros_number)
        
        # Create path
        self.next_image_path = os.path.join(self.path, self.file_name + file_number + '.jpg')
        return self.next_image_path



