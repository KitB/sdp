from SimpleCV import *
from Exceptions import *



class pre_processor:
    
    # Initialize an image pre-processor
    def __init__(self, width, height, ObjectParameters, pathIntrinsics, pathDistortion):    
        
        self.__width = width
        self.__height = height
        self.__parameters = ObjectParameters
        self.__pathIntrinsics = pathIntrinsics
        self.__pathDistortion = pathDistortion
        self.__reload_undistortion = False
        
        self.load_undistortion(False)
        self.load_default_parameters()



    # Load default parameters
    def load_default_parameters(self):
        crop = self.__parameters.get_crop_position()
        self.__crop_x = crop[0]
        self.__crop_y = crop[1]
        size = self.__parameters.get_crop_size()
        self.__crop_h = size[0]
        self.__crop_w = size[1]
        camera = self.__parameters.get_camera_pos()
        self.__camera_x = camera[0]
        self.__camera_y = camera[1]
        self.__camera_height = self.__parameters.get_camera_height()
        self.__robot_height = self.__parameters.get_robot_height()
        self.__rotate = self.__parameters.get_rotate()



    # Load undistortion parameters
    def load_undistortion(self, auto_undistort):
        
        # Start, stop automatic reload of untistortion parameters
        self.__reload_undistortion = auto_undistort
        
        intrinsic = cv.Load(self.__pathIntrinsics)
        distortion = cv.Load(self.__pathDistortion)
        
        self.__mapx = cv.CreateImage((self.__width, self.__height), cv.IPL_DEPTH_32F, 1)
        self.__mapy = cv.CreateImage((self.__width, self.__height), cv.IPL_DEPTH_32F, 1)
        self.__rotateMatrix = cv.CreateMat(2, 3, cv.CV_32FC1)
        
        cv.GetRotationMatrix2D((self.__width/2, self.__height/2), -1, 1, self.__rotateMatrix)
        cv.InitUndistortMap(intrinsic, distortion, self.__mapx, self.__mapy)
    
    
    
    # Undistort the image, if of proper dimensions
    def __undistort(self, img):
        
        if self.__reload_undistortion:
            self.load_undistortion(True)
        
        if img.size() != (self.__width, self.__height):
            raise ImageSizeError("input image has size: " + str(img.size())\
                + " but (" + str(self.__width) + ", " + str(self.__height) +") is expected")
        opencv_image = img.getBitmap()
        t = cv.CloneImage(opencv_image)
        cv.Remap(t, opencv_image, self.__mapx, self.__mapy)
        cv.WarpAffine(opencv_image, t, self.__rotateMatrix)
        return Image(t)



    def doRotate(self, img):
        
        # Image
        return img.rotate(self.get_rotate_r())



    def doUndistort(self, img):
        
        # Image
        return self.__undistort(img)



    def doCrop(self, img):
        
        # Image
        return img.crop(self.get_crop_x(), self.get_crop_y(), self.get_crop_w(), self.get_crop_h())



    # Rotate and undistort
    def doRotUnd(self, img):
        
        # Image
        return self.doRotate(self.doUndistort(img))



    # Rotate, Undistort and crop
    def doRotUndCrop(self, img):
        
        # Image
        return self.doCrop(self.doRotate(self.doUndistort(img)))



    # Return an image, cropped around a point
    def get_fixed_crop(self, img, point, obj):
        if point:
            value = obj.get_cropAroundBy()
            self.set_crop_x(point[0] - value/2)
            self.set_crop_y(point[1] - value/2)
            self.set_crop_w(point[0] + value/2 - self.get_crop_x())
            self.set_crop_h(point[1] + value/2 - self.get_crop_y())

            img = self.doCrop(img)
            fromCroped = False
            mappedPoint = self.get_mapped_coords(point, fromCroped)
        
        # Image, Point
        return img, mappedPoint



    # Return a point, mapped to/from the croped image
    def get_mapped_coords(self, point, fromCroped):
        if point and fromCroped:
            
            # Point
            return (point[0] + self.get_crop_x(), point[1] + self.get_crop_y())
        elif point and not fromCroped:
            return (point[0] - self.get_crop_x(), point[1] - self.get_crop_y())
        return None



    # Get/Set parameters
    def get_crop_x(self):
        
        # Integer
        return self.__crop_x

    def get_crop_y(self):
        
        # Integer
        return self.__crop_y

    def get_crop_w(self):
        
        # Integer
        return self.__crop_w

    def get_crop_h(self):
        
        # Integer
        return self.__crop_h

    def get_rotate_r(self):
        
        # Float
        return self.__rotate

    def get_camera_h(self):
        
        # Float
        return self.__camera_height

    def get_robot_h(self):
        
        # Float
        return self.__robot_height

    def get_camera_x(self):
        
        # Integer
        return self.__camera_x

    def get_camera_y(self):
        
        # Integer
        return self.__camera_y



    def set_crop_x(self, crop_x):
        if crop_x >= 0 and crop_x < self.__width -1:
            self.__crop_x = crop_x
        else:
            self.__crop_x = 0

    def set_crop_y(self, crop_y):
        if crop_y >= 0 and crop_y < self.__height -1:
            self.__crop_y = crop_y
        else:
            self.__crop_y = 0

    def set_crop_w(self, crop_w):
        if crop_w > 1 and crop_w + self.get_crop_x() <= self.__width -1:
            self.__crop_w = crop_w
        else:
            self.__crop_w = self.__width - self.get_crop_x()

    def set_crop_h(self, crop_h):
        if crop_h > 1 and crop_h + self.get_crop_y() <= self.__height -1:
            self.__crop_h = crop_h
        else:
            self.__crop_h = self.__height - self.get_crop_y()
            
    def set_rotate_r(self, rotate):
        self.__rotate_r = rotate



