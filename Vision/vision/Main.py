from multiprocessing import Process, Array, Value
import time
from VisionMain import visionMain

class SynchronisedMediator:
    # Pitch and hardware info
    _pitch = None
    _use_mplayer = None
    _use_jpegStreaming = None
    _videoDev = None
    _path = None

    # Synchronised objects
    _pos_ball = Array('i', [0, 0])
    _pos_ball_t = Value('d', 0.0)

    _pos_yellow_bot = Array('i', [0, 0])
    _pos_yellow_bot_t = Value('d', 0.0)

    _pos_blue_bot = Array('i', [0, 0])
    _pos_blue_bot_t = Value('d', 0.0)

    _orientation_yellow_bot = Value('d', 0.0)
    _orientation_yellow_bot_t = Value('d', 0.0)
    _orientation_blue_bot = Value('d', 0.0)
    _orientation_blue_bot_t = Value('d', 0.0)

    _dimensions = Array('i', [0, 0, 0, 0])

    _fps = Value('d', 0.0)
    def __init__(self, pitch=0, use_mplayer=False, use_jpegStreaming=False, videoDev=0, path='vision', computer=1):
        self._pitch = pitch
        self._computer = computer
        self._use_mplayer = use_mplayer
        self._use_jpegStreaming = use_jpegStreaming
        self._videoDev = videoDev
        self._path = path
        t = time.time()
        self._pos_ball_t.value = t
        self._pos_yellow_bot_t.value = t
        self._orientation_yellow_bot_t.value = t
        self._pos_blue_bot_t.value = t
        self._orientation_blue_bot_t.value = t


class Main:


    # Initializing variables
    def __init__(self, pitch=0, use_mplayer=False, use_jpegStreaming=False, videoDev=0, computer=1, path='vision'):
        self._mediator = SynchronisedMediator(pitch, use_mplayer, use_jpegStreaming, videoDev, path, computer)
        self.start_vision_process()

    # Getting the last known position of the desired object together with timestamp
    def get_position_ball(self):
        self.checkProcess()
        return (self._mediator._pos_ball[0], self._mediator._pos_ball[1], self._mediator._pos_ball_t.value)

    def get_position_yellow_bot(self):
        self.checkProcess()
        return (self._mediator._pos_yellow_bot[0], self._mediator._pos_yellow_bot[1], self._mediator._pos_yellow_bot_t.value)

    def get_position_blue_bot(self):
        self.checkProcess()
        return (self._mediator._pos_blue_bot[0], self._mediator._pos_blue_bot[1], self._mediator._pos_blue_bot_t.value)

    def get_orientation_blue_bot(self):
        self.checkProcess()
        return (self._mediator._orientation_blue_bot.value, self._mediator._orientation_blue_bot_t.value)

    def get_orientation_yellow_bot(self):
        self.checkProcess()
        return (self._mediator._orientation_yellow_bot.value, self._mediator._orientation_yellow_bot_t.value)

    def start_vision_process(self):
        self._vision_precept = Process(target=visionMain, args=(self._mediator,))
        self._vision_precept.start()

    def checkProcess(self):
        if not self._vision_precept.is_alive():
            self.start_vision_process()

    # Dimensions of the pitch in the format (centreX, centreY, deltaX, deltaY)
    def get_dimensions(self):
        print tuple(self._mediator._dimensions)
        return tuple(self._mediator._dimensions)

    # Returns the current framerate
    def get_fps(self):
        return self._mediator._fps.value



