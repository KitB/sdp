package test;

import java.io.IOException;
import java.nio.ByteBuffer;

import communication.BluetoothCommunication;

public class BluetoothCommunicationTest5 {

	public static final int TEST_NO = 30;

	public static final String NXT_MAC_ADDRESS = "00:16:53:08:A0:E6";
	public static final String NXT_NAME = "group6";

	private static BluetoothCommunication comms;

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		System.out.println("BluetoothTest 5 Working: " + testBTComm5());
	}

	public static boolean testBTComm5() {
		int sum = 0;
		long start_time = System.currentTimeMillis();
		long end_time = start_time;

		comms = new BluetoothCommunication(NXT_NAME, NXT_MAC_ADDRESS);
		try {
			comms.openBluetoothConnection();
		} catch (IOException e) {
			System.out.println("Error Open Connection");
			end_time = System.currentTimeMillis();
			System.out.println("Time: " + (end_time - start_time) + " ms");
			return false;
		}
		end_time = System.currentTimeMillis();
		System.out.println("Establish Connection Time: "
				+ (end_time - start_time) + " ms");

		if (comms.isRobotReady()) {
			start_time = System.currentTimeMillis();
			byte b = 0x00;
			int num = 1;
			for (int i = 0; i < TEST_NO; i++) {

				byte[] bytes = ByteBuffer.allocate(5).putInt(1, num).array();
				bytes[0] = b;

				try {
					comms.sendToRobot(bytes);
				} catch (IllegalArgumentException e) {
					System.err.println("ERROR: Illegal Argument "
							+ e.toString());
					return false;
				} catch (IOException e) {
					System.err.println("ERROR: IOException " + e.toString());
					return false;
				}

				sum += num;
				num *= 2;
				b++;
			}
		}
		int receive_int = 0;
		byte[] receive_info = null;
		receive_info = comms.receiveBytesFromRobot();
		for (int j = 1; j < receive_info.length; j++) {
			System.out.println(receive_info[j]);
			receive_int <<= 8;
			receive_int += 0x000000FF & receive_info[j];
		}
		System.out.println("Receive : " + receive_info[0] + ", " + receive_int);
		end_time = System.currentTimeMillis();
		System.out.println("Time: " + (end_time - start_time) + " ms");
		System.out.println("Result " + sum + " = " + receive_int);
		comms.closeBluetoothConnection();
		return sum == receive_int;
	}
}
