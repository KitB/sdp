package test;

import java.io.IOException;
import java.nio.ByteBuffer;

import communication.BluetoothCommunication;

public class BluetoothCommunicationTest7 {

	static long start_time = System.currentTimeMillis();

	public static final int test_no = 1000;

	public static final String NXT_MAC_ADDRESS = "00:16:53:08:A0:E6";
	public static final String NXT_NAME = "group6";

	private static BluetoothCommunication comms;

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		for (int i = 1;; i++) {
			System.out.println("Bluetooth Connection No." + i);

			testBTComm7();
		}
	}

	public static boolean testBTComm7() {

		long end_time = System.currentTimeMillis();
		comms = new BluetoothCommunication(NXT_NAME, NXT_MAC_ADDRESS);
		try {
			comms.openBluetoothConnection();
		} catch (IOException e) {
			System.out.println("Error Open Connection");
			end_time = System.currentTimeMillis();
			System.out.println("Time: " + (end_time - start_time) + " ms");
			return false;
		}
		end_time = System.currentTimeMillis();
		System.out.println("Time: " + (end_time - start_time) + " ms");
		if (comms.isRobotReady()) {
			byte b = 0x01;
			int a = 0;
			while (true) {
				System.out.println("Sent : " + b + ", " + a);

				byte[] bytes = ByteBuffer.allocate(5).putInt(1, a).array();
				bytes[0] = b;
				end_time = System.currentTimeMillis();
				System.out.println("Time: " + (end_time - start_time) + " ms");

				try {
					comms.sendToRobot(bytes);
				} catch (IllegalArgumentException e) {
					System.err.println("ERROR: Illegal Argument "
							+ e.toString());
					return false;
				} catch (IOException e) {
					System.err.println("ERROR: IOException " + e.toString());
					return false;
				}
				a++;
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		comms.closeBluetoothConnection();
		end_time = System.currentTimeMillis();
		System.out.println("Time: " + (end_time - start_time) + " ms");
		return true;
	}
}
