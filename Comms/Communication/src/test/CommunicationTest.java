package test;

import java.io.IOException;
import java.nio.ByteBuffer;

import communication.BluetoothCommunication;
import communication.Communication;

public class CommunicationTest {

	public static final String NXT_MAC_ADDRESS = "00:16:53:08:A0:E6";
	public static final String NXT_NAME = "group6";

	private static Communication comms;

	// This main method is an example of how we send commands to the robot
	public static void main(String args[]) throws IOException {
		comms = new BluetoothCommunication(NXT_NAME, NXT_MAC_ADDRESS);
		comms.openBluetoothConnection();

		int i;
		for (i = 0; i < 100; i++) {
			byte[] bytes = ByteBuffer.allocate(5).putInt(1, i).array();
			comms.sendToRobot(bytes);
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				System.err.println(e.toString());
			}
		}

		comms.closeBluetoothConnection();
	}
}
