package test;

import java.io.IOException;
import java.nio.ByteBuffer;

import communication.BluetoothCommunication;

public class BluetoothCommunicationTest4 {

	public static final int TEST_NO = 1000;

	public static final String NXT_MAC_ADDRESS = "00:16:53:08:A0:E6";
	public static final String NXT_NAME = "group6";

	private static BluetoothCommunication comms;

	/**
	 * Test bluetooth communication by sending an array of bytes and the robot
	 * will echo the same array back
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		System.out.println("BluetoothTest 4 Working: " + testBTComm4());
	}

	public static boolean testBTComm4() {
		int correct = 0;
		long start_time = System.currentTimeMillis();
		long end_time = start_time;

		comms = new BluetoothCommunication(NXT_NAME, NXT_MAC_ADDRESS);
		try {
			comms.openBluetoothConnection();
		} catch (IOException e) {
			System.out.println("Error Open Connection");
			end_time = System.currentTimeMillis();
			System.out.println("Time: " + (end_time - start_time) + " ms");
			return false;
		}
		end_time = System.currentTimeMillis();
		System.out.println("Establish Connection Time: "
				+ (end_time - start_time) + " ms");

		if (comms.isRobotReady()) {
			start_time = System.currentTimeMillis();
			byte b = 0x00;
			for (int i = 0; i < TEST_NO; i++) {

				byte[] bytes = ByteBuffer.allocate(5).putInt(1, i * 100000)
						.array();
				bytes[0] = b;

				try {
					comms.sendToRobot(bytes);
				} catch (IllegalArgumentException e) {
					System.err.println("ERROR: Illegal Argument "
							+ e.toString());
					return false;
				} catch (IOException e) {
					System.err.println("ERROR: IOException " + e.toString());
					return false;
				}

				byte[] receive_info = null;
				receive_info = comms.receiveBytesFromRobot();

				System.out.println("Sent : " + b + ", " + i * 100000);

				int receive_int = 0;
				for (int j = 1; j < receive_info.length; j++) {
					System.out.println(receive_info[j]);
					receive_int <<= 8;
					receive_int += 0x000000FF & receive_info[j];
				}
				System.out.println("Receive : " + receive_info[0] + ", "
						+ receive_int);

				end_time = System.currentTimeMillis();
				System.out.println("Time: " + (end_time - start_time) + " ms");

				boolean isMistake = false;
				for (int j = 0; j < 5 && !isMistake; j++) {
					if (bytes[j] != receive_info[j]) {
						isMistake = true;
						System.out.println("ERROR");
					}
				}
				if (!isMistake) {
					correct++;
				}
				b++;
			}
		}
		comms.closeBluetoothConnection();
		System.out.println("Result " + correct + "/" + TEST_NO);
		return correct == TEST_NO;
	}
}
