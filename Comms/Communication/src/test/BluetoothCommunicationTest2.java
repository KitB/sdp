package test;

import java.io.IOException;

import communication.BluetoothCommunication;

public class BluetoothCommunicationTest2 {

	public static final int TEST_NO = 8;

	public static final String NXT_MAC_ADDRESS = "00:16:53:08:A0:E6";
	public static final String NXT_NAME = "group6";

	private static BluetoothCommunication comms;

	/**
	 * Test bluetooth communication by sending number and let the robot do the
	 * summation and return the result
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		System.out.println("BluetoothTest 2 Working: " + testBTComm2());
	}

	public static boolean testBTComm2() {
		boolean correct = false;
		long start_time = System.currentTimeMillis();
		long end_time = start_time;
		int sum = 0;

		comms = new BluetoothCommunication(NXT_NAME, NXT_MAC_ADDRESS);
		try {
			comms.openBluetoothConnection();
		} catch (IOException e) {
			System.out.println("Error Open Connection");
			end_time = System.currentTimeMillis();
			System.out.println("Time: " + (end_time - start_time) + " ms");
			return false;
		}
		end_time = System.currentTimeMillis();
		System.out.println("Establish Connection Time: "
				+ (end_time - start_time) + " ms");

		if (comms.isRobotReady()) {
			start_time = System.currentTimeMillis();
			byte b = 0x01;
			for (int i = 0; i < TEST_NO; i++) {
				// comms.sendToRobot(b); sendToRobot(byte) is deprecated
				System.out.println("Sent :" + b);
				sum += 0x000000FF & b;
				b *= 2;
			}
		}

		System.out.println("Wait Data from Robot");
		int receive_info = 0;
		try {
			receive_info = comms.receiveByteFromRobot();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (receive_info != sum) {
			System.out.println("Error at " + sum + ", " + receive_info);
		} else {
			correct = true;
		}
		end_time = System.currentTimeMillis();
		System.out.println("Time: " + (end_time - start_time) + " ms");
		System.out.println("Result : " + sum + ", " + receive_info);
		comms.closeBluetoothConnection();
		return correct;
	}

}
