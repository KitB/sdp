import lejos.nxt.Button;


public class NXTTest2 {

	public static final int TEST_NO = 8;
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		if (testNXT2()) {
			System.out.println("Good:)");
		}
	}

	public static boolean testNXT2 () {
		NXT.establishConnection();
		NXT.sendReadySignal();

		int info = 0;
		int sum = 0;
		for (int i = 0; i < TEST_NO; i++) {
			info = NXT.readByte();
			System.out.println("Get :" + info);
			sum += info;
		}
		System.out.println("Send Back " + sum);
		NXT.sendByte((byte)sum);
        Button.waitForPress();
        return true;
	}
}
