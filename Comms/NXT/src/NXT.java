import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import lejos.nxt.comm.Bluetooth;
import lejos.nxt.Button;
import lejos.nxt.comm.NXTConnection;

public class NXT {
	private static final byte ROBOT_READY = 0; 
	
    private static InputStream in;
    private static OutputStream out;

    /**
     * Create a bluetooth connection to PC
     */
    public static void establishConnection() {
        System.out.println("Waiting for Bluetooth connection...");
        NXTConnection connection = Bluetooth.waitForConnection();
        in = connection.openInputStream();
        out = connection.openOutputStream();
        System.out.println("Connection established!");
    }

    /**
     * Read a byte from PC
     * @return opcode
     */
    public static int readByte() {
    	int opcode = 0;
    	try {
            opcode = in.read();
        } catch (IOException e) {
        	System.out.println("Couldn't read byte: " + e.toString());
        }
        return opcode;
    }

    /**
     * Read 5-bytes command from PC
     * @return command in array of bytes
     */
    public static byte[] readBytes() {
    	byte[] bytes = new byte[5];
    	try {
            if(in.read(bytes) != 5) {
            	System.out.println("Wrong Format");
            }
        }
    	catch (IOException e) {
        	System.out.println("Couldn't read bytes: " + e.toString());
        }
        return bytes;
    }

    /**
     * Get int parameter from command
     * @param command - a 5 bytes command
     * @return
     */
    public static int getInt(byte[] command) {
    	int val = 0;
    	for (int i = 1; i < 5; i++) {
			val <<= 8;
			val += 0x000000FF & command[i];
		}
    	return val;
    }

    /**
     * Get Opcode from command
     * @param command - a 5 bytes command
     * @return
     */
   public static int getOpcode(byte[] command) {
	   return 0x000000FF & command[0];
   }

    /**
     * Send a byte to PC
     * @param b - a byte of data sending to PC
     */
    public static void sendByte(byte b) {
    	try {
        	out.write(b);
        	out.flush();
    	} catch (IOException e) {
    		System.out.println("Couldn't send byte: " + e.toString());
    	}
    }

    /**
     * Send bytes to PC
     * @param bytes
     */
    public static void sendBytes(byte[] bytes) {
    	try {
        	out.write(bytes);
        	out.flush();
    	} catch (IOException e) {
    		System.out.println("Couldn't send bytes: " + e.toString());
    	}
    }

    /**
     * Send 0 to PC indicating that robot is ready
     */
    public static void sendReadySignal() {
    	sendByte(ROBOT_READY);
    	Robot.init();
    }

    /**
     * Translate the opcode
     * @param opcode
     * @return
     */
    public static boolean readCommand (int opcode) {
    	switch (opcode) {
        case Robot.FORWARDS:
            Robot.move_forward();
            System.out.println("fowards!");
            break;
        case Robot.BACKWARDS:
        	Robot.move_backwards();
        	System.out.println("backwards!");
        	break;
        case Robot.STOP:
        	Robot.stop();
        	System.out.println("stop!");
        	break;
        case Robot.LEFT:
        	Robot.left();
        	System.out.println("left!");
        	break;
        case Robot.RIGHT:
        	Robot.right();
        	System.out.println("right!");
        	break;
        case Robot.STRAIGHT:
        	Robot.straighten();
        	System.out.println("straighten!");
        	break;
        case Robot.KICK:
        	Robot.kick();
        	System.out.println("kick!");
        	break;
        case Robot.EXIT:
        	System.out.println("BREAKING...");
			return false;
        default:
        	Robot.stop();
        	System.out.println("stop-default!");
        	break;
    	}
    	return true;
    }
    
    /**
     * Close connection to PC
     */
    public static void closeNXT() {
    	Robot.close();
    	Button.waitForPress();
    }
    
    // for testing
    public static void main(String args[]) {
        establishConnection();
        sendReadySignal();

        while (true) {
            int opcode;
			opcode = readByte();
			System.out.println("opcode: " + opcode);
            if (!readCommand(opcode))
            	break;
        }
        closeNXT();
    }
}