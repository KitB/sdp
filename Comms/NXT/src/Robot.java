import java.io.File;
import java.io.IOException;

import lejos.nxt.Motor;
import lejos.nxt.Sound;

public class Robot {	
	
	public final static byte FORWARDS = 0X01;
	public final static byte BACKWARDS = 0X02;
	public final static byte STOP = 0X03;
	public final static byte LEFT = 0X05;
	public final static byte RIGHT = 0X04;
	public final static byte STRAIGHT = 0X06;
	public final static byte KICK = 0X07;
	public final static byte EXIT = 0X08;
	
	private static boolean isMovingForward = false;
	private static boolean isMovingBackwards = false;
	private static boolean isTurningLeft = false;
	private static boolean isTurningRight = false;
	private static boolean stopped = true;

	public static void init() {
		// Getting some data about the robot configuration:
		// acceleration is 6000 by default
		// I don't know if that's the MAX.
		System.out.println("Motor A : "+Motor.A.getAcceleration());
		System.out.println("Motor B : "+Motor.B.getAcceleration());
		System.out.println("Motor C : "+Motor.C.getAcceleration());
		
		Motor.A.setAcceleration(3000);
		Motor.B.setAcceleration(3000);
				
		File r2d2 = new File("r2d2.wav");
		
		Sound.setVolume(Sound.VOL_MAX);
		Sound.playSample(r2d2);
	}
	
	public static void close() {
		Sound.beep();
	}
	
	public static void main(String args[]) {
		
	}

	public static void move_forward() {
		if(stopped == true){
			Motor.A.setSpeed(720);
			Motor.B.setSpeed(720);
			Motor.A.forward();
			Motor.B.forward();
			isMovingForward = true;
			stopped = false;
		}
	}
	
	public static void move_backwards() {
		if(stopped == true){
			Motor.A.setSpeed(720);
			Motor.B.setSpeed(720);
			Motor.A.backward();
			Motor.B.backward();
			isMovingBackwards = true;
			stopped = false;
		}		
	}
	
	public static void stop() {
		if(stopped == false){
			
			Motor.A.setSpeed(500);
			Motor.B.setSpeed(500);
			try {
				Thread.sleep(200);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Motor.A.setSpeed(100);
			Motor.B.setSpeed(100);
			try {
				Thread.sleep(200);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Motor.A.setSpeed(30);
			Motor.B.setSpeed(30);
			try {
				Thread.sleep(200);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Motor.A.setSpeed(0);
			Motor.B.setSpeed(0);
			Motor.A.stop();
			Motor.B.stop();
			stopped = true;
		}
	}
	
	public static void left() {
		if(isMovingForward == true){
			Motor.B.setSpeed(350);
			isMovingForward = false;
		}
	}
	public static void right() {
		if(isMovingForward == true){
			Motor.A.setSpeed(350);
			isMovingForward = false;
		}
	}
	public static void straighten(){
		if(!isMovingForward){
			Motor.A.setSpeed(720);
			Motor.B.setSpeed(720);
			isMovingForward = true;
		}
	}
	public static void kick(){
		System.out.println("Kicking");
		Motor.C.setSpeed(900);
		Motor.C.rotate(-45);
		Motor.C.rotate(45);
	}
}
