import lejos.nxt.Button;


public class NXTTest6 {

	public static final int TEST_NO = 1000;

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		if (testNXT5()) {
			System.out.println("Good:)");
		}
	}

	public static boolean testNXT5 () {
		NXT.establishConnection();
		NXT.sendReadySignal();

		byte[] info;
		int sum = 0;
		for (int i = 0; i < TEST_NO; i++) {
			info = NXT.readBytes();
			System.out.println("Get :" + NXT.getOpcode(info) + ", ");
			System.out.println(NXT.getInt(info));
			sum += NXT.getInt(info);
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("Send Back");
		byte[] bytes = new byte[5];
		for (int i = 4; i > 0; i--) {
			bytes[i] = (byte) (0xFF & sum);
			sum >>= 8;
		}
		NXT.sendBytes(bytes);
        Button.waitForPress();
        return true;
	}
}
