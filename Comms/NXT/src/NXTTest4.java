import lejos.nxt.Button;


public class NXTTest4 {

	public static final int TEST_NO = 1000;

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		if (testNXT4()) {
			System.out.println("Good:)");
		}
	}

	public static boolean testNXT4 () {
		NXT.establishConnection();
		NXT.sendReadySignal();

		byte[] info;
		for (int i = 0; i < TEST_NO; i++) {
			info = NXT.readBytes();
			System.out.println("Get :" + NXT.getOpcode(info) + ", ");
			System.out.println(NXT.getInt(info));
			System.out.println("Send Back");
			NXT.sendBytes(info);
		}

        Button.waitForPress();
        return true;
	}
}
