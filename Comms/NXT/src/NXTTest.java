import lejos.nxt.Button;


public class NXTTest {
    
	public static final int test_no = 1000;
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		if (testNXT()) {
			System.out.println("Good:)");
		}
	}

	public static boolean testNXT () {
		NXT.establishConnection();
		NXT.sendReadySignal();

		int info = 0;
		for (int i = 0; i < test_no; i++) {
			info = NXT.readByte();
			System.out.println(info);

			System.out.println("Send Back " + info);
			NXT.sendByte((byte)info);
		}
        
        Button.waitForPress();
        return true;
	}
}
