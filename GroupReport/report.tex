\documentclass[a4paper,12pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[UKenglish]{babel}
\renewcommand{\familydefault}{\sfdefault}
\usepackage{ifpdf}
\usepackage{listings}
\usepackage{graphicx}
\usepackage{mathtools}
\usepackage[hmargin=1cm,vmargin=1cm]{geometry}
\usepackage{amsmath}
\usepackage{xfrac}
\usepackage{algpseudocode}
\usepackage{algorithmicx}
\usepackage{subfig}

\title{SDP: Final Report
	\begin{center}
		\includegraphics[scale=0.4]{team_logo}
	\end{center}}
	\author{Group 6 - Die Flipperwaldt Gersput}
	\date{\today}
\ifpdf
	\pdfinfo {
		/Author)
		/Title (Final Report: Group 6 - Die Flipperwaldt Gersput)
		/Subject (SDP)
		/Keywords (KEYWORDS)
		/CreationDate (D:20111101195909)
	}
\fi

\begin{document}
\maketitle
\thispagestyle{empty}
\pagestyle{empty}

\newpage

\section{Introduction}

The information presented here is intended as a conceptual guide to our systems
with few code examples and concrete information about code only when it
provides greater clarity than a general description. For code examples and
implementation details one should look at the `README.md` file in each system.

\section{Robot}

\subsection{Hardware}

Our robot design is decidedly simple, avoiding difficulties experienced by
teams with more complex designs (such as holonomic wheels).

The robot has two wheels connected directly to two motors with a small ball
bearing behind them so the robot is light, fast, and well balanced. The robot's
kicker is powered by a single motor - using transmissions to gain torque. The
kicker is wide with pegs at the edges to aid in dribbling the ball. Two touch
sensors are mounted on the front and the robot will move backward if either of
these is triggered. This is required to avoid giving away penalties.

\subsection{Software}
Our robot uses LeJOS 0.9.0 with code written in Java. We have three
concurrently executing classes:

\begin{description}
	\item[Receiver] takes input from bluetooth; 
	it runs in its own thread so that
	commands and reconnection can be handled without interfering with the robot's
	movement. When the robot is started this class waits for a bluetooth
	connection and, once it has one, starts listening for commands. Commands are
	detailed in the Communications section.
  
	\item[Instinct] 
	checks whether the touch sensors are pressed and, if so, tells the
  	controller to move backwards briefly.

	\item[Controller] receives commands from \textbf{Receiver} and \textbf{Instinct}
	and runs them on the robot.
\end{description}

\section{Vision}

The vision submodule converts a stream of images from the camera into the 
positions and orientations of objects on the pitch in real time.

\subsection{Position}

The position of an object is obtained by thresholding with object dependent
values in the HSV colour space. This thresholding produces a binarized image
which is then scanned for contiguous areas of white (blobs). These blobs are
filtered to satisfy further object dependent properties (such as blob area and
circularity) and the centroid of the largest remaining blob is taken as the
position of the object.

\subsubsection{Parallax}

As the plates on the robots are significantly elevated above the floor of the
pitch, the vision system needs to project them onto the plane of the pitch.
Accounting for this effect resulted in a 5cm improvement of the position
estimation near the edges.

\subsection{Orientation}

The robot orientations are calculated as the angle of the vector from the
centroid of the black circle and the centroid of the T shape on the plate of
each robot.

\subsection{Debugging}

Our vision debugger overlays an input stream with its estimates of object
properties. The debugger can also be asked for the the intermediate steps of
each calculation such as binarized images. The debugger also allows for
on-the-fly adjustment of many of the parameters and frame by frame movement
through the stream to locate states where vision returns incorrect results.

\begin{figure}
  \centering
  \subfloat[Detection and orientation in vision debugger]{\label{fig:vdebug}\includegraphics[width=0.3\textwidth]{VisionDebug.eps}}
  ~~~~~~~~~ %add desired spacing between images, e. g. ~, \quad, \qquad etc. (or a blank line to force the subfig onto a new line)
  \subfloat[Colour thresholding for position
  detection]{\label{fig:tshape}\includegraphics[width=0.188\textwidth]{Tshape.eps}}
  ~~~~~~~~~ %add desired spacing between images, e. g. ~, \quad, \qquad etc. (or a blank line to force the subfig onto a new line)
  \subfloat[Orientation using the black circle on the
  plate]{\label{fig:bcircle}\includegraphics[width=0.19\textwidth]{BlackCircle.eps}}
  \caption{Steps of vision processing}
  \label{fig:vision}
\end{figure}


\subsection{Experimental results}

On a sample set of 729 images (381 on the main pitch) the position of the
objects was estimated to within 5cm of a human-provided centroid 99.0\% of the
time. The orientation of both robots was estimated to within 15 degrees of
human-provided orientation 96.4\% of the time. The images were obtained from six
different recordings of friendly matches by selecting every 40th frame.

The vision system runs slightly in excess of 25 frames per second which is the
camera's limit.

\subsection{Server}

When in use the vision system provides data to other systems over a socket with
a simple protocol (detailed in the appendix). In the Strategy system this can
be easily accessed by instantiating the object imported as such:

\begin{verbatim}
	from Strategy.Communication.VisionClient import VisionClient
\end{verbatim}

\section{Strategy}

The strategy system uses the data acquired from vision and transforms it into
commands to be sent to the robot. The system is split into a high level
component and two lower level components.

\subsection{High Level}

The decision system is a simple reactive agent that loops through a list of
\textbf{Decision} objects and runs their actions if their conditions are satisfied.
Once an action has been run the agent returns to the top of the list, ensuring
only one action can be performed per frame of input. \textbf{Decision}s typically
consist of a \textbf{Percept} for conditions and an \textbf{Action} for actions.

\subsection{Low Level}

\subsubsection{Vision Access}
Though vision provides a direct interface, the strategy system wraps this in
\textbf{World} and \textbf{WorldObject} classes to provide velocity prediction, 
state freezing, and a visual debugger that actions and percepts can add 
drawn output to.

\subsubsection{Percepts}

\textbf{Percept}s are objects with a defined call method which takes a strategy and
will return a boolean value. They are typically considered questions which a
decision agent can ask of the vision system such as "Do we have the ball?".
\textbf{Percept}s can be chained and negated to produce more intricate questions and we
have implemented some percepts which only make sense when combined, such as
`Has this percept been activated at any point?'.

\subsubsection{Actions}
\textbf{Action}s are functions on a strategy object that use its stored robot control
object to perform motions on the robot. From the strategy they can also access
raw vision data (via a World object) and stored state (via the setExtra and
getExtra methods). \textbf{Action}s are typically such things as `Kick' and `Go to this
point'.

\subsection{Implemented Strategy}

The strategy we ran in matches was as follows:


\label{hlstrategy} 
\begin{algorithmic}  
\If{we can score} kick
\ElsIf{we have the ball and cannot score }  move toward their goalpost at high speed 
\ElsIf{ we are facing the wall and we are near to it }  move towards the centre
\ElsIf{ they have the ball and we are not at a defending position }  move to the defending position (between our goal and the ball)
\Else { go to the reachable predicted position of the ball}
\EndIf
\end{algorithmic}



\subsection{Motion}

PID control is used for point-to-point motion; this is a classic control
technique for systems with oscillatory values caused by infrequent polling. In
the case of two-wheeled robot motion the oscillatory value is the error between
the robot orientation and the direction to the point the robot is moving toward
and the polling frequency is 25hz. Tests showed that it would have required
upwards of 50hz to reliably use naive angle-error motion and 25hz is the
maximum framerate of the camera.

For pathfinding the system uses A* with a few modifications for real-world
usage (such as blacklisted nodes).

\section{Communications}

The communications server listens to commands from Strategy and sends them to
the robot.

\begin{center}
	\includegraphics[width=\textwidth]{comms-class-diagram.eps}
\end{center}

\subsection{Protocol}

Packets for the communications system consist of a single byte for the opcode
and two bytes for arguments. Three byte packets allow high throughput and none
of the commands we were using turned out to require any greater accuracy than
two bytes for arguments.

\subsection{Interface}

A Python interface is available in Strategy
to easily control the robot and is called like so:

\begin{verbatim}
from Strategy.Communication.Communication import RobotControl

r = RobotControl() # Create a TCP connection between Strategy and Comms
r.kick() # Send a kick command to Comms
r.move(127, 127) # Move at the maximum speed forwards
r.move(127, -127) # Turn as fast as possible
\end{verbatim}

Note that in both move commands the maxima are in what can be sent; the maximum
speed of the robot appears to actually be around 90.

\section{Simulator}

The simulator was created to allow the strategy to look ahead into the future
and to interpolate frames from vision -- allowing for a much smoother output.
Due to time constraints the interpolation feature was not implemented but there
remain functions in the system that were created for this purpose.
As the strategy needs to work in real time, the simulator needs to be fast.

We initially attempted to adapt a simulator from a previous year using Java and
Box2D but it was slow and physically unrealistic. Instead we wrote our own
simulator in Python using Pymunk.

The simulator runs in excess of 5000 ticks per second and by default each tick
furthers the simulation by \(\sfrac{1}{300}\) of a second. With this value the
simulator can reliably simulate ahead \(\sfrac{2}{3}\) of a second at a rate of
25hz. One can reasonably increase the tick value to around \(\sfrac{1}{100}\)
to simulate ahead two seconds with good accuracy; \(\sfrac{1}{300}\) was chosen
as the smallest value we could use while still simulating well ahead in real
time.

\subsection{Predictions}

In practice, the simulator is used to provide predictions to the strategy. This
allows us to catch the ball rather than following it.

The simulator is wrapped and controlled by the `Simulation` class and functions
for ease of prediction are in `Simulator.Utils.Lookahead`. To make predictions
in strategy, one should use `Strategy.Utils.Predictor`. A strategy instance
will have a Predictor that can be used by \textbf{Percept}s and
\textbf{Action}s easily.

\subsection{Accuracy}

The simulator does not provide a perfect simulation of the pitch. As you ask it
to predict further and further into the future it becomes less and less
accurate. This inaccuracy stems from both non-realism in the simulation and a
lack of knowledge about the opposing robot. As a result we have found in tests
that the simulator can reliably be used to predict up to two seconds ahead.
Beyond this it becomes unreliable and the robot would likely miss the ball if
predicting it using this.

\section{Final remarks}

\subsection{Possible improvements and caveats}

Having just completed this project, we would like to offer the following advice
on how to improve our systems and work as a successful team.

\subsubsection{Systems}

\paragraph{Vision}

The vision system is fast and, for the most part, accurate. It suffers from a
problem when other teams have set parameters for the capture card. We were able
to deal with this by running MPlayer by hand when we logged on to a system with
some sensible default settings for the capture card. We attempted an automated
solution to this but it did not work.

\paragraph{Simulator}

The simulator has a number of parameters that were never fine-tuned. Tuning
these parameters could significantly reduce simulation error.

Finishing off the simulator's ability to interpolate frames and account for
vision delay would produce a combined vision system that works without delay
and at a frequency greater than the input frequency. The tradeoff would be an
increased margin of error, though this would still be small.

Finally: introducing real-world constraints into the simulator would allow one
to use it as a far more reliable drop-in replacement for the real pitch when
testing. The facility is already included to reduce the output vision framerate
but there should also be a delay between sending a command and having it
execute. The command queue portion of the simulation should also be rewritten
as the communications does not use a queue anymore and this discrepancy could
cause trouble.

\paragraph{Strategy}

Adding a Kalman filter could improve the accuracy of the data received from the
vision. Kalman filters tend to smooth out the noisy signal, which provides
better estimates.

With an improved simulator, one could attempt a number of machine learning
and/or natural computing techniques to produce a more intelligent strategy.

For instance the strategy's fixed set of actions and percepts lend themselves
to a variable-length genetic representation, combining this with the rapid
simulation one could feasibly evolve a strategy using genetic algorithms.

\paragraph{Robot}

Removing the cap on acceleration of the robot motors allows for more power and
speed but reduces accuracy. Had we known this we would have removed this cap
for the matches but enabled it in milestones. Be warned however that one would
likely need to adapt the parameters for PID navigation to both of these
circumstances.

\paragraph{Teamwork}

Use a real project management tool. Our team had a number of instances of
duplicated work whilst members were idle. We improved on this drastically
throughout the project by meeting as subteams increasingly frequently but using
a project management tool to track activities would have assisted greatly.

Meet as frequently as possible for the duration of the project; our
productivity increased significantly when we started doing this.

\clearpage
\appendix

\section{Percepts}
\begin{description}
    \item[HasBall] Do we have the ball? Orientation and distance tolerance from our robot are optional parameters.
    \item[BallMoving] Is ball moving at the moment?
    \item[HasBallMoved] Has the ball moved in the past? Useful for penalties. 
    \item[ApproxAt(Position)] Are we approximately at a specified Position? Optional distance tolerance parameter.
    \item[FacingWall] Is our robot facing the wall?
    \item[NextToWall] Are we next to the wall? Used in combination with facing wall to avoid constant bumping into the wall. 
    \item[InBallMotionLine] Are we in the ball motion line? Useful for interception.
    \item[TheyHaveBall] Do they have the ball?
    \item[FacingGoal] Are we facing their goal?
    \item[FacingTheirSide] Are we facing their side, or our side?
    \item[CanScore] Do we have the ball and would score if we kick?
    \item[InGoal] Is the ball in goal?
    \item[OnGoalLine] Are we on the goal line?
\end{description}


\section{Class Diagram for Robot Subsystem}
	\begin{center}
		\includegraphics[width=\textwidth]{robot_class_diagram_cropped}
	\end{center}
\section{Vision Server Protocol}

Requests from a Vision Client are sent as a single byte with individual values
in that byte representing different requests (1 means blue, 2 means red, etc).
The values for this are supplied in the header of vision\_server.py.

Responses consist of either a position packet, a dimensions packet, or an FPS
packet.

The position packet contains the following values with types:
\begin{description}
    \item[Character] The response type, `r', `b', or `y'. This character is the
        first letter of the colour of the object.
    \item[Unsigned Short] 16-bit number representing the x position of the object
    \item[Unsigned Short] A similar value for y
    \item[Double] The time at which the position value was acquired
    \item[Double] The time at which the angle was acquired
    \item[Float] The angle of the object in radians (0 for the ball)
\end{description}

The dimensions packet contains the following values with types (all numbers in pixels):
\begin{description}
    \item[Character] `d', so that a client knows it has the dimensions
    \item[Unsigned Short] The x value of the centre of the pitch
    \item[Unsigned Short] The y value of the centre of the pitch
    \item[Unsigned Short] The distance from the centre of the pitch to the right of the pitch
    \item[Unsigned Short] A similar distance to the top of the pitch
\end{description}
Though this form may seem strange for dimensions it eases the calculation of the abstract dimensions in strategy.

The FPS packets consist of:
\begin{description}
    \item[Character]`f'
    \item[Float] The FPS at which vision is running.
\end{description}


\end{document}
