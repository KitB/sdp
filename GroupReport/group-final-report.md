%Final Report
% Group 6 - Die Flipperwaldt Gersput

# Introduction
The information presented here is intended as a conceptual guide to our systems
with few code examples and concrete information about code only when it
provides greater clarity than a general description. For code examples and
implementation details one should look at the `README.md` file in each system.

# Robot
## Hardware
Our robot design is decidedly simple, avoiding difficulties experienced by
teams with more complex designs (such as holonomic wheels).

The robot has two wheels connected directly to two motors with a small ball
bearing behind them so the robot is light, fast, and well balanced. The robot's
kicker is powered by a single motor - using transmissions to gain torque. The
kicker is wide with pegs at the edges to aid in dribbling the ball. Two touch
sensors are mounted on the front and the robot will move backward if either of
these is triggered. This is required to avoid giving away penalties.

## Software
Our robot uses LeJOS 0.9.0 with code written in Java. We have three
concurrently executing classes:

* `Receiver` takes input from bluetooth; it runs in its own thread so that
  commands and reconnection can be handled without interfering with the robot's
  movement. When the robot is started this class waits for a bluetooth
  connection and, once it has one, starts listening for commands. Commands are
  detailed in the [Communications](#communications) section.
* `Instinct` checks whether the touch sensors are pressed and, if so, tells the
  controller to move backwards briefly.
* `Controller` receives commands from `Receiver` and `Instinct` and runs them
  on the robot.

# Vision
The vision submodule converts a stream of images from the camera into the positions
and orientations of objects on the pitch in real time.

## Position
The position of an object is obtained by thresholding with object dependent
values in the HSV colour space. This thresholding produces a binarized image
which is then scanned for contiguous areas of white (blobs). These blobs are
filtered to satisfy further object dependent properties (such as blob area and
circularity) and the centroid of the largest remaining blob is taken as the
position of the object.

### Parallax
As the plates on the robots are significantly elevated above the floor of the
pitch, the vision system needs to project them onto the plane of the pitch.
Without this accuracy near the edges of the pitch would be significantly
reduced.

## Orientation
The robot orientations are calculated as the angle of the vector from the
centroid of the black circle and the centroid of the T shape on the plate of
each robot.

## Parameters
Object properties for each pitch as well as pitch specific information such as
camera position and barrel distortion matrices are stored in XML files inside
the Parameters directory.

## Debugging
Our vision debugger overlays an input stream with its estimates of object
properties. The debugger can also be asked for the the intermediate steps of
each calculation such as binarized images. The debugger also allows for
on-the-fly adjustment of many of the parameters and frame by frame movement
through the stream to locate states where vision returns incorrect results.

## Experimental results
On a sample set of 729 images (381 on the main pitch) the position of the
objects was estimated to within 5cm of a human-provided centroid 99.0% of the
time. The orientation of both robots was estimated to within 15 degrees of
human-provided orientation 96.4% of the time. The images were obtained from six
different recordings of friendly matches by selecting every 40th frame.

The vision system runs slightly in excess of 25 frames per second which is the
camera's limit so further optimization would be fruitless.

## Server
When in use the vision system provides data to other systems over a socket with
a simple protocol (detailed in the appendix). In the Strategy system this can
be easily accessed by instantiating the object imported as such: `from
Strategy.Communication.VisionClient import VisionClient`.

# Strategy
The strategy system uses the data acquired from vision and transforms it into
commands to be sent to the robot. The system is split into a high level
component and two lower level components.

## High level
The decision system is a simple reactive agent that loops through a list of
`Decision` objects and runs their actions if their conditions are satisfied.
Once an action has been run the agent returns to the top of the list, ensuring
only one action can be performed per frame of input. Decisions typically
consist of a `Percept` for conditions and an `Action` for actions.

## Lower level
### Vision Access
Though vision provides a direct interface, the strategy system wraps this in
`World` and `WorldObject` classes to provide velocity prediction, state
freezing, and a visual debugger that actions and percepts can add drawn output
to.

### Percepts
Percepts are objects with a defined call method which takes a strategy and
will return a boolean value. They are typically considered questions which a
decision agent can ask of the vision system such as "Do we have the ball?".
Percepts can be chained and negated to produce more intricate questions and we
have implemented some percepts which only make sense when combined, such as
"Has this percept been activated at any point?".

### Actions
Actions are functions on a strategy object that use its stored robot control
object to perform motions on the robot. From the strategy they can also access
raw vision data (via a World object) and stored state (via the setExtra and
getExtra methods). Actions are typically such things as "Kick" and "Go to this
point".

## Our implemented strategy
The strategy we ran in matches was as follows:

~~~~{.haskell}
if we can score
    -> kick
if we have the ball and cannot score
    -> move toward their goalpost at high speed
if we are facing the wall and we are near to it
    -> move towards the centre
if they have the ball and we are at a defending position
    -> move towards the reachable predicted ball position
if they have the ball
    -> move to the defending position (between our goal and the ball)
if true
    -> go to the reachable predicted position of the ball
~~~~

## Motion
PID control is used for point-to-point motion; this is a classic control
technique for systems with oscillatory values caused by infrequent polling. In
the case of two-wheeled robot motion the oscillatory value is the error between
the robot orientation and the direction to the point the robot is moving toward
and the polling frequency is 25hz. Tests showed that it would have required
upwards of 50hz to reliably use naive angle-error motion and 25hz is the
maximum framerate of the camera.

For pathfinding the system uses A* with a few modifications for real-world
usage (such as blacklisted nodes).

# Communications
The communications server listens to commands from Strategy and sends them to
the robot.

![The Communications Server Classes](comms-class-diagram.png)

## Protocol
Packets for the communications system consist of a single byte for the opcode
and two bytes for arguments. Three byte packets allow high throughput and none
of the commands we were using turned out to require any greater accuracy than
two bytes for arguments.

## Interface
A Python interface is available in Strategy
to easily control the robot and is called like so:

~~~~{.python}
from Strategy.Communication.Communication import RobotControl

r = RobotControl() # Create a TCP connection between Strategy and Comms
r.kick() # Send a kick command to Comms
r.move(127, 127) # Move at the maximum speed forwards
r.move(127, -127) # Turn as fast as possible
~~~~

Note that in both move commands the maxima are in what can be sent; the maximum
speed of the robot appears to actually be around 90.

# Simulator
The simulator was created to allow the strategy to look ahead into the future
and to interpolate frames from vision -- allowing for a much smoother output.
Due to time constraints the interpolation feature was not implemented but there
remain functions in the system that were created for this purpose.
As the strategy needs to work in real time, the simulator needs to be fast.

We initially attempted to adapt a simulator from a previous year using Java and
Box2D but it was slow and physically unrealistic. Instead we wrote our own
simulator in Python using Pymunk.

The simulator runs in excess of 5000 ticks per second and by default each tick
furthers the simulation by $\sfrac{1}{300}$ of a second. With this value the
simulator can reliably simulate ahead $\sfrac{2}{3}$ of a second at a rate of
25hz.

## Predictions
In practice, the simulator is used to provide predictions to the strategy. This
allows us to catch the ball rather than following it.

The simulator is wrapped and controlled by the `Simulation` class and functions
for ease of prediction are in `Simulator.Utils.Lookahead`. To make predictions
in strategy, one should use `Strategy.Utils.Predictor`. A strategy instance
will have a Predictor that can be used by Percepts and Actions easily.

## Accuracy
The simulator does not provide a perfect simulation of the pitch. As you ask it
to predict further and further into the future it becomes less and less
accurate. This inaccuracy stems from both non-realism in the simulation and a
lack of knowledge about the opposing robot. As a result we have found in tests
that the simulator can reliably be used to predict up to two seconds ahead.
Beyond this it becomes unreliable and the robot would likely miss the ball if
predicting it using this.

# Final remarks
## Possible improvements and caveats
Having just completed this project, we would like to offer the following advice
on how to improve our systems and work as a successful team.

### Systems
#### Vision
The vision system is fast and, for the most part, accurate. It suffers from a
problem when other teams have set parameters for the capture card. We were able
to deal with this by running MPlayer by hand when we logged on to a system with
some sensible default settings for the capture card. We attempted an automated
solution to this but it did not work.

#### Simulator
The simulator has a number of parameters that were never fine-tuned. Tuning
these parameters could significantly reduce simulation error.

Finishing off the simulator's ability to interpolate frames and account for
vision delay would produce a combined vision system that works without delay
and at a frequency greater than the input frequency. The tradeoff would be an
increased margin of error, though this would still be small.

Finally: introducing real-world constraints into the simulator would allow one
to use it as a far more reliable drop-in replacement for the real pitch when
testing. The facility is already included to reduce the output vision framerate
but there should also be a delay between sending a command and having it
execute. The command queue portion of the simulation should also be rewritten
as the communications does not use a queue anymore and this discrepancy could
cause trouble.

#### Strategy
With an improved simulator, one could attempt a number of machine learning
and/or natural computing techniques to produce a more intelligent strategy.

For instance the strategy's fixed set of actions and percepts lend themselves
to a variable-length genetic representation, combining this with the rapid
simulation one could feasibly evolve a strategy using genetic algorithms.

#### Robot
Removing the cap on acceleration of the robot motors allows for more power and
speed but reduces accuracy. Had we known this we would have removed this cap
for the matches but enabled it in milestones. Be warned however that one would
likely need to adapt the parameters for PID navigation to both of these
circumstances.

### Teamwork
Use a real project management tool. Our team had a number of instances of
duplicated work whilst members were idle. We improved on this drastically
throughout the project by meeting as subteams increasingly frequently but using
a project management tool to track activities would have assisted greatly.

Meet as frequently as possible for the duration of the project; our
productivity increased significantly when we started doing this.
