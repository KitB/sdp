import subprocess
import os

class Communications:
    _subp = None
    port = None
    host = None
    path = None
    def __init__(self, port=None, host=None):
        os.chdir(os.path.dirname(__file__))
        os.chdir("../Comms")
        self.host = host
        self.path = os.getcwd()

    def start(self):
        cmd = "./run_comms.sh"
        if self.host != "localhost":
            cmd = "ssh -Ct %s 'cd %s; %s'" % (self.host, self.path, cmd)
        self._subp = subprocess.Popen(cmd, shell=True)

    def stop(self):
        self._subp.terminate()
        subprocess.Popen("pgrep -f CommunicationServer | xargs kill -2", shell=True)

    def recompile(self):
        cmd = "./make_comms.sh"
        if self.host != "localhost":
            cmd = "ssh -Ct %s 'cd %s; %s'" % (self.host, self.path, cmd)

        subprocess.Popen(cmd, shell=True)
