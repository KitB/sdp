#!/usr/bin/env python
import pygtk
pygtk.require('2.0')
import gtk
import os
from Controllers import Communications, Vision, Strategy
from Strategy.Params import Params


class Control:


    # get the parameters from the radio buttons and entry, then compile the vision
    def make_vision(self, widget):
        print "Making vision"
        self.vision_label_port.set_label("Port: " + self.vision_entry_port.get_text())
        self.vision_label_host.set_label("Host: " + self.vision_entry_host.get_text())
        port = int(self.vision_entry_port.get_text())
        host = (self.vision_entry_host.get_text())
        computer = self.vision_entry_computer.get_text()
        if self.vision_radio_pitch_main.get_active():
            pitch = 0
        else:
            pitch = 1
        if self.vision_radio_mplayer_true.get_active():
            use_mplayer = True
        else:
            use_mplayer = False
        video_device_number = 0
        self.vision_process = Vision(port, pitch, use_mplayer, video_device_number, host, computer)
        print "Vision compiled"
        # Enable clicking of the "Start Vision" button. This is how you change the sensitivity of a gtk.ToggleButton.
        self.vision_button.props.sensitive = True

    # Actually start the vision.
    def vision(self, widget):
        if widget.get_active():
            print "Vision starting"
            self.vision_process.start()
            self.vision_button.set_label("Stop vision")
            # Disable the compile vision button, so that if they want to restart it, it forces self.vision_process.stop() to be called first
            self.comms_button_make.set_sensitive(True)
            self.vision_button_make.set_sensitive(False)
        else:
            print "Vision stopping"
            self.activity(3)
            self.vision_button.set_label("Start vision")
            # If vision has been stopped, allow the user to recompile vision.
            self.vision_button_make.set_sensitive(True)
            self.comms_button_make.set_sensitive(False)



    # Compile comms
    def make_comms(self, widget):
        print "Making comms"
        self.comms_label_port.set_label("Port: " + self.comms_entry_port.get_text())
        self.comms_label_host.set_label("Host: " + self.comms_entry_host.get_text())
        commsport = int(self.comms_entry_port.get_text())
        commshost = (self.comms_entry_host.get_text())
        self.comms_process = Communications(commsport, commshost)
        self.comms_process.recompile()
        print "Comms compiled"
        self.comms_button.props.sensitive = True

    # Launch the comms, unlock Strategy.
    def comms(self, widget):
        if widget.get_active():
            print "Comms starting"
            self.comms_process.start()
            self.comms_button.set_label("Stop comms")
            self.comms_button_make.set_sensitive(False)
            self.strategy_button_make.set_sensitive(True)
        else:
            print "Comms stopping"
            self.activity(2)
            self.comms_button_make.set_sensitive(True)
            self.comms_button.set_label("Start comms")


    # Set the variables for strategy.
    def make_strategy(self, widget):
        visionhost = self.vision_entry_host.get_text()
        visionport = int(self.vision_entry_port.get_text())

        commshost = self.comms_entry_host.get_text()
        commsport = int(self.comms_entry_port.get_text())

        if self.firstRun == True:
            self.strategy_process.initVision(visionhost, visionport)
            self.strategy_process.initRobotControl(commshost, commsport)
            self.firstRun = False
        else:
            self.strategy_process.updateVisionSource(visionhost, visionport)
            self.strategy_process.updateRobotControl(commshost, commsport)

        if self.strategy_radio_side_low.get_active():
            self.strategy_process.setSide("low")
        else:
            self.strategy_process.setSide("high")

        if self.strategy_radio_player_blue.get_active():
            self.strategy_process.setPlayer("b")
        else:
            self.strategy_process.setPlayer("y")

        self.strategy_process.setStrategy(self.strategy_current)
        self.strategy_process.updateStrategy()
        print "Making strategy: %s" % (self.strategy_current)
        self.strategy_button.props.sensitive = True    

    # Start the strategy.
    def strategy(self, widget):
        if widget.get_active():
            print "Strategy starting"
            self.strategy_process.start()
            self.strategy_button.set_label("Stop strategy")
        else:
            print "Strategy stopping"
            self.activity(1)
            self.strategy_button.set_label("Start strategy")

    def make_list(self, dname=None):
        listmodel = gtk.ListStore(str)
        for s in self.strategy_list:
            listmodel.append([s])
        return listmodel

    def strategy_name(self, column, cell, model, iter):
        cell.set_property('text', model.get_value(iter, 0))
        return

    def updateStrategy(self, treeview, path, column):
        model = treeview.get_model()
        iter = model.get_iter(path)
        self.strategy_current = model.get_value(iter, 0)
        self.strategy_button_make.set_label("Prepare strategy: %s" % self.strategy_current)
        print "new strategy selected: " + self.strategy_current

    def activity(self, proc):
            if proc == 3:            
                if self.vision_button.get_active() == True:
                    self.vision_process.stop()
                if self.comms_button.get_active() == True:
                    self.comms_process.stop()
                if self.strategy_button.get_active() == True:
                    self.strategy_process.stop()
                self.vision_button.set_label("Start vision")
                self.vision_button.props.sensitive = False
                self.comms_button_make.set_sensitive(False)
                self.comms_button.set_label("Start comms")
                self.comms_button.props.sensitive = False
                self.strategy_button_make.set_sensitive(False)
                self.strategy_button.set_label("Start strategy")
                self.strategy_button.props.sensitive = False
            elif proc ==2:
                if self.comms_button.get_active() == True:
                    self.comms_process.stop()
                if self.strategy_button.get_active() == True:
                    self.strategy_process.stop()
                self.comms_button.set_label("Start comms")
                self.comms_button.props.sensitive = False
                self.strategy_button_make.set_sensitive(False)
                self.strategy_button.set_label("Start strategy")
                self.strategy_button.props.sensitive = False
            elif proc == 1:
                if self.strategy_button.get_active() == True:
                    self.strategy_process.stop()
                self.strategy_button.set_label("Start strategy")
                self.strategy_button.props.sensitive = False

    

    # Quit function that ensures all processes are ended before we actually quit.
    def quit(self, event=None, widget=None):
        print "Closing"
        self.activity(3)
        Params.stop()
        gtk.main_quit()

    # When help is clicked, spawn a dialog box with 3 buttons. When one of those buttons is clicked, create a message box with information.
    def help(self, widget):
        label = gtk.Label("What do you need help with?")
        dialog = gtk.Dialog("Select a category",
                            None,
                            gtk.DIALOG_MODAL | gtk.DIALOG_DESTROY_WITH_PARENT,
                            )
        dialog.vbox.pack_start(label)
        dialog.add_buttons("Vision", 1, "Comms", 2, "Strategy", 3)
        response = dialog.run()
        dialog.destroy()

        if (response == 1):
            messagedialog = gtk.MessageDialog(self.window, 0, gtk.MESSAGE_INFO, gtk.BUTTONS_OK, "How to run the Vision.")
            messagedialog.format_secondary_text("First make sure you have set the paramaters to how you wish them. Then click 'compile vision'. "
                                                "This readies the vision system. Click start to proceed. If you wish to change the paramaters "
                                                "you will need to recompile vision. \n \n The port specifies which port the vision is listening on.")
            messagedialog.run()
            messagedialog.destroy()

        elif (response == 2):
            messagedialog = gtk.MessageDialog(self.window, 0, gtk.MESSAGE_INFO, gtk.BUTTONS_OK, "How to run the Comms.")
            messagedialog.format_secondary_text("Once vision is running, comms is started next. As before, compile then start. This"
                                                " lets you start the strategy in the next tab.\n \n The port and host let you choose a different"
                                                " computer to run on.")
            messagedialog.run()
            messagedialog.destroy()
        else:
            messagedialog = gtk.MessageDialog(self.window, 0, gtk.MESSAGE_INFO, gtk.BUTTONS_OK, "How to run the Strategy.")
            messagedialog.format_secondary_markup("The strategy needs the vision and communications servers running first. \n"
                                                "Once these are functional, Select a strategy by <b>double clicking </b> from the automatically "
                                                "populated tree on the right. "
                                                "Click 'prepare strategy' to load everything, ready for an immediate start. Click"
                                                " 'Start Video' to start")
            messagedialog.run()
            messagedialog.destroy()


    def __init__(self, dname = None):

        self.scrolledwindow_size = (0,0)
        self.column_names = ['Strategy']

        self.window = gtk.Window(gtk.WINDOW_TOPLEVEL)
        self.window.set_title("Control GUI")
        self.window.connect("delete_event", self.quit)
        self.window.set_border_width(10)

        self.box_main = gtk.VBox(False, 10)

        self.notebook = gtk.Notebook()

        self.box_vision = gtk.VBox(False, 10)
        self.box_vision_buttons = gtk.HBox(False, 0)
        self.box_vision_port = gtk.HBox(False, 5)
        self.box_vision_host = gtk.HBox(False, 5)
        self.box_vision_computer = gtk.HBox(False, 5)
        self.box_vision_pitch = gtk.HBox(False, 5)
        self.box_vision_mplayer = gtk.HBox(False, 5)

        self.box_comms = gtk.VBox(False, 10)
        self.box_comms_buttons = gtk.HBox(False, 0)
        self.box_comms_port = gtk.HBox(False, 10)
        self.box_comms_host = gtk.HBox(False, 10)

        self.box_strategy2 = gtk.HBox(False, 10)
        self.box_strategy = gtk.VBox(False, 10)
        self.strategy_tree_strats = gtk.VBox(False, 3)
        self.box_strategy_connection_text = gtk.HBox(False, 0)
        self.box_strategy_connection = gtk.HBox(False, 0)
        self.box_player_radios = gtk.HBox(False, 5)
        self.box_side_radios = gtk.HBox(False, 5)
        self.box_strategy_buttons = gtk.HBox(False, 0)

        self.box_tools = gtk.HBox(False, 10)


        self.vision_label = gtk.Label("Vision")
        self.comms_label = gtk.Label("Comms")
        self.strategy_label = gtk.Label("Strategy")

        self.vision_frame = gtk.Frame()       
        self.vision_frame.add(self.box_vision)
        self.notebook.append_page(self.vision_frame, self.vision_label)

        self.comms_frame = gtk.Frame()
        self.comms_frame.add(self.box_comms)
        self.notebook.append_page(self.comms_frame, self.comms_label)

        self.strategy_frame = gtk.Frame()
        self.strategy_frame.add(self.box_strategy2)
        self.notebook.append_page(self.strategy_frame, self.strategy_label)
      

        self.vision_button_make = gtk.Button("Compile vision")
        self.vision_button_make.connect("clicked", self.make_vision)

        self.vision_separator = gtk.HSeparator() 

        self.vision_button = gtk.ToggleButton("Start vision")
        self.vision_button.connect("toggled", self.vision)
        self.vision_button.props.sensitive = False

        self.vision_label_port = gtk.Label("Port: 12345")

        self.vision_entry_port = gtk.Entry()
        self.vision_entry_port.set_text("12345")

        self.vision_label_host = gtk.Label("Host: localhost")

        self.vision_entry_host = gtk.Entry()
        self.vision_entry_host.set_text("localhost")

        self.vision_label_computer = gtk.Label("Computer: 11")

        self.vision_entry_computer = gtk.Entry()
        self.vision_entry_computer.set_text("11")

        self.vision_label_pitch = gtk.Label("Which pitch are you using?")
        self.vision_radio_pitch_main = gtk.RadioButton(group=None, label="Main")
        self.vision_radio_pitch_side = gtk.RadioButton(group=self.vision_radio_pitch_main, label="Side")

        self.vision_label_mplayer = gtk.Label("Are you using mplayer?")
        self.vision_radio_mplayer_true = gtk.RadioButton(group=None, label="True")
        self.vision_radio_mplayer_false = gtk.RadioButton(group=self.vision_radio_mplayer_true, label="False")
        self.vision_radio_mplayer_false.set_active(True)

        self.box_vision_buttons.pack_start(self.vision_button_make, True, True, 0)
        self.box_vision_buttons.pack_start(self.vision_separator, True, True, 0)
        self.box_vision_buttons.pack_start(self.vision_button, True, True, 0)

        self.box_vision_port.pack_start(self.vision_label_port, True, True, 0)
        self.box_vision_port.pack_start(self.vision_entry_port, True, True, 0)

        self.box_vision_host.pack_start(self.vision_label_host, True, True, 0)
        self.box_vision_host.pack_start(self.vision_entry_host, True, True, 0)

        self.box_vision_computer.pack_start(self.vision_label_computer, True, True, 0)
        self.box_vision_computer.pack_start(self.vision_entry_computer, True, True, 0)

        self.box_vision_pitch.pack_start(self.vision_label_pitch, True, True, 0)
        self.box_vision_pitch.pack_start(self.vision_radio_pitch_main, True, True, 0)
        self.box_vision_pitch.pack_start(self.vision_radio_pitch_side, True, True, 0)

        self.box_vision_mplayer.pack_start(self.vision_label_mplayer, True, True, 0)
        self.box_vision_mplayer.pack_start(self.vision_radio_mplayer_true, True, True, 0)
        self.box_vision_mplayer.pack_start(self.vision_radio_mplayer_false, True, True, 0)

        self.box_vision.pack_start(self.box_vision_buttons, True, True, 0)
        self.box_vision.pack_start(self.box_vision_port, True, True, 0)
        self.box_vision.pack_start(self.box_vision_host, True, True, 0)
        self.box_vision.pack_start(self.box_vision_computer, True, True, 0)
        self.box_vision.pack_start(self.box_vision_pitch, True, True, 0)
        self.box_vision.pack_start(self.box_vision_mplayer, True, True, 0)


        self.comms_button_make = gtk.Button("Compile comms")
        self.comms_button_make.connect("clicked", self.make_comms)
        self.comms_button_make.set_sensitive(False)

        self.comms_button = gtk.ToggleButton("Start comms")
        self.comms_button.connect("toggled", self.comms)
        self.comms_button.props.sensitive = False



        self.comms_separator = gtk.HSeparator() 

        self.comms_label_port = gtk.Label("Port: 6789")

        self.comms_entry_port = gtk.Entry()
        self.comms_entry_port.set_text("6789")

        self.comms_label_host = gtk.Label("Host: localhost")

        self.comms_entry_host = gtk.Entry()
        self.comms_entry_host.set_text("localhost")

        self.box_comms_buttons.pack_start(self.comms_button_make, False, True, 0)
        self.box_comms_buttons.pack_start(self.comms_separator, True, True, 0)
        self.box_comms_buttons.pack_start(self.comms_button, False, True, 0)

        self.box_comms_port.pack_start(self.comms_label_port, False, True, 0)
        self.box_comms_port.pack_start(self.comms_entry_port, False, True, 0)
        
        self.box_comms_host.pack_start(self.comms_label_host, False, True, 0)
        self.box_comms_host.pack_start(self.comms_entry_host, False, True, 0)

        self.box_comms.pack_start(self.box_comms_buttons, False, True, 0)
        self.box_comms.pack_start(self.box_comms_port, False, True, 0)
        self.box_comms.pack_start(self.box_comms_host, False, True, 0)


        self.strategy_label_connection_host = gtk.Label("Host: localhost")
        self.strategy_label_connection_port = gtk.Label("Port: 12345")

        self.strategy_entry_host = gtk.Entry()
        self.strategy_entry_host.set_text("localhost")

        self.strategy_entry_port = gtk.Entry()
        self.strategy_entry_port.set_text("12345")

        self.strategy_player_label = gtk.Label("What player are you?")
        self.strategy_radio_player_blue = gtk.RadioButton(group=None, label="Blue")
        self.strategy_radio_player_yellow = gtk.RadioButton(group=self.strategy_radio_player_blue, label="Yellow")

        self.strategy_radio_side_low = gtk.RadioButton(group=None, label="Low")
        self.strategy_radio_side_high = gtk.RadioButton(group=self.strategy_radio_side_low, label="High")
        self.strategy_side_label = gtk.Label("What side are you?")


        self.strategy_button = gtk.ToggleButton("Start strategy")
        self.strategy_button.connect("toggled", self.strategy)
        self.strategy_button.props.sensitive = False

        self.strategy_process = Strategy()
        self.strategy_list = self.strategy_process.getStrategies()
        self.firstRun = True

        self.strategy_button_make = gtk.Button("Prepare strategy: %s" % self.strategy_list[0])
        self.strategy_button_make.connect("clicked", self.make_strategy)
        self.strategy_button_make.set_sensitive(False)


##############################Don't ask me how this works, I just bastardized an example tree I found online #######################################  
        listmodel = self.make_list(dname)
        self.treeview = gtk.TreeView()

        self.tvcolumn = [None] * len(self.column_names)
        cellpb = gtk.CellRendererPixbuf()
        self.tvcolumn[0] = gtk.TreeViewColumn(self.column_names[0], cellpb)
        cell = gtk.CellRendererText()
        self.tvcolumn[0].pack_start(cell, True)
        self.tvcolumn[0].set_cell_data_func(cell, self.strategy_name)
        self.treeview.append_column(self.tvcolumn[0])
        for n in range(1, len(self.column_names)):
            cell = gtk.CellRendererText()
            self.tvcolumn[n] = gtk.TreeViewColumn(self.column_names[n], cell)
            if n == 1:
                cell.set_property('xalign', 1.0)
            self.tvcolumn[n].set_cell_data_func(cell, cell_data_funcs[n])
            self.treeview.append_column(self.tvcolumn[n])

        self.treeview.connect('row-activated', self.updateStrategy)
        self.scrolledwindow = gtk.ScrolledWindow()
        self.scrolledwindow.add(self.treeview)
        self.scrolledwindow.set_policy(gtk.POLICY_AUTOMATIC,gtk.POLICY_AUTOMATIC)
        self.treeview.set_model(listmodel)
        self.scrolledwindow.set_size_request(150, 300)

        self.strategy_current = self.strategy_list[0]
        self.strategy_tree_strats.pack_start(self.scrolledwindow, True, True, 0)

############################### Feel free to ask again ##############################################


        self.box_strategy_connection_text.pack_start(self.strategy_label_connection_host, True, True, 0)
        self.box_strategy_connection_text.pack_start(self.strategy_label_connection_port, True, True, 0)

        self.box_strategy_connection.pack_start(self.strategy_entry_host, True, True, 0)
        self.box_strategy_connection.pack_start(self.strategy_entry_port, True, True, 0)

        self.box_player_radios.pack_start(self.strategy_player_label, True, True, 0)
        self.box_player_radios.pack_start(self.strategy_radio_player_blue, True, True, 0)
        self.box_player_radios.pack_start(self.strategy_radio_player_yellow, True, True, 0)

        self.box_side_radios.pack_start(self.strategy_side_label, True, True, 0)
        self.box_side_radios.pack_start(self.strategy_radio_side_low, True, True, 0)
        self.box_side_radios.pack_start(self.strategy_radio_side_high, True, True, 0)

        self.box_strategy_buttons.pack_start(self.strategy_button_make, True, True, 0)
        self.box_strategy_buttons.pack_start(self.strategy_button, True, True, 0)

        self.box_strategy.pack_start(self.box_strategy_connection_text, True, True, 0)
        self.box_strategy.pack_start(self.box_strategy_connection, True, True, 0)
        self.box_strategy.pack_start(self.box_player_radios, True, True, 0)
        self.box_strategy.pack_start(self.box_side_radios, True, True, 0)
        self.box_strategy.pack_start(self.box_strategy_buttons, True, True, 0)

        self.box_strategy2.pack_start(self.box_strategy, True, True, 0)
        self.box_strategy2.pack_start(self.strategy_tree_strats,True, True, 0)

        self.button_quit = gtk.Button("Quit")
        self.button_quit.connect("clicked", self.quit)

        self.button_help = gtk.Button("Help")
        self.button_help.connect("clicked", self.help)

        self.box_tools.pack_start(self.button_quit, True, True, 0)
        self.box_tools.pack_start(self.button_help, True, True, 0)
        self.box_main.pack_start(self.notebook, True, True, 0)
        self.box_main.pack_start(self.box_tools, True, True, 0)


        self.window.add(self.box_main)
        self.window.show_all()

    def run(self):
        gtk.main()
